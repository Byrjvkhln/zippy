import img1 from "@/public/Vector.svg";
import img2 from "@/public/Vector (1).svg";
import img3 from "@/public/Vector (2).svg";
import img4 from "@/public/Vector (3).svg";
import img5 from "@/public/Vector (4).svg";
import img6 from "@/public/Vector (5).svg";

import noyon from "@/public/partners/noyon.png";
import easy from "@/public/partners/easy.png";
import arur from "@/public/partners/arur.png";
import tedy from "@/public/partners/tedy.png";
import nama from "@/public/partners/nama.png";

import { BiSolidCategory, BiUser } from "react-icons/bi";
import { TbTruckDelivery, TbPlus, TbListDetails, TbSettings } from "react-icons/tb";
import { RiCustomerServiceLine } from "react-icons/ri";

export const data = [
  {
    title: "Программ хангамж",
    desc: `Тенологид суурилсан хүргэлтийн цогч систем. `,
    icon: img1,
    bg: `bg-red-500`,
  },
  {
    title: "Баг хамт олон",
    desc: `Чадварлаг баг хамт олон таны бүтээгдэхүүнийг аюулгүй, шуурхай хүргэж үйлчилнэ. `,
    icon: img2,
    bg: ``,
  },
  {
    title: "Цаг хугацаа",
    desc: `Цаг хугацаагаа хэмнэх, бизнесээ илүү олон хүнд хүргэхэд тусална `,
    icon: img3,
    bg: `bg-blue-900`,
  },
  {
    title: "Маркетинг",
    desc: `Хамтарсан маркетингийн үйлчилгээ`,
    icon: img4,
    bg: `bg-green-500`,
  },
  {
    title: "Борлуулалт",
    desc: `Борлуулалтын суваг нэмэгдүүлэх үйлчилгээ `,
    icon: img5,
    bg: `bg-orange-500`,
  },
  {
    title: "Агуулах",
    desc: `Бараа, бүтээгдэхүүн байршуулах боломжтой. `,
    icon: img6,
    bg: `bg-purple-500`,
  },
];

export const partners = [{ img: noyon }, { img: nama }, { img: tedy }, { img: easy }, { img: arur }];

export const links = [
  {
    title: "Эхлэл",
    icon: <BiSolidCategory />,
    path: "/dashboard",
  },
  {
    title: "Хүргэлт",
    icon: <TbTruckDelivery />,
    path: "/dashboard/delivery",
  },
  {
    title: "Мэдээ нэмэх",
    icon: <TbPlus />,
    path: "/dashboard/add-news",
  },
  {
    title: "Мэдээ жагсаалт",
    icon: <TbListDetails />,
    path: "/dashboard/news",
  },

  {
    title: "Хүргэлтийн ажилтан",
    icon: <BiUser />,
    path: "/dashboard/delivery-worker",
  },
  {
    title: "Харилцагч",
    icon: <RiCustomerServiceLine />,
    path: "/dashboard/vendor",
  },
];
