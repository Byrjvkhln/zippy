import { prisma } from "@/src/utils/prisma";
import { addAdmin } from "./adminservice";
export const addCustomer = async ({
  Name,
  Email,
  PhoneNumber,
  Color,
  TransportCost,
  Password,
  Location,
  Image,
  DriverSalary,
}: any) => {
  try {
    const result = await prisma.customer.create({
      data: { Name, Email, PhoneNumber, Color, TransportCost, Location, Image, DriverSalary },
    });
    await addAdmin({ Email, Password, Status: "customer" });

    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getALLCustomer = async ({ page, pageSize, app }: any) => {
  page = Number(page);
  pageSize = Number(pageSize);
  try {
    if (app) {
      const customers = await prisma.customer.findMany({});
      return { response: customers };
    } else {
      const customers = await prisma.customer.findMany({
        skip: (page - 1) * pageSize,
        take: pageSize,
        orderBy: {
          createdAt: "desc",
        },
      });
      return { response: customers };
    }
  } catch (error) {
    return { error };
  }
};

export const getALLCustomerWithDelivery = async () => {
  try {
    const customers = await prisma.customer.findMany();

    const customersWithDeliveryCounts = await Promise.all(
      customers.map(async (customer) => {
        const notDoneDeliveriesCount = await prisma.delivery.count({
          where: {
            CustomerId: customer.id,
            Status: {
              not: "done",
            },
          },
        });
        return {
          ...customer,
          too: notDoneDeliveriesCount,
        };
      })
    );
    return { response: customersWithDeliveryCounts };
  } catch (error) {
    return { error };
  }
};

export const changeCustomer = async ({ id, change }: any) => {
  try {
    const result = await prisma.customer.update({
      where: { id },
      data: { [change[0]]: change[1] },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getCustomerById = async (id: any) => {
  try {
    const result = await prisma.customer.findUnique({
      where: {
        id: id,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
