import { prisma } from "@/src/utils/prisma";
import bcrypt from "bcrypt";
// import { generateToken } from "../utils/tokenhandler";
import jwt from "jsonwebtoken";
export async function hashPassword(password: string) {
  const saltRounds = 10;
  const hashedPassword = await bcrypt.hash(password, saltRounds);
  return hashedPassword;
}
const SECRET_KEY = "Lol999za"; // Replace with your actual secret key

export const generateToken = (payload: any) => {
  return jwt.sign(payload, SECRET_KEY, { expiresIn: "1h" });
};
export async function comparePassword(password: string, hashedPassword: string) {
  const isMatch = await bcrypt.compare(password, hashedPassword);
  return isMatch;
}
export const addAdmin = async ({ Email, Password, Status }: any) => {
  try {
    const hashedPassword = await hashPassword(Password);
    const result = await prisma.admin.create({ data: { Email, Password: hashedPassword, Status } });
    return { response: result };
  } catch (error) {
    return { error };
  }
};

export const LoginAdmin = async ({ Email, Password }: any) => {
  try {
    const user = await prisma.admin.findUnique({ where: { Email } });

    if (!user) return { response: "User not found" };
    const result: boolean = await comparePassword(Password, user?.Password);

    if (result == true) {
      switch (user.Status) {
        case "driver":
          const driver = await prisma.driver.findUnique({
            where: { Email },
          });
          let a = await { ...driver, Status: user.Status };

          return { response: { data: a, token: generateToken(a) } };
        case "customer":
          const customer = await prisma.customer.findUnique({
            where: { Email },
          });

          const b = await { ...customer, Status: user.Status };
          return { response: { data: b, token: generateToken(b) } };
        case "admin":
          return { response: { data: user, token: generateToken(user) } };
      }
    } else {
      return { response: "Incorrect password" };
    }
  } catch (error) {
    return { error };
  }
};
export const getAllAdmin = async () => {
  try {
    const admins = await prisma.admin.findMany();
    return { response: admins };
  } catch (error) {
    return { error };
  }
};
