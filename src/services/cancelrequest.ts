import { prisma } from "@/src/utils/prisma";
export const addCancelRequest = async ({ Reason, deliveryId, TypeOfReq, DriverId, NextDate }: any) => {
  try {
    const result = await prisma.cancelReq.create({ data: { Reason, deliveryId, TypeOfReq, DriverId, NextDate } });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const changeRequest = async ({ id, change }: any) => {
  try {
    console.log({ id, change });

    const result = await Promise.all(
      change.map(async (el: any) => {
        const updatedRecord = await prisma.cancelReq.update({
          where: { id },
          data: { [el[0]]: el[1] },
        });
        return updatedRecord;
      })
    );

    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getAllRequests = async ({ page, pageSize, app }: any) => {
  page = Number(page);
  pageSize = Number(pageSize);
  try {
    if (app) {
      const requests = await prisma.cancelReq.findMany({
        include: {
          Delivery: true,
        },
      });
      return { response: requests };
    } else {
      const requests = await prisma.cancelReq.findMany({
        include: {
          Delivery: true,
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
        orderBy: {
          createdAt: "desc", // or 'asc'
        },
      });
      return { response: requests };
    }
  } catch (error) {
    return { error };
  }
};
export const getRequestByDriverId = async (driverId: any) => {
  try {
    const result = await prisma.cancelReq.findMany({
      where: {
        DriverId: driverId,
      },
      include: {
        Delivery: true,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getRequestById = async (id: any) => {
  try {
    const result = await prisma.cancelReq.findUnique({
      where: {
        id: id,
      },
      include: {
        Driver: true,
        Delivery: true,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
