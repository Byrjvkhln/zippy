import { prisma } from "@/src/utils/prisma";
export const createInvoice = async ({
  customerId,
  Items,
  Value,
  total,
  CustomerName,
  Interval,
  totaltransportcost,
}: any) => {
  try {
    const result = await prisma.invoice.create({
      data: {
        Items,
        Value,
        customerId,
        total,
        CustomerName,
        Interval: {
          Start: Interval.Start.split("T")[0],
          End: Interval.End.split("T")[0],
        },
        totaltransportcost,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getAllInvoice = async () => {
  try {
    let invoices = await prisma.invoice.findMany({
      orderBy: {
        createdAt: "desc", // or 'asc'
      },
    });
    return { response: invoices };
  } catch (error) {
    return { error };
  }
};
export const getInvoiceById = async (id: any) => {
  try {
    const result = await prisma.invoice.findUnique({
      where: {
        id: id,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const deliveriesByArray = async (data: []) => {
  try {
    const result = await prisma.delivery.findMany({
      where: {
        id: {
          in: data,
        },
      },
      include: {
        Driver: true,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const changeInvoice = async ({ id, change }: any) => {
  try {
    const result = await prisma.invoice.update({
      where: { id },
      data: { [change[0]]: change[1] },
    });

    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getInvoiceByCustomer = async (id: any) => {
  try {
    const result = await prisma.invoice.findMany({
      where: {
        customerId: id,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
