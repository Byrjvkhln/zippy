import { prisma } from "@/src/utils/prisma";
export const addDelivery = async ({
  CustomerId,
  Location,
  PhoneNumber,
  Status,
  DriverId,
  DeliveryDate,
  Product,
  Name,
  Price,
  PriceDetail,
  Street,
}: any) => {
  try {
    console.log({
      CustomerId,
      Location,
      PhoneNumber,
      Status,
      DriverId,
      DeliveryDate,
      Product,
      Name,
      Price,
      PriceDetail,
      Street,
    });

    const result = await prisma.delivery.create({
      data: {
        CustomerId,
        Location,
        PhoneNumber,
        Status,
        DriverId,
        DeliveryDate,
        Product,
        Name,
        Price,
        PriceDetail,
        Street,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getAllDelivery = async () => {
  try {
    const deliveries = await prisma.delivery.findMany({
      include: {
        Driver: true,
      },
    });
    return { response: deliveries };
  } catch (error) {
    return { error };
  }
};
export const getDeliverById = async (id: any) => {
  try {
    const result = await prisma.delivery.findUnique({
      where: {
        id: id,
      },
      include: {
        Driver: true,
        Customer: true,
      },
    });

    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getDeliverByCustomerId = async ({ customerId, page, pageSize, filter }: any) => {
  page = Number(page);
  pageSize = Number(pageSize);

  try {
    if (filter) {
      const result = await prisma.delivery.findMany({
        where: {
          CustomerId: customerId,
          Status: filter,
        },
        include: {
          Driver: true,
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
        orderBy: {
          createdAt: "desc", // or 'asc'
        },
      });
      return { response: result };
    } else {
      const result = await prisma.delivery.findMany({
        where: customerId !== "Not selected" ? { CustomerId: customerId } : undefined,
        include: {
          Driver: true,
        },
        skip: (page - 1) * pageSize,
        take: pageSize,
        orderBy: {
          createdAt: "desc", // or 'asc'
        },
      });
      return { response: result };
    }
  } catch (error) {
    return { error };
  }
};

export const changeDelivery = async ({ id, change }: any) => {
  console.log({ id, change });

  try {
    // if (!Array.isArray(id)) {
    const result = await Promise.all(
      change.map(async (el: any) => {
        const updatedRecord = await prisma.delivery.update({
          where: { id },
          data: { [el[0]]: el[1] },
        });
        return updatedRecord;
      })
    );

    return { response: result };

    // }
    // const result = await prisma.delivery.updateMany({
    //   where: { id: { in: id } },
    //   data: { [change[0]]: change[1] },
    // });

    // return { response: result };
  } catch (error) {
    return { error };
  }
};

export const getDeliverByDriverId = async ({ driverId }: any) => {
  try {
    const result = await prisma.delivery.findMany({
      where: {
        DriverId: driverId,
      },
      include: {
        Customer: true,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const deletealldelivery = async () => {
  await prisma.delivery.deleteMany({});
};
