import { prisma } from "@/src/utils/prisma";
export const updater = async () => {
  try {
    // Fetch all records
    const records = await prisma.delivery.findMany();

    for (const record of records) {
      // Define the new PriceDetail structure
      const priceDetail = {
        tulsun: record.Price || "0", // Use existing Price or set to "0"
        belen: "0",
      };

      // Update the record to remove District and add PriceDetail
      await prisma.delivery.update({
        where: { id: record.id },
        data: {
          PriceDetail: priceDetail, // Add PriceDetail
        },
      });
    }
  } catch (error) {
    console.error("Error updating records:", error);
  }
};
