import { prisma } from "@/src/utils/prisma";
export const addProducts = async ({ Name, BarCode, Price, Amount, Img, customerId, Change }: any) => {
  try {
    const result = await prisma.product.create({ data: { Name, BarCode, Price, Amount, Img, Change, customerId } });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getAllProducts = async () => {
  try {
    const products = await prisma.product.findMany({});
    return { response: products };
  } catch (error) {
    return { error };
  }
};
export const getProductById = async (id: any) => {
  try {
    const result = await prisma.product.findUnique({
      where: {
        id: id,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getProductByCustomerId = async ({ customerId }: any) => {
  try {
    const result = await prisma.product.findMany({
      where: {
        customerId: customerId,
      },
    });

    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const changeProduct = async ({ id, change }: any) => {
  try {
    if (change[0] === "all") {
      const result = await prisma.product.update({
        where: { id },
        data: {
          Name: change[1].Name,
          BarCode: change[1].BarCode,
          Img: change[1].Img,
          Price: change[1].Price,
          Amount: change[1].Amount,
        },
      });
      return { response: result };
    } else {
      const result = await prisma.product.update({
        where: { id },
        data: { [change[0]]: change[1] },
      });

      return { response: result };
    }
  } catch (error) {
    return { error };
  }
};
