import { prisma } from "@/src/utils/prisma";
export const addNews = async ({ Body, Title, Discription, Image, For }: any) => {
  try {
    const result = await prisma.news.create({ data: { Body, Title, Discription, Image, For } });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getAllNews = async () => {
  try {
    const news = await prisma.news.findMany({});
    return { response: news };
  } catch (error) {
    return { error };
  }
};
export const changeNews = async ({ id, change }: any) => {

  try {
    const result = await prisma.news.update({
      where: { id },
      data: { [change[0]]: change[1] },
    });

    return { response: result };
  } catch (error) {
    return { error };
  }
};