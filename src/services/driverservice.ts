import { prisma } from "@/src/utils/prisma";
import { addAdmin } from "./adminservice";
export const addDriver = async ({
  Surname,
  Name,
  Email,
  PhoneNumber,
  AccountName,
  Account,
  CarNumber,
  Password,
  Salary,
}: any) => {
  try {
    const result = await prisma.driver.create({
      data: {
        Surname,
        Name,
        Email,
        PhoneNumber,
        AccountName,
        Account,
        CarNumber,
        Salary,
        totalSalary: 0,
      },
    });
    await addAdmin({ Email, Password, Status: "driver" });
    return { response: result };
  } catch (error) {
    return { error };
  }
};

export const getAllDrivers = async ({ page, pageSize }: any) => {
  page = Number(page);
  pageSize = Number(pageSize);

  try {
    let drivers = await prisma.driver.findMany({
      skip: (page - 1) * pageSize,
      take: pageSize,
      orderBy: {
        createdAt: "desc", // or 'asc'
      },
    });
    drivers = await drivers.filter((el) => el?.id != "678623f3057973ab31d6ff1c");
    return { response: drivers };
  } catch (error) {
    return { error };
  }
};
export const getDriverById = async (id: any) => {
  try {
    const result = await prisma.driver.findUnique({
      where: {
        id: id,
      },
      include: {
        CancelReq: true,
      },
    });
    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const changeDriver = async ({ id, change }: any) => {
  console.log("psdaaaa");

  try {
    const result = await prisma.driver.update({
      where: { id },
      data: { [change[0]]: change[1] },
    });

    return { response: result };
  } catch (error) {
    return { error };
  }
};
export const getALLDriverWithDelivery = async () => {
  try {
    const customers = await prisma.driver.findMany({
      include: {
        Delivery: true,
      },
    });
    return { response: customers };
  } catch (error) {
    return { error };
  }
};
