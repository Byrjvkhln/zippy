export const convertToMongolianTime = (utcDate: any) => {
    const date = new Date(utcDate);
    const options: any = {
        timeZone: 'Asia/Ulaanbaatar',
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false, 
    };
  
    const dateString = date.toLocaleString('en-GB', options);

    const [datePart, timePart] = dateString.split(', ');
    const [day, month, year] = datePart.split('/');
    
    const formattedDate = `${year}/${month}/${day}`;

    return `${formattedDate} ${timePart}`;
};
