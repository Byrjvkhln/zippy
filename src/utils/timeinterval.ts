export const filtererTime = async (data: any, startDate: any, endDate: any) => {
  const start = new Date(startDate);
  const end = new Date(endDate);
  const result = await data.filter((el: any) => new Date(el.createdAt) >= start && new Date(el.createdAt) <= end);
  return result;
};
