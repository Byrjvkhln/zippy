import { imageDb } from "../config.js/firebase.config";
import { v4 } from "uuid";
import { ref, uploadBytes } from "firebase/storage";
export const ImageUploader = (img: any) => {
  const name = v4()
  const imgRef = ref(imageDb, name);
  uploadBytes(imgRef, img);

  return `https://firebasestorage.googleapis.com/v0/b/zippy-21ad8.appspot.com/o/${name}?alt=media&token=c2199654-7399-4613-9e46-157a814a2cab`;
};
