// middleware/authenticate.js
export default function authenticate(req: any, res: any, next: any) {
  const apiKey = req.headers["x-api-key"];

  if (apiKey !== process.env.API_KEY) {
    return res.status(403).json({ message: "Zaa bti davrad bgarai" });
  }

  next();
}
