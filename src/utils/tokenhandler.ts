"use client";
import jwt from "jsonwebtoken";
import { useEffect } from "react";
import { useRouter } from "next/navigation";
import { jwtDecode } from "jwt-decode";

export const useTokenHandler = () => {
  const router = useRouter();

  useEffect(() => {
    const token = localStorage.getItem("token");

    if (token) {
      const decodedToken: { exp: number } = jwtDecode(token);
      const currentTime = Date.now() / 1000;

      if (decodedToken.exp < currentTime) {
        localStorage.removeItem("token"); // Clear the expired token
        localStorage.removeItem("user");
        localStorage.removeItem("products");
        router.push("/"); // Redirect to the login page
      }
    } else {
      router.push("/"); // Redirect to the login page if no token is found
    }
  }, [router]);
};
