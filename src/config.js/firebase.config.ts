// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDWQcMZ8Ags1RZXHuXlqj0u8FY4dua0aSs",
  authDomain: "zippy-21ad8.firebaseapp.com",
  projectId: "zippy-21ad8",
  storageBucket: "zippy-21ad8.appspot.com",
  messagingSenderId: "389100218666",
  appId: "1:389100218666:web:1dcb0bc564864aac741653"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const imageDb = getStorage(app);
