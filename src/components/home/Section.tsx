import React from 'react'

const Section = ({children}:any) => {
  return (
    <section className={'max-w-[1532px] mx-auto w-[95%] py-12'}>
      {children}
    </section>
  )
}

export default Section
