import React from "react";
import Section from "./Section";
import Image from "next/image";
import Link from 'next/link'
import {
  FaLocationArrow,
  FaMailBulk,
  FaPhoneAlt,
  FaFacebookF,
  FaInstagram,
  FaTwitter,
} from "react-icons/fa";

const Footer = () => {
  return (
    <footer className="bg-[#407697] text-white">
      <Section>
        <div className="flex flex-col gap-10 sm:flex-row">
          <div className="flex-1">
            <div className="flex items-center gap-2 mb-11">
              <Image src="/logo.svg" alt="" width={30} height={30} />
              <p className="uppercase text-[14px]">
                zippy
                <span className="font-bold text-[#1AD75E]"> delivery</span>
              </p>
            </div>

            <p className="uppercase text-xl ">
              хүргэлтийн <span className="font-bold"> тань</span> <br />{" "}
              асуудлыг <span className="font-bold">бид</span> шийдье.
            </p>
          </div>
          <div className="flex-1 flex flex-col items-start space-y-4">
            <p className="font-bold text-2xl mb-10">Холбоо барих</p>

            <div className="flex items-center gap-6 border-b pb-3 ">
              <FaLocationArrow className="self-start mt-2" />
              <p className="text-[#cfcfcf]">
                Чд, 8-р хороо, Компьютер молл цогцолбор, 13д-1316
              </p>
            </div>
            <div className="flex items-center gap-6">
              <FaMailBulk className="self-start mt-2" />
              <p className="text-[#cfcfcf]">info@zippy.mn</p>
            </div>
            <div className="flex items-center gap-6">
              <FaPhoneAlt className="self-start mt-2" />
              <p className="text-[#cfcfcf]">+976 7600-2001, +976 9915-0719</p>
            </div>
            <div className="flex gap-7">
              <Link
                href=""
                className="size-8 border-2 rounded-full flex items-center justify-center hover:opacity-50 transition-opacity duration-300 "
              >
                <FaFacebookF />
              </Link>
              <Link
                href=""
                className="size-8 border-2 rounded-full flex items-center justify-center hover:opacity-50 transition-opacity duration-300 "
              >
                <FaInstagram />
              </Link>
              <Link
                href=""
                className="size-8 border-2 rounded-full flex items-center justify-center hover:opacity-50 transition-opacity duration-300 "
              >
                <FaTwitter />
              </Link>
            </div>
          </div>
        </div>
      </Section>
    </footer>
  );
};

export default Footer;
