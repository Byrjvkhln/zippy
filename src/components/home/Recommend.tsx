import React from "react";
import Section from "./Section";
import { data } from "../../../data";
import Card from "./Card";

const Recommend = () => {
  return (
    <Section>
      <div>
        <h4 className="text-main font-bold text-[26px] text-center mb-28">
          Бид танд дараах зүйлсийг санал болгож байна
        </h4>

        <div className="grid sm:grid-cols-2 lg:grid-cols-3 gap-10 gap-y-20">
          {data.map((d) => (
            <Card {...d} key={d.title} />
          ))}
        </div>
      </div>
 
    </Section>
  );
};

export default Recommend;
