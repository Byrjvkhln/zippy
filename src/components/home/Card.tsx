import React from 'react'
import Image from 'next/image'
import {twMerge as tw} from 'tailwind-merge'

const Card = ({title, desc, icon, bg}:any) => {
 
  return (
    <div className="border relative px-4 xl:px-11  rounded-tr-[100px] pt-16 h-[270px]  border-[#6d6d6d] text-[20px] text-secondary">
      <div>
        {" "}
        <p className="font-bold mb-6">{title}</p>
        <p className="text-balance">{desc}</p>
      </div>

      <div
      style={{background: bg}}
        className={
          `absolute xl:size-[100px] size-20 rounded-[20px]  flex items-center justify-center  p-4 -top-10 xl:-top-[54px]`}
      >
        <Image
          src={icon}
          alt=""
          width={46}
          height={46}
          className="w-auto h-auto"
        />
      </div>
    </div>
  );
}

export default Card
