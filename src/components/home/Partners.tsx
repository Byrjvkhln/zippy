import React from 'react'
import Section from './Section'
import Image from 'next/image'
import {partners} from '../../../data'

const Partners = () => {
  return (
    <Section>
      <h4 className='text-main font-bold text-[26px] text-center mb-14'>Хамтрагч байгууллага</h4>

      <div className='grid sm:grid-cols-2 lg:grid-cols-3 gap-10'>
        {partners.map((p, i )=> (
            <div key={i} className={`first:sm:row-span-2 border first:border-none rounded-[40px] overflow-hidden border-black ${i === 2 && 'border-none'} `}>
                <Image src={p.img} alt=''/>
            </div>

        ))}
        
      </div>
    </Section>
  )
}

export default Partners
