import React from 'react'
import Section from './Section'
import Image from 'next/image'

const AboutUs = () => {
  return (
    <Section>
      <div className="flex gap-10 items-center flex-col 2xl:flex-row">
        <div className='flex flex-col gap-10 items-center lg:flex-row '>
          <Image
            src="/img2.png"
            alt=""
            width={500}
            height={500}
            className="w-[377px] h-[413px] s"
          />
          <Image
            src="/img3.svg"
            alt=""
            width={500}
            height={500}
            className="w-[496px] h-fit"
          />
        </div>

        <div className="flex-1">
          <h4 className="text-main font-bold text-[26px] mb-6 text-center 2xl:text-left">Бидний тухай</h4>
          <p className="font-light text-xl">
            Манай баг мэдээлэл технологийн салбарт сүүлийн 5 жил ажилласан
            тушлагатай бөгөөд өнгөрсөн хугацаанд олон төрлийн томоохон
            системүүдийг зах зээлд амжилттай нэвтрүүлээд байгаа билээ. Бид
            өөрсдийн туршлага, орчин цагийн хэрэгцээнд нийцүүлэн технологид
            суурилсан шуурхай хүргэлтийн цогц шийдэл, үйлчилгээг үе шаттайгаар
            танд хүргэж байгаадаа таатай байна.
          </p>
        </div>
      </div>
    </Section>
  );
}

export default AboutUs
