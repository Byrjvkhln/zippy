import React from 'react'
import Image from 'next/image'

const NoData = ({text}:any) => {
  return (
    <div className="mt-20 flex justify-center">
      <div>
        <Image
          src="/nodata.svg"
          alt=""
          width={500}
          height={500}
          className="w-28 h-fit object-cover mb-5 mx-auto "
        />
        <p className='text-center text-lg font-medium '>{text ? text : "Одоогоор мэдээлэл байхгүй."}</p>
      </div>
    </div>
  );
}

export default NoData
