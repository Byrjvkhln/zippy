// PrintComponent.tsx
import React, { forwardRef, useState } from "react";
import { useEffect } from "react";
import Image from "next/image";
import axios from "axios";
interface PrintComponentProps {
  invoice?: any; // Adjust the type based on your actual invoice structure
}
const StatusColor: any = {
  Баталсан: "text-[#008767] border-[#008767] bg-[#A1E5E2]",
  "Хүлээгдэж байна": "text-[#FFC107] border-[#FFC107] bg-[#FFF9C4]",
  Цуцлагдсан: "text-[#DF0404] border-[#DF0404] bg-[#FFC5C5]",
};
const PrintComponent = forwardRef<HTMLDivElement, PrintComponentProps>(({ invoice }: any, ref) => {
  const [deliveries, setDeliveries] = useState([]);
  const deliverieshandler = async () => {
    const result = await axios.post("/api/invoice/deliveryimfos", {
      data: invoice?.Items,
    });

    const deliveries = await result?.data?.map((el: any) => {
      el.Product = el?.Product.map((el: any) => {
        return JSON.parse(el);
      });
      return el;
    });

    setDeliveries(deliveries);
  };
  useEffect(() => {
    if (invoice) {
      deliverieshandler();
    }
  }, [invoice]);

  return (
    <div className=" p-10 flex flex-col gap-10 bg-white rounded-lg " ref={ref}>
      <div className="flex justify-between">
        <div className="flex flex-col gap-10">
          <div className="flex items-center   ">
            <Image src={`/logo.svg`} alt="" width={60} height={60} />

            <div className=" font-bold   text-gray flex">
              ZIPPY <p className="text-[#51ba5b]"> DELIVERY</p>
            </div>
          </div>
          <div className=" flex flex-col gap-2">
            <div className="text-[17.5px] font-semibold">VIRASOFT LLC</div>
            {/* <div className="text-[15px] text-[#808080] ">ID:66e8ff279bf51dbf9a463966</div> */}
            <div className="text-[15px] text-[#808080] ">virasoft@gmail.com | +976 96652141</div>
            <div className="text-[15px] text-[#808080]   flex justify-between">
              <div>БАЙГУУЛЛАГА:</div>
              <div>{invoice?.CustomerName}</div>
            </div>
          </div>
        </div>
        <div className="flex flex-col gap-5 justify-end">
          <div className="text-[25px] font-bold text-right">НЭХЭМЖЛЭЛ</div>

          <div className="text-right flex flex-col gap-2">
            {/* <div className="text-[15px] font-thin">ID:66e8ff279bf51dbf9a463966</div> */}
            <div className="text-[15px] text-[#808080]   flex justify-between">
              <div>ID:</div>
              <div>{invoice?.id}</div>
            </div>
            <div className="text-[15px] text-[#808080]   flex justify-between">
              <div>ҮҮСГЭСЭН ОГНОО:</div>
              <div>{invoice?.createdAt.split("T")[0]}</div>
            </div>
            <div className="text-[15px] text-[#808080]   flex justify-between gap-10">
              <div>ХООРОНД:</div>
              <div>{`${invoice?.Interval?.Start} -> ${invoice?.Interval?.End}`}</div>
            </div>
            <div className="text-[15px] text-[#808080]   flex justify-between items-center gap-10">
              <div>ТӨЛӨВ:</div>
              <div className={`rounded-md p-2  ${StatusColor[invoice?.Status]}  border-[1px] `}>{invoice?.Status}</div>
            </div>
          </div>
        </div>
      </div>
      <table className="w-full border border-[#DFE4EA] rounded-lg">
        <thead>
          <tr className="border-b border-[#DFE4EA]">
            <th>
              <div className="text-[12.5px] h-[50px] flex items-center px-5">УТАС</div>
            </th>
            <th>
              <div className="text-[12.5px] h-[50px] flex items-center px-5">ХҮРГЭГЧ</div>
            </th>
            <th>
              <div className="text-[12.5px] h-[50px] flex items-center px-5">BAR CODE</div>
            </th>
            <th>
              <div className="text-[12.5px] h-[50px] flex items-center px-5">ШИРХЭГ</div>
            </th>
            <th>
              <div className="text-[12.5px] h-[50px] flex items-center px-5">ҮНЭ</div>
            </th>
            <th>
              <div className="text-[12.5px] h-[50px] flex items-center px-5">ОГНОО</div>
            </th>
          </tr>
        </thead>
        <tbody>
          {deliveries?.map((el: any) => {
            return (
              <tr key={el?.id} className="border-b border-[#DFE4EA]">
                <td>
                  <div className="text-[12.5px] h-[50px] flex items-center px-5">{el?.PhoneNumber}</div>
                </td>
                <td>
                  <div className="text-[12.5px] h-[50px] flex items-center px-5">{el?.Driver.Name}</div>
                </td>
                <td>
                  {el?.Product?.map((el: any, i: any) => {
                    return (
                      <div key={i} className="text-[12.5px]  flex items-center px-5">
                        {el?.BarCode}
                      </div>
                    );
                  })}
                </td>
                <td>
                  <div className="text-[12.5px] h-[50px] flex items-center px-5">
                    {el?.Product?.map((el: any, i: any) => {
                      return (
                        <div key={i} className="text-[12.5px]  flex items-center px-5">
                          {el?.too}
                        </div>
                      );
                    })}
                  </div>
                </td>
                <td>
                  {el?.Product?.map((el: any, i: any) => {
                    return (
                      <div key={i} className="text-[12.5px]  flex items-center  px-5">
                        {el?.Price * el?.too}₮ {"("}{" "}
                        <p>
                          {el?.PriceDetail?.tulsun}+{el?.PriceDetail?.belen}
                        </p>
                      </div>
                    );
                  })}
                </td>
                <td>
                  <div className="text-[12.5px] h-[50px] flex items-center px-5">{el?.createdAt?.split("T")[0]}</div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className=" flex justify-end">
        <div className="flex flex-col gap-5 justify-end w-[50%] bg-[#F9F9FA] p-5 border border-[#DFE4EA] rounded-lg">
          <div className="text-right flex flex-col gap-2">
            <div className="text-[15px] text-[#808080]   flex justify-between">
              <div> НИЙТ БАРААНЫ ҮНЭ:</div>
              <div>{invoice?.total}₮</div>
            </div>
            <div className="text-[15px] text-[#808080]   flex justify-between">
              <div>НИЙТ ХҮРГЭЛТИЙН ТОО:</div>
              <div>{invoice?.Value}</div>
            </div>
            <div className="text-[15px]    flex  flex-col text-left">
              <div>
                {`${invoice?.Interval?.Start} -> ${invoice?.Interval?.End}`} хооронд хийсэн хүргэлтийн нийт төлбөр:
              </div>
              <div className="text-right text-[#28a745] text-lg">{invoice?.totaltransportcost}₮</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

// Set the display name
PrintComponent.displayName = "PrintComponent";

export default PrintComponent;
