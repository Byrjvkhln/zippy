"use client";

import React from "react";
import { Drawer } from "@material-tailwind/react";
import CartProduct from "./CartProduct";
import { AiOutlineClose } from "react-icons/ai";

const Cart = ({ open, setOpen, products, NumberChanger, handleClick, sumPrice, user }: any) => {
  const handleClose = () => {
    setOpen(!open);
  };

  return (
    <Drawer
      {...({} as any)}
      size={420}
      placement="right"
      open={open}
      onClose={handleClose}
      className=" overflow-y-auto "
    >
      {products.filter((el: any) => el.too > 0).length > 0 && (
        <>
          {" "}
          <div
            className={`flex flex-col   pb-6
               h-full 
          `}
          >
            <div className=" flex items-baseline justify-between mb-5 px-4 pt-5">
              <h4 className="font-semibold text-xl">Захиалга</h4>

              <AiOutlineClose className="text-lg" onClick={handleClose} />
            </div>
            <hr />
            <div className="border-b flex-1 overflow-auto pt-4" onClick={(e: any) => e.stopPropagation()}>
              {products.map((s: any, i: any) => {
                if (s.too > 0) {
                  return <CartProduct NumberChanger={NumberChanger} {...s} key={i} />;
                }
              })}
            </div>

            <div className=" pt-5 text-base *:mb-2 px-4">
              <div className="flex justify-between">
                <p className="text-gray-600">Нийт </p>
                <p className="font-medium">
                  {sumPrice?.toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })}
                  ₮{" "}
                </p>
              </div>
              <div className="flex justify-between">
                <p className="text-gray-600">Хүргэлтийн төлбөр </p>
                <p className="font-medium">
                  {Number(user.TransportCost)?.toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })}
                  ₮{" "}
                </p>
              </div>
              <div className="flex justify-between">
                <p className="text-gray-600">Төлөв </p>
                <p className="font-medium">Хүргэлт</p>
              </div>

              <div className="border-b pb-4 border-gray-400">
                <p className="text-gray-600">0.00₮</p>
              </div>
            </div>
            <div>
              <div className="flex items-center justify-between mb-4 font-semibold text-base px-4">
                <p>Нийт төлбөр:</p>
                <p>
                  {" "}
                  {sumPrice?.toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })}
                  ₮{" "}
                </p>
              </div>
              <div className="px-4">
                <button
                  onClick={handleClick}
                  className=" bg-blue-900 w-full px-4 py-2 text-white rounded-lg text-base font-medium hover:bg-main transition-colors duration-300"
                >
                  Үргэлжлүүлэх
                </button>
              </div>
            </div>
          </div>
        </>
      )}
    </Drawer>
  );
};

export default Cart;
