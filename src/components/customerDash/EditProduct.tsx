"use client";

import React, { useRef, useState, useEffect } from "react";
import {
  Button,
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
} from "@material-tailwind/react";
import Image from "next/image";
import axios from "axios";
import { GoPencil } from "react-icons/go";

const EditProduct = ({ open, setOpen, product }: any) => {
  const file: any = useRef(null);
  const [img, setImg] = useState<any>(null);
  const [proName, setProName] = useState(product.Name);
  const [loading, setLoading] = useState(false);
  const [proBarCode, setProBarCode] = useState(product.BarCode);
  const [proPrice, setProPrice] = useState(product.Price);
  const [image, setImage] = useState<any>(product?.Img || "");
  const handler = () => {
    setOpen(!open);
  };

  useEffect(() => {
    if (img) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setImage(reader.result);
      };
      reader.readAsDataURL(img);
    }
  }, [img]);

  const handleImageChange = (e: any) => {
    if (e.target.files && e.target.files[0]) {
      setImg(e.target.files[0]);
    }
  };

  const productChanger = async (change: any) => {
    setLoading(true);
    const res = await axios.put("  /api/product", {
      id: product.id,
      change,
    });

    if (res.status === 200) {
      setOpen(!open);
      setLoading(false);
    }
  };

  return (
    <Dialog {...({} as any)} open={open} size="xs" handler={handler}>
      <DialogHeader {...({} as any)}>
        <div className="flex items-center justify-between w-full"></div>
      </DialogHeader>
      <DialogBody {...({} as any)} className="text-gray-900">
        <div className="mb-5">
          <p className="mb-2">Нэр</p>
          <input
            defaultValue={product?.Name}
            type="text"
            className="border w-full border-gray-400 px-4 py-2 rounded-lg outline-none"
            onChange={(e: any) => setProName(e.target.value)}
          />
        </div>
        <div className="mb-5">
          <p className="mb-2">Баркод /Optional/</p>
          <input
            defaultValue={product?.BarCode}
            type="text"
            className="border w-full border-gray-400 px-4 py-2 rounded-lg outline-none"
            onChange={(e: any) => setProBarCode(e.target.value)}
          />
        </div>
        <div className="mb-5">
          <p className="mb-2">Үнийн дүн</p>
          <input
            defaultValue={product?.Price}
            type="text"
            className="border w-full border-gray-400 px-4 py-2 rounded-lg outline-none"
            onChange={(e: any) => setProPrice(e.target.value)}
          />
        </div>

        <div className="">
          <label htmlFor="uploadImg">
            <div className="size-40 border rounded-full border-gray-400 flex items-center justify-center cursor-pointer overflow-hidden relative mx-auto">
              <Image
                src={image}
                alt=""
                width={100}
                height={120}
                className="w-full h-full object-cover"
              />

              <div className="absolute inset-0 bg-black/70 flex items-center justify-center">
                <GoPencil className="text-3xl text-white" />
              </div>
            </div>
          </label>
          <input
            ref={file}
            type="file"
            id="uploadImg"
            className="hidden"
            onChange={handleImageChange}
          />
        </div>
      </DialogBody>
      <DialogFooter {...({} as any)}>
        <Button
          {...({} as any)}
          variant="outlined"
          className="mr-1 normal-case text-base py-2"
          onClick={handler}
        >
          <span>Хаах</span>
        </Button>
        <Button
          {...({} as any)}
          className="normal-case text-base bg-main hover:bg-blue-900 py-2 border border-main"
          onClick={() =>
            productChanger([
              "all",
              {
                Name: proName,
                BarCode: proBarCode,
                Img: image,
                Price: proPrice,
              },
            ])
          }
        >
          <span>{loading ? "Уншиж байна..." : "Хадгалах"}</span>
        </Button>
      </DialogFooter>
    </Dialog>
  );
};

export default EditProduct;
