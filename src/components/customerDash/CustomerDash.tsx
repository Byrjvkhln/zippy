"use client";

import React, { useEffect, useState } from "react";
import { IoMdSearch } from "react-icons/io";
import { BsCart2 } from "react-icons/bs";
import { FaPlus } from "react-icons/fa6";
import axios from "axios";
import { useRouter } from "next/navigation";
import Link from "next/link";
import Product from "./Product";
import CartProduct from "./CartProduct";
import Cart from "./Cart";
import Loader from "../wait/Loader";
import NoData from "../NoData";

const CustomerDash = ({ user }: any) => {
  const router = useRouter();
  const [products, setProducts] = useState([]);
  const [showproducts, setShowproducts] = useState([]);
  const [basket, setBasket] = useState<any>([]);
  const [sum, setSum] = useState<number>();
  const [sumPrice, setSumPrice] = useState<number>();
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchProducts = async () => {
      const res: any = await axios.post("/api/product/customerid", {
        customerId: JSON.parse(window.localStorage.getItem("user") || "{}").id,
      });

      setProducts(
        await res?.data.map((el: any, i: any) => {
          return { ...el, too: 0 };
        })
      );
      if (res.status === 200) setLoading(false);
    };

    fetchProducts();
  }, []);

  const NumberChanger = (id: any, nemeh: any) => {
    const newporducts: any = products.map((el: any) => {
      if (el.id == id) {
        return nemeh
          ? { ...el, too: el.too + 1, Amount: Number(el.Amount) - 1 }
          : { ...el, too: el.too - 1, Amount: Number(el.Amount) + 1 };
      }
      setBasket([...basket, el]);
      return el;
    });
    setProducts(newporducts);
  };
  useEffect(() => {
    let a: number = 0;
    let b: number = 0;
    products.map((el: any) => {
      a = a + Number(el?.too);
      b = b + Number(el?.Price * el?.too);
    });
    setSum(a);
    setSumPrice(b);

    localStorage.setItem(
      "products",
      JSON.stringify(
        products.map((el: any) => {
          if (el.too > 0) {
            return el;
          }
        })
      )
    );
  }, [products]);

  const clear = () => {
    let newporducts: any = products.map((el: any) => {
      return { ...el, too: 0 };
    });
    setProducts(newporducts);
  };

  const handleClick = () => {
    router.push("/dashboard/orders");
  };
  useEffect(() => {
    setShowproducts(products);
  }, [products]);
  return (
    <div className="">
      {user?.Status === "customer" && (
        <div className="flex flex-col md:flex-row md:items-center justify-between gap-5 mb-10">
          <div className="font-semibold">
            <h2 className="font-bold text-2xl mb-2">{user?.Name}</h2>
            <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
              Эхлэл
            </Link>{" "}
            <ul className="inline-block text-gray-500 list-disc">
              <li>Захиалга үүсгэх</li>
            </ul>
          </div>

          <div className="flex items-center gap-5  justify-between">
            <div className="flex w-[50%] sm:w-fit items-center shadow-md px-3 py-2 rounded-lg gap-2 bg-white">
              <IoMdSearch className="text-lg" />
              <input
                onChange={(e) =>
                  setShowproducts(
                    products.filter((el: any) => el.Name.toUpperCase().includes(e.target.value.toUpperCase()))
                  )
                }
                type="text"
                className=" outline-none w-full"
                placeholder="Бараа хайх"
              />
            </div>
            <Link
              href="/dashboard/create"
              className="flex shrink-0 items-center gap-2 bg-main px-4 py-2 text-white font-semibold rounded-lg hover:bg-blue-900 transition-colors duration-300"
            >
              <FaPlus className="text-lg" /> Бараа нэмэх
            </Link>
          </div>
        </div>
      )}

      {loading ? (
        <Loader color="#183770" />
      ) : (
        <div className="flex flex-col sm:flex-row gap-5">
          <div className="lg:flex-[2] xl:flex-[2.5] 2xl:flex-[2] 3xl:flex-[3] sm:flex-1 @container ">
            {products.length > 0 ? (
              <div className="grid @3xl:grid-cols-3 @sm:grid-cols-2 @5xl:grid-cols-4 @8xl:grid-cols-5 gap-5">
                {showproducts?.map((p: any, i: any) => {
                  return <Product {...p} key={i} NumberChanger={NumberChanger} />;
                })}
              </div>
            ) : (
              <NoData text="Одоогоор бараа алга." />
            )}
          </div>
          {products.filter((el: any) => el.too > 0).length > 0 && (
            <>
              <div
                className={` border border-gray-400 rounded-lg pb-6
             sm:flex-[1] self-start bg-white h-fit hidden lg:block 
          `}
              >
                <div className=" flex items-baseline justify-between mb-5 p-4">
                  <div className="flex flex-col md:flex-row items-baseline gap-1 2xl:flex-col 3xl:flex-row">
                    <h2 className="font-semibold text-base xl:text-lg">
                      Захиалга үүсгэх ({products.filter((el: any) => el.too > 0).length}){" "}
                    </h2>
                    <p className="font-semibold">{sum} ш</p>
                  </div>

                  <p className="font-semibold text-gray-600 cursor-pointer" onClick={clear}>
                    Цэвэрлэх
                  </p>
                </div>
                <div className="border-b h-[400px] overflow-auto">
                  {products.map((s: any, i: any) => {
                    if (s.too > 0) {
                      return <CartProduct NumberChanger={NumberChanger} {...s} key={i} />;
                    }
                  })}
                </div>

                <div className="p-4 lg:p-5 lg:pb-2 text-base *:mb-2 ">
                  <div className="flex justify-between">
                    <p className="text-gray-600">Нийт </p>
                    <p className="font-semibold text-gray-800">
                      {sumPrice?.toLocaleString("en-US", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                      ₮{" "}
                    </p>
                  </div>
                  <div className="flex justify-between">
                    <p className="text-gray-600">Хүргэлтийн төлбөр </p>
                    <p className="font-semibold text-gray-800">{user.TransportCost}₮ </p>
                  </div>
                  <div className="flex justify-between">
                    <p className="text-gray-600">Төлөв </p>
                    <p className="font-semibold text-gray-800">Хүргэлт</p>
                  </div>

                  <div className="border-b pb-4 border-gray-400">
                    <p className="text-gray-600">0.00₮</p>
                  </div>
                </div>
                <div className="px-4 lg:px-5">
                  <div className="flex items-center justify-between mb-4 font-semibold text-base">
                    <p>Нийт төлбөр:</p>
                    <p>
                      {" "}
                      {sumPrice?.toLocaleString("en-US", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                      ₮{" "}
                    </p>
                  </div>
                  <button
                    onClick={handleClick}
                    className=" bg-main w-full px-4 py-2 text-white rounded-lg text-base font-semibold hover:bg-blue-900 transition-colors duration-300"
                  >
                    Үргэлжлүүлэх
                  </button>
                </div>
              </div>
              <div
                className="h-20 w-32 absolute right-3 bg-blue-900 rounded-lg shadow-lg flex items-center justify-center lg:hidden"
                onClick={() => setOpen(!open)}
              >
                <div className="text-white font-semibold flex flex-col items-center gap-2">
                  <BsCart2 className="text-2xl" />
                  {sum} бараа
                </div>
                {open && (
                  <Cart
                    open={open}
                    setOpen={setOpen}
                    products={products}
                    NumberChanger={NumberChanger}
                    clear={clear}
                    handleClick={handleClick}
                    sumPrice={sumPrice}
                    user={user}
                  />
                )}
              </div>
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default CustomerDash;
