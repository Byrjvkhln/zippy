"use client";

import React, { useState, useRef, useEffect } from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";
import { Avatar } from "@material-tailwind/react";

const CustomerNav = ({ user }: any) => {
  const [open, setOpen] = useState(false);
  const router = useRouter();

  const dropdownRef = useRef<any>(null);

  const handleClickOutside = (event: MouseEvent) => {
    if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
      setOpen(false);
    }
  };

  useEffect(() => {
    if (open) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [open]);

  const logout = () => {
    router.push("/login");
    localStorage.removeItem("user");
    localStorage.removeItem("products");
    setOpen(false);
  };

  return (
    <div>
      <div className="relative rounded-full " ref={dropdownRef}>
        <div
          onClick={() => setOpen(!open)}
          className="cursor-pointer size-12 rounded-full flex items-center justify-center bg-gray-300"
        >
          <Avatar {...({} as any)} src={user?.Image} className="border" />
        </div>

        {open && (
          <div className="absolute z-30 right-0 top-full bg-white shadow-lg mt-4 rounded-lg w-[300px] border">
            <div className="p-4 border-b flex items-center gap-4">
              <div className="size-12 rounded-full flex items-center justify-center border bg-gray-300">
                <Avatar {...({} as any)} src={user?.Image} />
              </div>
              <div>
                <p className="text-xl uppercase font-semibold"> {user?.Name}</p>
                <p> {user?.Email}</p>
              </div>
            </div>
            <div className=" border-b" onClick={() => setOpen(false)}>
              {/* <Link
                href="/dashboard/profile-settings"
                className="block hover:bg-gray-200 transition-colors duration-300 px-8 py-4 cursor-pointer"
              >
                Бүртгэл
              </Link> */}
              {user?.Status === "admin" && (
                <p className="hover:bg-gray-200 px-8 py-4 cursor-pointer transition-colors duration-300">
                  Үйл ажиллагааны түүх
                </p>
              )}
            </div>
            <div
              className="py-4 px-8 border-b cursor-pointer hover:bg-gray-200 transition-colors duration-300"
              onClick={logout}
            >
              Гарах
            </div>
            <div className="size-6 absolute bg-white border right-3 z-40 -top-2 rotate-45 rounded"></div>
            <div className="h-6 w-12 absolute bg-white right-0 z-40 top-0 rounded-lg"></div>
          </div>
        )}
      </div>
    </div>
  );
};

export default CustomerNav;
