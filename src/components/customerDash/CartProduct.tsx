import React, { useEffect } from "react";
import Image from "next/image";
import { FaPlus, FaMinus } from "react-icons/fa6";
import { RiDeleteBin6Line } from "react-icons/ri";


const CartProduct = ({ Img, Price, Name, too, id, NumberChanger}: any) => {


  return (
    <div className="mb-5 px-4 lg:px-5 border-b pb-5 border-gray-400 last:border-none flex justify-between">
      <div className="flex items-center gap-3">
        <div className="relative size-16 rounded-lg overflow-hidden shrink-0">
          <Image
            src={Img}
            alt=""
            width={100}
            height={100}
            className="w-full h-full object-cover"
          />

          <div className="absolute inset-0 bg-black/50 opacity-0 hover:opacity-100 flex items-center justify-center transition-all duration-300">
            <RiDeleteBin6Line className="text-white text-2xl" />
          </div>
        </div>
        <div>
          <p className="font-semibold line-clamp-1">{Name && Name}</p>
          <p>
            {Price && Price}₮ x {too}
          </p>
          <p className="font-semibold">
            {(Price * too).toLocaleString("en-US", {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2,
            })}
            ₮
          </p>
        </div>
      </div>

      {NumberChanger && (
        <div className="self-end">
          <div className="flex gap-2 xl:gap-4 items-center">
            <div
              onClick={() => NumberChanger(id, false)}
              className="p-1 size-7 flex items-center justify-center rounded hover:bg-gray-300 transition-colors duration-300"
            >
              <FaMinus />
            </div>
            <p className=" font-medium">{too}</p>
            <div
              onClick={() => NumberChanger(id, true)}
              className="p-1 size-7 flex items-center justify-center rounded hover:bg-gray-300 transition-colors duration-300"
            >
              <FaPlus />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default CartProduct;
