"use client";

import React from "react";
import { FaPlus, FaMinus } from "react-icons/fa6";
import Image from "next/image";
import Link from "next/link";

const Product = ({ id, Name, Price, Img, Amount, BarCode, too, NumberChanger }: any) => {
  return (
    <div className="rounded-lg relative bg-white shadow-md">
      <p className="absolute top-3 left-3 bg-[#86efac] px-4 py-2 rounded text-gray-600 font-semibold">
        {Number(Amount)} ширхэг
      </p>
      <div className=" h-[300px] rounded-lg bg-gray-200 ">
        <Image
          src={Img ? Img : ""}
          alt=""
          width={500}
          height={500}
          className="w-full h-full object-cover rounded-t-lg"
          priority
        />
      </div>
      <div className="p-5 b">
        <Link
          href={`/dashboard/${id}`}
          className="font-semibold text-lg mb-2 line-clamp-1"
        >
          {Name}
        </Link>
        <p className="text-gray-700 mb-2">Баркод: {BarCode}</p>
        <p className="font-semibold text-base mb-2">
          {Number(Price).toLocaleString()}₮
        </p>
        <button
          className={`w-full border h-10 px-1 border-gray-400 rounded-lg hover:border-blue-900 hover:text-blue-900 transition-colors duration-300 `}
        >
          {too != 0 ? (
            <div className="flex justify-between items-center">
              <div
                onClick={() => NumberChanger(id, false)}
                className="p-1 size-7 flex items-center justify-center rounded hover:bg-gray-300 transition-colors duration-300"
              >
                <FaMinus />
              </div>
              <p className="text-base font-medium">{too}</p>
              <div
                onClick={() => NumberChanger(id, true)}
                className="p-1 size-7 flex items-center justify-center rounded hover:bg-gray-300 transition-colors duration-300"
              >
                <FaPlus />
              </div>
            </div>
          ) : (
            <div onClick={() => NumberChanger(id, true)}>Сагслах</div>
          )}
        </button>
      </div>
    </div>
  );
};

export default Product;
