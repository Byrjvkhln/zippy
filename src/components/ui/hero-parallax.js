"use client";
import React from "react";
import { motion, useScroll, useTransform, useSpring } from "framer-motion";
import { TextGenerateEffect } from "@/src/components/ui/text-generate-effect";

import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";


export const products = [
  {
    title: "ZIPPY",
    link: "/",
    thumbnail: "/icon.jpg",
  },
  {
    title: "Нүүр хуудас #2",
    link: "/",
    thumbnail: "/ani2.jpeg",
  },
  {
    title: "Нүүр хуудас #3",
    link: "/",
    thumbnail: "/ani3.jpeg",
  },

  {
    title: "Нүүр хуудас #1",
    link: "/",
    thumbnail: "/ani1.jpeg",
  },
  {
    title: "Нүүр хуудас #2",
    link: "/",
    thumbnail: "/ani2.jpeg",
  },
  {
    title: "Нүүр хуудас #3",
    link: "/",
    thumbnail: "/ani3.jpeg",
  },

  {
    title: "Нүүр хуудас #1",
    link: "/",
    thumbnail: "/ani1.jpeg",
  },
  {
    title: "Нүүр хуудас #2",
    link: "/",
    thumbnail: "/ani2.jpeg",
  },
  {
    title: "Нүүр хуудас #3",
    link: "/",
    thumbnail: "/ani3.jpeg",
  },
  {
    title: "ZIPPY",
    link: "/",
    thumbnail: "/icon.jpg",
  },
  {
    title: "Нүүр хуудас #2",
    link: "/",
    thumbnail: "/ani2.jpeg",
  },
];


export const HeroParallax = () => {
  const firstRow = products.slice(0, 5);
  const secondRow = products.slice(5, 10);
  const thirdRow = products.slice(10, 15);
  const ref = React.useRef(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ["start start", "end start"],
  });

  const springConfig = { stiffness: 300, damping: 30, bounce: 100 };

  const translateX = useSpring(useTransform(scrollYProgress, [0, 1], [0, 1000]), springConfig);
  const translateXReverse = useSpring(useTransform(scrollYProgress, [0, 1], [0, -1000]), springConfig);
  const rotateX = useSpring(useTransform(scrollYProgress, [0, 0.2], [15, 0]), springConfig);
  const opacity = useSpring(useTransform(scrollYProgress, [0, 0.2], [0.2, 1]), springConfig);
  const rotateZ = useSpring(useTransform(scrollYProgress, [0, 0.2], [20, 0]), springConfig);
  const translateY = useSpring(useTransform(scrollYProgress, [0, 0.2], [-700, 0]), springConfig);
  return (
    (<div
      ref={ref}
      className="h-[180vh] py-40 overflow-hidden antialiased relative flex flex-col self-auto [perspective:1000px] [transform-style:preserve-3d]">
      <Header />
      <motion.div
        style={{
          rotateX,
          rotateZ,
          translateY,
          opacity,
        }}
        className="">
        <motion.div className="flex flex-row-reverse space-x-reverse space-x-20 mb-20">
          {firstRow.map((product, idx) => (
            <ProductCard product={product} translate={translateX} key={`${product.title}-1-${idx}`} />
          ))}
        </motion.div>
        <motion.div className="flex flex-row  mb-20 space-x-20 ">
          {secondRow.map((product, idx) => (
            <ProductCard product={product} translate={translateXReverse} key={`${product.title}-2-${idx}`} />
          ))}
        </motion.div>
      </motion.div>
    </div>)
  );
};

export const Header = () => {
  const router = useRouter();

  return (
    (<div
      className="max-w-7xl relative mx-auto py-20 md:py-40 px-4 w-full left-0 top-0">
      <h1 className="text-2xl md:text-7xl font-bold dark:text-white">
        ZIPPY <br /> Delivery
      </h1>
      <div className="max-w-2xl text-base md:text-xl mt-8 dark:text-neutral-200">
        <TextGenerateEffect duration={2} filter={false} words={'Бизнест тань хүч нэмье. Бизнесийн тань өсөлтөд хүч нэмэх ZIPPY шуурхай хүргэлтийн саадгүй үйлчилгээ.'} />
      </div>

      <button className="p-[3px] relative z-50 mt-3" onClick={() => router.push('/login') }>
        <div className="absolute inset-0 bg-gradient-to-r from-indigo-500 to-purple-500 rounded-lg" />
        <div className="px-8 py-2  bg-black rounded-[6px]  relative group transition duration-200 text-white hover:bg-transparent">
          Нэвтрэх
        </div>
      </button>
    </div>)
  );
};

export const ProductCard = ({
  product,
  translate
}) => {
  return (
    (<motion.div
      style={{
        x: translate,
      }}
      whileHover={{
        y: -20,
      }}
      key={product.title}
      className="group/product h-96 w-[30rem] relative flex-shrink-0">
      <Link href={product.link} className="block group-hover/product:shadow-2xl ">
        <Image
          src={product.thumbnail}
          height="600"
          width="600"
          className="object-cover object-left-top absolute h-full w-full inset-0"
          alt={product.title} />
      </Link>
      <div
        className="absolute inset-0 h-full w-full opacity-0 group-hover/product:opacity-80 bg-black pointer-events-none"></div>
      <h2
        className="absolute bottom-4 left-4 opacity-0 group-hover/product:opacity-100 text-white">
        {product.title}
      </h2>
    </motion.div>)
  );
};
