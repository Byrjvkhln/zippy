import React from 'react'

const FormInput = ({text, type, value, onChange, placeholder}: any) => {
  return (
    <div className="w-full mb-5">
      <label>{text }</label>
      <input
        type={type ? type: 'text'}
        className="outline-none border px-3 py-2 block w-full mt-3 border-gray-400 rounded-lg"
        placeholder={text || placeholder}
        onChange={onChange} value={value} 
      />
    </div>
  );
}

export default FormInput
