import { useState, useEffect, Suspense } from "react";
import { Card, CardBody, CardFooter, Avatar } from "@material-tailwind/react";
import axios from "axios";
import { FiEye } from "react-icons/fi";
import Link from "next/link";
import Pagination from "../Pagination";
import Loader from "../wait/Loader";
import NoData from "../NoData";

const TABLE_HEAD = [
  "Жолооч",
  "И-мэйл, Утас",
  "Дансны дугаар",
  "Данс эзэмшигчийн нэр",
  "Машины дугаар",
  "Бүртгэгдсэн",
  "",
];

export function Workers({ show, fetchDrivers, drivers, loading, refetch }: any) {
  const [currentPage, setCurrentPage] = useState(1);
  const length = drivers?.length;
  const driverPerPage = 100;

  const indexOfLastItem = currentPage * driverPerPage;
  const indexOfFirstItem = indexOfLastItem - driverPerPage;
  const currentDrivers = drivers.slice(indexOfFirstItem, indexOfLastItem);

  useEffect(() => {
    fetchDrivers({ currentPage, driverPerPage });
  }, [show, refetch]);

  return (
    <>
      {loading ? (
        <Loader color="#183770" />
      ) : drivers.length > 0 ? (
        <>
          <Card className="h-fit w-full rounded-none shadow-none overflow-auto" {...({} as any)}>
            <CardBody className="!p-0 " {...({} as any)}>
              <table className=" w-full min-w-max table-auto text-left">
                <thead>
                  <tr className="">
                    {TABLE_HEAD.map((head) => (
                      <th key={head} className=" bg-blue-gray-200/50 px-4 py-2">
                        <p className="max-w-[150px] text-center  mx-auto">{head}</p>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {currentDrivers?.map((driver: any, index: any) => {
                    const isLast = index === drivers.length - 1;
                    const classes = isLast
                      ? "px-4 py-2 text-center"
                      : "px-4 py-2 border-b border-blue-gray-100 text-center";

                    return (
                      <tr key={driver?.id}>
                        <td className={classes}>
                          <div className="flex items-center gap-3">
                            <Avatar
                              src="https://wallpapers.com/images/hd/profile-picture-xj8jigxkai9jag4g.jpg"
                              alt=""
                              size="sm"
                              {...({} as any)}
                            />
                            <div className="flex flex-col items-start">
                              <p>{driver.Surname}</p>
                              <p>{driver.Name}</p>
                            </div>
                          </div>
                        </td>
                        <td className={classes}>
                          <p>{driver.Email}</p>
                          <p>{driver.PhoneNumber}</p>
                        </td>

                        <td className={classes}>
                          <p>{driver.Account}</p>
                        </td>
                        <td className={classes}>
                          <p>{driver.AccountName}</p>
                        </td>
                        <td className={classes}>
                          <p>{driver.CarNumber}</p>
                        </td>

                        <td className={classes}>
                          <p>{driver.createdAt.split("T")[0]}</p>
                        </td>
                        <td className={classes}>
                          <div className="flex text-lg items-center gap-3 ">
                            <Link
                              href={`/dashboard/delivery-worker/${driver.id}`}
                              className="size-8 flex items-center justify-center border border-gray-400 rounded hover:border-black hover:text-black duration-300 transition-colors"
                            >
                              <FiEye />
                            </Link>
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </CardBody>
            <CardFooter className="flex items-center  border-t border-gray-400  p-4 justify-end" {...({} as any)}>
              <Suspense fallback={<div>Loading...</div>}>
                <Pagination
                  currentPage={currentPage}
                  setCurrentPage={setCurrentPage}
                  length={length}
                  num={driverPerPage}
                />
              </Suspense>
            </CardFooter>
          </Card>
        </>
      ) : (
        <NoData />
      )}
    </>
  );
}
