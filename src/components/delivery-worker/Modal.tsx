"use client";

import React, { useEffect, useState } from "react";
import { AiOutlineClose } from "react-icons/ai";
import FormInput from "./FormInput";
import { ColorPicker, useColor } from "react-color-palette";
import axios from "axios";
import { FiEye, FiEyeOff } from "react-icons/fi";
import "react-color-palette/css";
import Loader from "../wait/Loader";

const Modal = ({ setShow, show }: any) => {
  const [color, setColor] = useColor("");
  const [customers, setAllCustomers] = useState([]);
  const [hide, setHide] = useState(false);
  const [formValues, setFormValues] = useState({
    lastName: "",
    name: "",
    phone: "",
    email: "",
    password: "",
    accountName: "",
    account: "",
    car: "",
  });

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchCustomers = async () => {
      const res = await axios.get("/api/customer", {
        headers: {
          "x-api-key": "Lol999za",
        },
      });
      setAllCustomers(res.data);
    };
    fetchCustomers();
  }, []);

  const send = async () => {
    setLoading(true);
    const res = await axios.post("/api/driver", {
      Account: formValues.account,
      AccountName: formValues.accountName,
      CarNumber: formValues.car,
      Email: formValues.email,
      Name: formValues.name,
      Surname: formValues.lastName,
      PhoneNumber: formValues.phone,
      Active: false,
      Password: formValues.password,
      Salary: {
        History: [],
        Total: "0",
      },
    });

    if (res.status === 200) {
      setLoading(false);
      setShow(false);
    }
  };

  const handler = () => {
    setShow(false);
  };

  const handleChange = (e: any, field: string) => {
    setFormValues({
      ...formValues,
      [field]: e.target.value,
    });
  };

  useEffect(() => {
    if (show) {
      document.body.classList.add("overflow-hidden");
    } else {
      document.body.classList.remove("overflow-hidden");
    }
    return () => {
      document.body.classList.remove("overflow-hidden");
    };
  }, [show]);

  return (
    <div className="fixed inset-0 top-0 z-10 flex items-center justify-center w-full h-full   ">
      <div
        className="fixed inset-0 top-0 w-full h-full  bg-black/50 backdrop-blur-sm overflow-y-auto py-10"
        onClick={handler}
      >
        <div
          onClick={(e) => e.stopPropagation()}
          className=" max-w-[600px] mx-auto w-[95%] max-h-fit bg-white px-6 py-8  rounded-xl text-gray-900 "
        >
          <div className="flex items-center justify-between w-full border-b pb-3 border-gray-400 mb-5">
            <h2 className="text-xl font-bold">Шинэ жолооч нэмэх</h2>
            <AiOutlineClose className="text-xl cursor-pointer" onClick={() => setShow(!show)} />
          </div>

          <FormInput text="Овог" onChange={(e: any) => handleChange(e, "lastName")} value={formValues.lastName} />
          <FormInput text="Нэр" onChange={(e: any) => handleChange(e, "name")} value={formValues.name} />
          <FormInput
            text="И-мэйл"
            type="email"
            onChange={(e: any) => handleChange(e, "email")}
            value={formValues.email}
          />
          <FormInput text="Утас" onChange={(e: any) => handleChange(e, "phone")} value={formValues.phone} />

          {/* <div className="mb-5">
              <p className="mb-3">Өнгө {color.hex}</p>
              <ColorPicker color={color} onChange={setColor} />
            </div> */}
          <FormInput
            text="Данс эзэмшигчийн нэр"
            onChange={(e: any) => handleChange(e, "accountName")}
            value={formValues.accountName}
          />
          <FormInput
            text="Цалингийн данс"
            onChange={(e: any) => handleChange(e, "account")}
            value={formValues.account}
          />
          <FormInput
            text="Автомашины улсын дугаар"
            onChange={(e: any) => handleChange(e, "car")}
            value={formValues.car}
          />

          <p className="mb-3">Нууц үг</p>
          <div className="flex items-center border px-4 py-2 rounded-lg border-gray-400 gap-5 mb-5">
            <input
              type={hide ? "text" : "password"}
              className="outline-none w-full"
              onChange={(e: any) => handleChange(e, "password")}
              value={formValues.password}
            />

            {hide ? (
              <FiEye className="text-lg cursor-pointer" onClick={() => setHide(!hide)} />
            ) : (
              <FiEyeOff className="text-lg cursor-pointer" onClick={() => setHide(!hide)} />
            )}
          </div>

          <button
            onClick={send}
            className="bg-main text-white rounded-lg py-2 w-full px-4 font-medium text-base hover:bg-blue-900 transition-colors duration-300"
          >
            {loading ? <Loader color="white" /> : "Хадгалах"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
