import React from 'react'

const Stat = ({text, num}:any) => {
  return (
    <div className='flex items-center justify-between pr-20 mb-2'>
      <p>{text}</p>
      <p>{num}</p>
    </div>
  );
}

export default Stat
