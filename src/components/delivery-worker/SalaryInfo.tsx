import React from "react";
import { FaPhoneSquare, FaCar, FaMotorcycle } from "react-icons/fa";
import { CiEdit } from "react-icons/ci";

const SalaryInfo = ({ phone, fuel, onClick, onFuelClick, onSalaryClick }: any) => {
  return (
    <div className="border-b pb-5 border-gray-400 ">
      <p className="text-lg font-semibold text-main mb-5">Цалингийн мэдээлэл</p>
      <div className="@container">
        <div className="grid @4xl:grid-cols-3 @sm:grid-cols-2 gap-5">
          {" "}
          <div className="flex flex-col p-4 border border-gray-400 gap-2 cursor-pointer" onClick={onSalaryClick}>
            <CiEdit className="text-2xl" />
            <FaMotorcycle className="text-main text-2xl self-center" />
            <div className="flex justify-between items-center">
              <p className="text-gray-700">Хүргэлтийн цалин</p>
              <p className="font-semibold text-lg text-main">
                {Number(fuel).toLocaleString("en-US", {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                })}
                ₮
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SalaryInfo;
