'use client'

import React, {useState} from 'react'
import { Tabs, TabsHeader, TabsBody, Tab } from "@material-tailwind/react";
import { IoDocumentAttach } from "react-icons/io5";

const data = [
  {
    label: "Жолооны үнэмлэх",
    value: "Жолооны үнэмлэх",
  },
  {
    label: "Иргэний үнэмлэх",
    value: "Иргэний үнэмлэх",
  },
  {
    label: "Машины зураг",
    value: "Машины зураг",
  },
];

const Documents = () => {
    
 const [activeTab, setActiveTab] = useState("Жолооны үнэмлэх");
  return (
    <div className="  border-gray-400 mt-5 mb-10">
      <p className="text-lg font-semibold text-main mb-5">
        Бичиг баримтын баталгаажуулалт
      </p>
      <Tabs value={activeTab}>
        <TabsHeader
          {...({} as any)}
          className="rounded-none border-b border-gray-400 bg-transparent p-0 whitespace-nowrap overflow-x-auto scrollbar-hide mb-5"
          indicatorProps={{
            className:
              "bg-transparent border-b-2 border-main shadow-none rounded-none",
          }}
        >
          {data.map(({ label, value }) => (
            <Tab
              {...({} as any)}
              key={value}
              value={value}
              onClick={() => setActiveTab(value)}
              className={
                activeTab === value ? " w-fit px-4 text-main" : "w-fit px-4"
              }
            >
              <div className='flex items-center gap-2'>
                <IoDocumentAttach />
                {label}
              </div>
            </Tab>
          ))}
        </TabsHeader>
        <TabsBody {...({} as any)}>ggfdgfghgfhgfhgf</TabsBody>
      </Tabs>
    </div>
  );
}

export default Documents
