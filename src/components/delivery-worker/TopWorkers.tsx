"use client";
import React, { useState, useEffect } from "react";
import axios from "axios";

const TopWorkers = () => {
  const [drivers, setDrivers] = useState([]);
  const today = new Date();
  const startOfLastMonth = new Date(today.getFullYear(), today.getMonth() - 1, 1);
  const endOfLastMonth = new Date(today.getFullYear(), today.getMonth(), 0);
  endOfLastMonth.setHours(23, 59, 59, 999);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const resDrivers = await axios.get("  /api/driver/delivery", {
          headers: {
            "x-api-key": "Lol999za",
          },
        });
        const filteredDrivers = resDrivers.data
          .filter((d: any) => d.Delivery.length !== 0 && d.id !== "678623f3057973ab31d6ff1c")
          .map((c: any) => {
            const filteredDeliveries = c.Delivery.filter((delivery: any) => {
              const deliveryDate = new Date(delivery.createdAt);
              return deliveryDate >= startOfLastMonth && deliveryDate <= endOfLastMonth;
            });
            return {
              ...c,
              Delivery: filteredDeliveries,
            };
          })
          .filter((c: any) => c.Delivery.length !== 0)
          .sort((a: any, b: any) => b.Delivery.length - a.Delivery.length);
        const topWorkers = filteredDrivers.slice(0, 3);
        setDrivers(topWorkers);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="p-5  rounded-lg bg-white shadow-md">
      <h4 className="text-xl font-semibold mb-1">Топ ажилтан</h4>
      <p>Сүүлийн 1 сараар</p>

      <div className="overflow-auto scrollbar-hide">
        <div className="flex gap-5 sm:gap-10 font-semibold mb-5">
          <div className="w-40 sm:w-52 shrink-0"></div>
          <div>
            <p>Нийт</p>
          </div>
          <div>
            <p>Хүргэгдсэн</p>
          </div>
          <div>
            <p>Цуцлагдсан</p>
          </div>
        </div>
        <div></div>
        {drivers?.map((c: any) => (
          <div className="flex gap-5 sm:gap-10" key={c.id}>
            <p className="font-semibold xl:col-span-2 w-40 shrink-0 sm:w-52">{c.Name}</p>
            <p className="w-8 shrink-0 text-center">{c.Delivery?.length}</p>
            <p className="w-[78px] text-center shrink-0">{c.Delivery.filter((d: any) => d.Status === "done").length}</p>
            <p className="text-center w-20 shrink-0">{c.Delivery.filter((d: any) => d.Status === "cancel").length}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TopWorkers;
