"use client";

import React, { useState } from "react";
import { Button, Dialog, DialogHeader, DialogBody, DialogFooter } from "@material-tailwind/react";
import { AiOutlineClose } from "react-icons/ai";
import { FiEye, FiEyeOff } from "react-icons/fi";
import axios from "axios";
import Loader from "./wait/Loader";

const EditModal = ({
  openPass,
  setOpenPass,
  openPhone,
  setOpenPhone,
  openCustomerPass,
  setOpenCustomerPass,
  openCustomerPhone,
  setOpenCustomerPhone,
  setOpenCustomerCost,
  customerCost,
  openCustomerCost,
  phone,
  openEmail,
  setOpenEmail,
  openSalary,
  setOpenSalary,
  openCustomerEmail,
  setOpenCustomerEmail,
  email,
  customerEmail,
  customerPhone,
  id,
  customerId,
  driverSalary,
}: any) => {
  const [show, setShow] = useState(false);
  const [phoneNum, setPhoneNum] = useState(phone);
  const [mail, setMail] = useState(email);
  const [password, setPassword] = useState("");
  const [salary, setSalary] = useState(driverSalary);
  const [customerPhoneNum, setCustomerPhoneNum] = useState(customerPhone);
  const [customerMail, setCustomerMail] = useState(customerEmail);
  const [newcustomerCost, setNewcustomerCost] = useState(customerCost);
  const [customerPassword, setCustomerPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const handleOpen = () => {
    setOpenPass(false);
    setOpenPhone(false);
    setOpenEmail(false);
    setOpenSalary(false);
  };

  const handleCustomerOpen = () => {
    setOpenCustomerPass(false);
    setOpenCustomerPhone(false);
    setOpenCustomerEmail(false);
    setOpenCustomerCost(false);
  };

  const driverChanger = async (change: any) => {
    setLoading(true);
    const res = await axios.put("  /api/driver", {
      id,
      change,
    });

    if (res.status === 200) {
      handleOpen();
      setLoading(false);
    }
  };

  const customerChanger = async (change: any) => {
    setLoading(true);
    const res = await axios.put("  /api/customer", {
      id: customerId,
      change,
    });

    if (res.status === 200) {
      handleCustomerOpen();
      setLoading(false);
    }
  };

  return (
    <Dialog
      {...({} as any)}
      open={
        openPass ||
        openPhone ||
        openEmail ||
        openCustomerEmail ||
        openCustomerPass ||
        openCustomerPhone ||
        openSalary ||
        openCustomerCost
      }
      handler={openPass || openPhone || openEmail || openSalary ? handleOpen : handleCustomerOpen}
      size="xs"
    >
      <DialogHeader {...({} as any)}>
        <div className="flex items-center justify-between w-full">
          {(openPass || openCustomerPass) && "Нууц үг солих"}{" "}
          {(openPhone || openCustomerPhone) && "Утасны дугаар солих"}{" "}
          {(openEmail || openCustomerEmail) && "И-мэйл хаяг солих"}
          {openCustomerCost && "Хүргэлтийн төлбөр шинэчлэх"}
          {openSalary && "Хүргэлтийн цалин шинэчлэх"}
          <AiOutlineClose
            className="text-xl cursor-pointer"
            onClick={openPass || openPhone || openEmail || openSalary ? handleOpen : handleCustomerOpen}
          />
        </div>
      </DialogHeader>
      <DialogBody {...({} as any)}>
        {openPass || openCustomerPass ? (
          <div className="border px-4 py-2 border-gray-400 rounded-lg flex items-center gap-5 text-gray-900">
            <input
              type={show ? "text" : "password"}
              className="w-full outline-none "
              onChange={(e) => (openPass ? setPassword(e.target.value) : setCustomerPassword(e.target.value))}
            />
            {show ? (
              <FiEye className="text-lg cursor-pointer" onClick={() => setShow(!show)} />
            ) : (
              <FiEyeOff className="text-lg cursor-pointer" onClick={() => setShow(!show)} />
            )}
          </div>
        ) : (
          <input
            type="text"
            className="border w-full px-4 py-2 border-gray-400 rounded-lg outline-none text-gray-900"
            defaultValue={
              (openPhone && phone) ||
              (openEmail && email) ||
              (openCustomerEmail && customerEmail) ||
              (openCustomerPhone && customerPhone) ||
              (openCustomerCost && customerCost) ||
              (openSalary && driverSalary)
            }
            onChange={(e) =>
              openPhone
                ? setPhoneNum(e.target.value)
                : openEmail
                ? setMail(e.target.value)
                : openCustomerEmail
                ? setCustomerMail(e.target.value)
                : openCustomerCost
                ? setNewcustomerCost(e.target.value)
                : openCustomerPhone
                ? setCustomerPhoneNum(e.target.value)
                : setSalary(e.target.value)
            }
          />
        )}
      </DialogBody>
      <DialogFooter {...({} as any)}>
        <Button
          {...({} as any)}
          variant="outlined"
          onClick={openPass || openPhone || openEmail || openPhone || openSalary ? handleOpen : handleCustomerOpen}
          className="mr-1 normal-case text-base py-2"
        >
          <span>Хаах</span>
        </Button>
        <Button
          {...({} as any)}
          className="normal-case text-base bg-main hover:bg-blue-900 py-2 border border-main"
          onClick={() => {
            if (openPhone) driverChanger(["PhoneNumber", phoneNum]);
            else if (openEmail) driverChanger(["Email", mail]);
            else if (openPass) driverChanger(["Password", password]);
            else if (openCustomerPhone) customerChanger(["PhoneNumber", customerPhoneNum]);
            else if (openCustomerEmail) customerChanger(["Email", customerMail]);
            else if (openCustomerPass) customerChanger(["Password", customerPassword]);
            else if (openCustomerCost) customerChanger(["TransportCost", newcustomerCost]);
          }}
        >
          <span>{loading ? <Loader color="white" /> : "Хадгалах"}</span>
        </Button>
      </DialogFooter>
    </Dialog>
  );
};

export default EditModal;
