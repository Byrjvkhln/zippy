'use client'

import React, {useState, useEffect} from 'react'
import { GoMail, GoPencil } from "react-icons/go";
import { FiPhoneCall } from "react-icons/fi";
import { Avatar } from '@material-tailwind/react';
import ImgModal from './ImgModal';

const Registration = ({user}:any) => {
    const [img, setImg] = useState<any>('');
    const [image, setImage] = useState<any>('');
const [open ,setOpen] = useState(false)
    
  useEffect(() => {
    if (img) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setImage(reader.result);
      };
      reader.readAsDataURL(img);
    }
  }, [img]);
  return (
    <div className="px-5 pb-10 mt-10">
      <div className="mb-10">
        <h4 className="text-lg mb-2">Хувийн мэдээлэл</h4>
        <p>Хувийн мэдээлэл шинэчлэх хэсэг</p>
      </div>

      <div className="flex flex-col lg:flex-row lg:items-center border-b pb-10  border-gray-400 border-dashed">
        <div className="flex-1">
          {user.Status === "admin" ? (
            <p className="text-lg">Нэр</p>
          ) : (
            <div>
              {" "}
              <h4 className="text-lg mb-2">Байгууллагын нэр</h4>
              <p>Байгууллагын нэрийг 2 үед багтаан оруулна уу?</p>
            </div>
          )}
        </div>

        <div className="flex-[2] flex gap-5">
          <input
            type="text"
            className="border py-2 px-4 border-gray-400 rounded-lg outline-none w-80"
            placeholder={user.Status === "admin" ? "Нэр" : "Байгууллага"}
            defaultValue={user.Name}
          />
          <input
            type="text"
            className="border py-2 px-4 border-gray-400 rounded-lg outline-none w-80"
            placeholder={user.Status === "admin" ? "Овог" : "LLC"}
          />
        </div>
      </div>
      <div className="flex flex-col sm:flex-row sm:items-center border-b py-10 border-gray-400 border-dashed">
        <div className="flex-1">
          <h4 className="text-lg mb-2">И-мэйл хаяг</h4>
        </div>

        <div className="flex-[2] ">
          <div className="flex items-center gap-2 border py-2 px-4 border-gray-400 rounded-lg outline-none w-80">
            <GoMail className="text-lg text-gray-700" />
            <input
              type="text"
              placeholder="И-мэйл"
              className="outline-none w-full"
              defaultValue={user.Email}
            />
          </div>
        </div>
      </div>
      <div className="flex flex-col sm:flex-row sm:items-center  py-10 border-gray-400 border-dashed">
        <div className="flex-1">
          <h4 className="text-lg mb-2">Утас</h4>
        </div>

        <div className="flex-[2] ">
          <div className="flex items-center gap-2 border py-2 px-4 border-gray-400 rounded-lg outline-none w-80">
            <FiPhoneCall className="text-xl text-gray-700" />
            <input
              type="text"
              placeholder="+976"
              className="outline-none w-full"
              defaultValue={user.PhoneNumber}
            />
          </div>
        </div>
      </div>
      <div className="flex flex-col gap-5 sm:flex-row sm:items-center  py-10 ">
        <div className="flex-1 sm:flex-[2] md:flex-1">
          <h4 className="text-lg mb-2">
            {user.Status === "admin" ? "Профайл зураг" : "Байгууллагын лого"}
          </h4>
          <p>
            {user.Status === "admin" ? "Профайл зургийг" : "Байгууллагын лого"} 
            {" "}хуулна уу?
          </p>
        </div>

        <div className="flex-[2] ">
          <div>
            <label htmlFor="uploadImg">
              <div
                className="size-32 relative border rounded-full border-gray-400 flex items-center justify-center cursor-pointer overflow-hidden"
                onClick={() => setOpen(!false)}
              >
                <Avatar {...({} as any)} src={user.Image} />
                <div className=" bg-black/70 absolute inset-0 flex items-center justify-center">
                  <GoPencil className="text-3xl text-white" />
                </div>
              </div>
            </label>
            <input
              type="file"
              id="uploadImg"
              className="hidden"
              onChange={(e: any) => setImg(e.target.files[0])}
            />
          </div>

          {image && open && (
            <ImgModal
              open={open}
              setOpen={setOpen}
              image={image}
              setImage={setImage}
            />
          )}
        </div>
      </div>

      <button className="block ml-auto bg-main text-base font-medium px-4 py-2 rounded text-white hover:bg-blue-900 transition-colors duration-300">
        Шинэчлэх
      </button>
    </div>
  );
}

export default Registration
