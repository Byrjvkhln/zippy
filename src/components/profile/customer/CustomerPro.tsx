"use client";

import React, { useState } from "react";
import {
  Tabs,
  TabsHeader,
  TabsBody,
  Tab,
} from "@material-tailwind/react";
import Registration from "./Registration";
import Password from "./Password";
import Integration from "./Integration";

const data = [
  {
    label: "Ерөнхий бүртгэл",
    value: "Ерөнхий бүртгэл",

  },
  {
    label: "Нууц үг",
    value: "Нууц үг",

  },
  {
    label: "Төлбөр",
    value: "Төлбөр",

  },
  {
    label: "Хаяг",
    value: "Хаяг",
  },
];

const CustomerPro = ({ user }: any) => {
  const [activeTab, setActiveTab] = useState("Ерөнхий бүртгэл");

  return (
    <Tabs value={activeTab}>
      <TabsHeader
        {...({} as any)}
        className="rounded-none border-b border-gray-400 bg-transparent p-0 whitespace-nowrap overflow-x-auto scrollbar-hide mx-5"
        indicatorProps={{
          className:
            "bg-transparent border-b-2 border-gray-900 shadow-none rounded-none",
        }}
      >
        {data.map(({ label, value }) => (
          <Tab
            {...({} as any)}
            key={value}
            value={value}
            onClick={() => setActiveTab(value)}
            className={
              activeTab === value ? "text-gray-900 w-fit px-4" : "w-fit px-4"
            }
          >
             {label}
          </Tab>
        ))}
      </TabsHeader>
      <TabsBody {...({} as any)}>
        {activeTab === "Ерөнхий бүртгэл" && <Registration user={user} />}
        {activeTab === "Нууц үг" && <Password user={user} />}
        {activeTab === "Төлбөр" && <Integration />}
      </TabsBody>
    </Tabs>
  );
};

export default CustomerPro;
