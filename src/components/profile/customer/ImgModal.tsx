"use client";

import React from "react";
import {
  Button,
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
  Avatar
} from "@material-tailwind/react";
import { AiOutlineClose } from "react-icons/ai";

const ImgModal = ({ open, setOpen, image, setImage }: any) => {
  const handleOpen = () => {
    setOpen(!open)
    setImage('')
  };
  return (
    <Dialog {...({} as any)} open={open} handler={handleOpen} size="xs">
      <DialogHeader {...({} as any)}>
        <div className="flex items-center justify-between w-full">
          <p className="text-lg">Профайл зураг шинэчлэх үү?</p>
          <AiOutlineClose
            className="text-lg cursor-pointer"
            onClick={handleOpen}
          />
        </div>
      </DialogHeader>
      <DialogBody {...({} as any)}>
        <Avatar
          src={image}
          {...({} as any)}
          size="xxl"
          className="mx-auto block"
        />
      </DialogBody>
      <DialogFooter {...({} as any)}>
        <Button
          onClick={handleOpen}
          className="mr-1 bg-red-300 normal-case text-base"
          {...({} as any)}
        >
          <span>Хаах</span>
        </Button>
        <Button
          {...({} as any)}
          className="normal-case text-base bg-blue-900"
          onClick={handleOpen}
        >
          <span>Хадгалах</span>
        </Button>
      </DialogFooter>
    </Dialog>
  );
};

export default ImgModal;
