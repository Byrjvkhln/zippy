'use client'

import React from 'react'
import { FiEye, FiEyeOff } from "react-icons/fi";
import { Avatar } from '@material-tailwind/react';

const Password = ({user}:any) => {
  return (
    <div>
      <div className="h-80 bg-yellow-100 relative overflow-visible mb-28">
        <div className="-bottom-20 absolute left-10 flex gap-5">
          {" "}
          <div className="size-40 rounded-full border flex items-center justify-center bg-white p-2 ">
            <div className="w-full h-full bg-gray-300 rounded-full ">
              <Avatar src={user.Image} {...({} as any)} className='w-full h-full'/>
            </div>
          </div>
          <div className="self-end">
            <h2 className="text-xl uppercase font-semibold mb-1">
              {user?.Name}
            </h2>
            <p className="text-gray-700">{user?.Email}</p>
          </div>
        </div>
      </div>

      <div className="p-5 pb-10">
        <div className="flex flex-col lg:flex-row lg:items-center border-t py-10 gap-5 border-gray-400 border-dashed">
          <div className="flex-1">
            <h4 className="text-lg mb-2">Шинэ нууц үг</h4>
          </div>

          <div className="flex-[2] flex gap-5 ">
            <div className="border border-gray-400 flex items-center px-4 py-2 rounded-lg w-80">
              <input
                type="text"
                className=" outline-none w-full"
                placeholder="Шинэ нууц үг"
              />
              <FiEyeOff className="text-lg" />
            </div>
          </div>
        </div>
        <div className="flex flex-col lg:flex-row lg:items-center border-y py-10 gap-5 border-gray-400 border-dashed mb-10">
          <div className="flex-1">
            <h4 className="text-lg mb-2">Нууц үг давт</h4>
          </div>

          <div className="flex-[2] flex gap-5 ">
            <div className="border border-gray-400 flex items-center px-4 py-2 rounded-lg w-80">
              <input
                type="text"
                className=" outline-none w-full"
                placeholder="Нууц үг давтан оруулах"
              />
              <FiEyeOff className="text-lg" />
            </div>
          </div>
        </div>
        <button className="block ml-auto bg-main text-base font-medium px-4 py-2 rounded text-white hover:bg-blue-900 transition-colors duration-300">
          Нууц үг шинэчлэх
        </button>
      </div>
    </div>
  );
}

export default Password
