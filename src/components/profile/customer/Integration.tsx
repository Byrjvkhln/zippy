"use client";

import React, { useState } from "react";
import { Switch, Dialog, DialogHeader, DialogBody, DialogFooter, Button } from "@material-tailwind/react";
import Link from "next/link";
import { FaPlus } from "react-icons/fa6";
import { FiLock } from "react-icons/fi";

const Integration = () => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(!open);
  return (
    <div className="p-5 mt-5">
      <div>
        <h4 className="font-bold text-xl mb-2">Хүргэлтийн төлбөрийн тохиргоо</h4>
        <p className="mb-7">Хүргэлтийн төлбөрийн дэлгэрэнгүй мэдээлэл.</p>
      </div>

      <div className="p-5 border border-gray-400 rounded-lg flex gap-5">
        <div>
          <p className="mb-2 font-semibold text-lg">Хүргэлтийн төлбөр 6,000.00₮</p>
          <p className="mb-5 text-gray-700">
            Хүргэлтийн төлбөр нь Zippy Delivery System - д гэрээ байгуулсан 6,000.00₮ үнийн дүнгээр байх бөгөөд тухайн
            хүргэлтнээс хүргэлтийн төлбөрийг суутгах төлөв &quot;Идэвхгүй&quot; байна. Төлвийг солихыг хүсвэл
            хэрэглэгчийн төвд хандана уу!
          </p>
          <Link href="/dashboard/delivery" className="font-medium">
            Дэлгэрэнгүй
          </Link>
        </div>

        <div className="shrink-0">
          <Switch
            {...({} as any)}
            disabled
            id="custom-switch-component"
            className="h-full w-full checked:bg-blue-900"
            containerProps={{
              className: "w-11 h-6",
            }}
            circleProps={{
              className: "before:hidden left-0.5 border-none ",
            }}
          />
        </div>
      </div>

      <div className="flex flex-col lg:flex-row lg:items-center  py-10 gap-5 ">
        <div className="flex-1">
          <h4 className="text-xl mb-2 font-bold">Дансны мэдээлэл</h4>
          <p>
            Тухайн дансны мэдээлэл нь Шонхор системээс тооцоо нийлэх болон нэхэмжлэх илгээх данс байх учир дансны
            мэдээллээ сайн шалгаарай.
          </p>
        </div>

        <div
          className="flex-[2] 
           "
        >
          <button
            onClick={handleOpen}
            className="border flex items-center gap-3 px-4 py-2 rounded-lg border-gray-400 hover:text-blue-900 hover:border-blue-900 transition-all duration-300"
          >
            <FaPlus /> Дансны мэдээлэл оруулах
          </button>

          <Dialog {...({} as any)} open={open} handler={handleOpen} size="sm">
            <DialogHeader {...({} as any)}>Дансны дугаар бүртгэх</DialogHeader>
            <DialogBody {...({} as any)}>
              <div className="mb-5">
                <p className="mb-2 text-gray-900 ">Данс эзэмшигч</p>
                <input
                  type="text"
                  className="w-full border border-gray-400 rounded-lg px-4 py-2 outline-none"
                  placeholder="Гүйлгээ хийх данс эзэмшигчийн нэр"
                />
              </div>
              <div className="mb-5">
                <p className="mb-2 text-gray-900">Дансны дугаар</p>
                <input
                  type="text"
                  className="w-full border border-gray-400 rounded-lg px-4 py-2 outline-none"
                  placeholder="Гүйлгээ хийх дансны дугаар"
                />
              </div>

              <div className="flex items-center gap-2 text-gray-600">
                <FiLock className="text-lg shrink-0" /> Данснаас автомат таталт байхгүй бөгөөд SSL хамгаалалттай.
              </div>
            </DialogBody>
            <DialogFooter {...({} as any)}>
              <Button
                {...({} as any)}
                variant="text"
                onClick={handleOpen}
                className="mr-1 border border-gray-400 text-base normal-case"
              >
                <span>Хаах</span>
              </Button>
              <Button
                {...({} as any)}
                className="bg-blue-900 hover:bg-main transition-colors duration-300 text-base normal-case"
                onClick={handleOpen}
              >
                <span>Нэмэх</span>
              </Button>
            </DialogFooter>
          </Dialog>
        </div>
      </div>
    </div>
  );
};

export default Integration;
