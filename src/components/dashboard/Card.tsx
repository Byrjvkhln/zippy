import React from 'react'

const Card = ({text, icon, bg, month, week}:any) => {
  return (
    <div className=" rounded-xl p-6  bg-white shadow-md">
      <div className="flex items-center gap-4 mb-4">
        <div
          className={`size-12 rounded-lg flex items-center justify-center ${bg} `}
        >
          {icon}
        </div>
        <div>
          <p className="mb-1">{text}</p>
          <p>
            <span className=" font-semibold text-xl align-middle">
              {month}
            </span>{" "}
            (Сүүлийн 30 хоног)
          </p>
        </div>
      </div>
      <p>
        <span className="text-green-700 font-semibold text-xl align-middle">
          {week}
        </span>{" "}
        (Сүүлийн 7 хоног)
      </p>
    </div>
  );
}

export default Card
