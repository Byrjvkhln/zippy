"use client";

import React from "react";
import { MdOutlineMenu } from "react-icons/md";
import Image from "next/image";
import CustomerNav from "../customerDash/CustomerNav";
import { FaMessage } from "react-icons/fa6";
import {
  Badge,
  IconButton,
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
  Button,
} from "@material-tailwind/react";
import Notif from "../Notif";
import Link from "next/link";

const Navbar = ({ open, setOpen, user }: any) => {
  return (
    <>
      <div className=" flex items-center justify-between mb-5 p-5 ">
        <div className="flex items-center gap-4">
          <MdOutlineMenu
            onClick={() => setOpen(!open)}
            className="2xl:hidden text-2xl text-gray-900"
          />
          <Link href='/dashboard' className="size-7 relative lg:hidden" >
            <Image src="/logo.svg" alt="" fill />
          </Link>
        </div>
        <div className="flex items-center gap-4">
          {" "}
          {user?.Status === "customer" && (
           <Notif/>
          )}{" "}
          <CustomerNav user={user} />
        </div>
      </div>
    </>
  );
};

export default Navbar;
