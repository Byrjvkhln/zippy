import React, { useState, useEffect } from "react";
import axios from "axios";
import TopWorkers from "../delivery-worker/TopWorkers";

const TopCustomers = () => {
  const [customers, setCustomers] = useState([]);
  const today = new Date();
  const startOfLastMonth = new Date(today.getFullYear(), today.getMonth() - 1, 1);
  const endOfLastMonth = new Date(today.getFullYear(), today.getMonth(), 0);
  endOfLastMonth.setHours(23, 59, 59, 999);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const resCustomers = await axios.get("/api/customer/delivery", {
          headers: {
            "x-api-key": "Lol999za",
          },
        });
        const filteredCustomers = resCustomers.data
          .map((c: any) => {
            const filteredDeliveries = c.Delivery.filter((delivery: any) => {
              const deliveryDate = new Date(delivery.createdAt);
              return deliveryDate >= startOfLastMonth && deliveryDate <= endOfLastMonth;
            });
            return {
              ...c,
              Delivery: filteredDeliveries,
            };
          })
          .filter((c: any) => c.Delivery.length !== 0)
          .sort((a: any, b: any) => b.Delivery.length - a.Delivery.length);

        const topCustomers = filteredCustomers.slice(0, 3);

        setCustomers(topCustomers);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="flex flex-col lg:flex-row gap-5">
      <div className="flex-1 flex flex-col gap-5 ">
        <div className="p-5 rounded-lg shadow-md bg-white">
          <h4 className="text-xl font-semibold mb-1">Топ харилцагчид</h4>
          <p>Сүүлийн 1 сараар</p>

          <div className="overflow-auto scrollbar-hide">
            <div className="flex gap-5 sm:gap-10 font-semibold mb-5">
              <div className="w-40 sm:w-52 shrink-0"></div>
              <div>
                <p>Нийт</p>
              </div>
              <div>
                <p>Хүргэгдсэн</p>
              </div>
              <div>
                <p>Цуцлагдсан</p>
              </div>
            </div>

            {customers?.map((c: any) => (
              <div className="flex gap-5 sm:gap-10" key={c.id}>
                <p className="font-semibold xl:col-span-2 w-40 shrink-0 sm:w-52">{c.Name}</p>
                <p className="w-8 shrink-0 text-center">{c.Delivery?.length}</p>
                <p className="w-[78px] text-center shrink-0">
                  {c.Delivery.filter((d: any) => d.Status === "done").length}
                </p>
                <p className="text-center w-20 shrink-0">
                  {c.Delivery.filter((d: any) => d.Status === "cancel").length}
                </p>
              </div>
            ))}
          </div>
        </div>
        <div>
          <TopWorkers />
        </div>
      </div>

      <div className="flex-[1.3]">
        <iframe
          src={`https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21393.619090559125!2d47.934963!3d106.9210056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5d969266d6724f7b%3A0xa013be7e198bcfa4!2sComputer%20Mall!5e0!3m2!1sen!2smn!4v1722417094837!5m2!1sen!2smn`}
          width="100%"
          height="500"
          loading="lazy"
        ></iframe>
      </div>
    </div>
  );
};

export default TopCustomers;
