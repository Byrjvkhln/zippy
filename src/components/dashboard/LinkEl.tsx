import React from 'react'
import Link from 'next/link'
import { usePathname } from 'next/navigation'


const LinkEl = ({title, icon, path, onClick}: any) => {
  const pathname = usePathname()
  return (
    <Link
    onClick={onClick}
      href={path}
      className={`flex items-center gap-3  text-xl mb-3 px-3 py-1  rounded-lg transition-colors duration-300  ${
        path === pathname ? "text-main !font-bold" : "text-gray-900 hover:bg-gray-200 font-medium"
      }`}
    >
      {icon}
      <p className="text-[14px]">{title}</p>
    </Link>
  );
}

export default LinkEl
