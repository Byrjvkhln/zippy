import React, { useEffect, useState } from "react";
import { links } from "../../../data";
import Image from "next/image";
import LinkEl from "./LinkEl";
import DropdownLink from "./DropdownLink";
import { PiCalendar, PiCurrencyCircleDollarBold } from "react-icons/pi";
import { TbReport } from "react-icons/tb";
import DropEl from "./DropEl";
import { PageChanger } from "../wait/pagechanger";
const Sidebar = ({ user, setOpen, open }: any) => {
  const [diss, setDiss] = useState(false);

  return (
    <div className=" w-[240px]  h-full overflow-y-scroll scrollbar-hide ">
      {diss && <PageChanger setDiss={setDiss}></PageChanger>}
      <div className="flex items-center gap-3 justify-center bg-white mb-5 ">
        <div className="size-10 relative">
          <Image src="/logo.svg" alt="" fill priority />
        </div>
        <p className="uppercase font-semibold text-gray-900">
          zippy <span className="text-[#1AD75E] font-bold">delivery</span>
        </p>
      </div>

      <div className=" pb-10 ">
        {links.slice(0, 1).map((l) => (
          <LinkEl {...l} key={l.title} onClick={() => (open == true ? setOpen(false) : setDiss(true))} />
        ))}

        <h3 className="mt-8 text-gray-600 font-semibold mb-3">Үйл ажиллагаа</h3>
        {links.slice(1, 2).map((l) => (
          <LinkEl {...l} key={l.title} onClick={() => (open == true ? setOpen(false) : setDiss(true))} />
        ))}
        {user?.Status == "admin" && <h3 className="mt-8 text-gray-600 font-semibold mb-3">Мэдээ & Мэдээлэл</h3>}
        {user?.Status == "admin" &&
          links
            .slice(2, 4)
            .map((l) => (
              <LinkEl {...l} key={l.title} onClick={() => (open == true ? setOpen(false) : setDiss(true))} />
            ))}
        <h3 className="mt-8 text-gray-600 font-semibold mb-3">Тооцоо</h3>

        <>
          <DropdownLink title="Цуцлах хүсэлт" icon={<PiCalendar />}>
            <DropEl
              text="Хүсэлтүүд"
              path="/dashboard/delivery-request"
              onClick={() => (open == true ? setOpen(false) : setDiss(true))}
            />
          </DropdownLink>
          <DropdownLink title="Тайлан & Тооцоо" icon={<TbReport />}>
            {user?.Status === "admin" && (
              <DropEl
                text="Нэхэмжлэл үүсгэх"
                path="/dashboard/accounting"
                onClick={() => (open == true ? setOpen(false) : setDiss(true))}
              />
            )}
            {(user?.Status === "admin" || user?.Status === "customer") && (
              <DropEl
                text="Нэхэмжлэх"
                path="/dashboard/invoice"
                onClick={() => (open == true ? setOpen(false) : setDiss(true))}
              />
            )}
          </DropdownLink>

          {user?.Status === "admin" && (
            <DropdownLink title="Цалин & Урамшуулал" icon={<PiCurrencyCircleDollarBold />}>
              <DropEl
                text="Цалин бодох"
                path="/dashboard/salary"
                onClick={() => (open == true ? setOpen(false) : setDiss(true))}
              />
              {/* <DropEl text="Цалингийн түүх " path="" onClick={() => (open == true ? setOpen(false) : setDiss(true))} /> */}
            </DropdownLink>
          )}
        </>

        {user?.Status === "admin" && (
          <>
            <h3 className="mt-8 text-gray-600 font-semibold mb-3">Харилцагч & Ажилтан</h3>
            {links.slice(4, 6).map((l) => (
              <LinkEl {...l} key={l.title} onClick={() => (open == true ? setOpen(false) : setDiss(true))} />
            ))}
          </>
        )}
      </div>
    </div>
  );
};

export default Sidebar;
