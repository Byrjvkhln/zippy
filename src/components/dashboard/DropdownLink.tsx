"use client";

import React, { useState } from "react";
import { IoChevronForward, IoChevronDownSharp } from "react-icons/io5";
import Link from "next/link";
import { usePathname } from "next/navigation";

const DropdownLink = ({ title, icon, children}: any) => {
  const pathname = usePathname()
  const [open, setOpen] = useState(false);
  const [clicked, setClicked] = useState(false);

  return (
    <div onClick={() => setOpen(!open)}>
      <div className="  flex items-center gap-4 rounded-lg transition-colors duration-300 text-xl cursor-pointer mb-2 px-3 py-1 hover:bg-gray-200 ">
        {icon}
        <p className={`flex-1 text-[14px] font-medium shrink-0 `}> {title}</p>

        {open ? (
          <IoChevronDownSharp className="text-[15px] mb-1" />
        ) : (
          <IoChevronForward className="text-[15px] mb-1" />
        )}
      </div>
      {open && (
        <div className=" bg-white" onClick={(e) => e.stopPropagation()}>
         {children}
        </div>
      )}
    </div>
  );
};

export default DropdownLink;
