import React from 'react'
import Link from 'next/link';
import { usePathname } from 'next/navigation';

const DropEl = ({text, path, onClick}:any) => {
    const pathname = usePathname()
  return (
    <Link
    onClick={onClick}
      href={path}
      className={`flex text-[14px] items-center gap-3 ml-1 hover:bg-gray-300 px-4 py-2 rounded-lg transition-colors last:mb-2 duration-300 ${
        pathname === path &&
        "text-main font-medium"
      }`}
    >
      <div
        className={`size-1 rounded-full  ${
          pathname.includes(path)
            ? "bg-main "
            : "bg-gray-700"
        }`}
      ></div>
      {text}
    </Link>
  );
}

export default DropEl
