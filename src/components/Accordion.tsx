"use client";

import { createContext, useContext, useRef, useEffect, useState, ReactNode } from "react";
import { FaChevronDown, FaChevronUp } from "react-icons/fa6";

interface AccordionContextProps {
  selected: any;
  setSelected: (value: any) => void;
}

const AccordionContext = createContext<AccordionContextProps | undefined>(undefined);

interface AccordionProps {
  children: ReactNode;
  value: any;
  onChange?: (value: any) => void;
  [key: string]: any;
}

export default function Accordion({ children, value, onChange, ...props }: AccordionProps) {
  const [selected, setSelected] = useState(value);

  useEffect(() => {
    onChange?.(selected);
  }, [selected, onChange]);

  return (
    <ul {...props}>
      <AccordionContext.Provider value={{ selected, setSelected }}>{children}</AccordionContext.Provider>
    </ul>
  );
}

interface AccordionItemProps {
  children: ReactNode;
  value: any;
  trigger: ReactNode;
  [key: string]: any;
}

export function AccordionItem({ children, value, trigger, ...props }: AccordionItemProps) {
  const context = useContext(AccordionContext);

  if (!context) {
    throw new Error("AccordionItem must be used within an Accordion");
  }

  const { selected, setSelected } = context;
  const open = selected === value;

  const ref = useRef<HTMLDivElement | null>(null);

  return (
    <li className="bg-white mb-9 rounded p-2  " {...props}>
      <header
        role="button"
        onClick={() => setSelected(open ? null : value)}
        className="flex items-center gap-3  text-lg font-semibold  md:justify-between"
      >
        {trigger}
        <div className="flex items-center justify-center size-10">{open ? <FaChevronUp /> : <FaChevronDown />}</div>
      </header>
      <div className="overflow-y-hidden transition-all" style={{ height: open ? ref.current?.offsetHeight || 0 : 0 }}>
        <div className="py-2 pt-8" ref={ref}>
          {children}
        </div>
      </div>
    </li>
  );
}
