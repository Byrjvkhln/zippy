import { Card, CardBody, CardFooter, Avatar } from "@material-tailwind/react";
import Link from "next/link";
import { FiEye } from "react-icons/fi";
import Pagination from "../Pagination";
import NoData from "../NoData";

const TABLE_HEAD = ["№", "Төрөл", "Хаяг", "Утас", "Бараа тоо", "Хүсэлт илгээсэн", "Шалтгаан", "Хариулсан", ""];

const AllRequests = ({ allReqData, currentPage, setCurrentPage, num }: any) => {
  const length = allReqData?.length;
  return (
    <>
      {allReqData?.length > 0 ? (
        <Card className=" w-full rounded-none shadow-none" {...({} as any)}>
          <CardBody className="overflow-auto !p-0 " {...({} as any)}>
            <table className="w-full min-w-max table-auto text-left">
              <thead>
                <tr>
                  {TABLE_HEAD.map((head) => (
                    <th key={head} className="bg-blue-gray-200/50 p-4 text-center">
                      <p>{head}</p>
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {allReqData?.map((c: any, index: any) => {
                  const isLast = index === allReqData.length - 1;
                  const classes = isLast ? "p-4 text-center" : "p-4 border-b border-blue-gray-100 text-center";
                  // const itemIndex = indexOfFirstItem + index + 1;
                  return (
                    <tr
                      className={`${c.Status == true ? "bg-green-100" : c.Status == false ? "bg-red-100" : ""} `}
                      key={index}
                    >
                      <td className={classes}>
                        <p>{index + 1}</p>
                      </td>
                      <td className={classes}>
                        <p>{c.TypeOfReq == "cancel" ? "Цуцлах" : "Хойшлох"}</p>
                      </td>

                      <td className={classes}>
                        <p className="max-w-[300px]">{c.Delivery.Location}</p>
                      </td>
                      <td className={classes}>
                        <p> {c.Delivery.PhoneNumber}</p>
                      </td>
                      <td className={classes}>
                        <p>{c.Delivery.Product.length} ш</p>
                      </td>

                      <td className={classes}>
                        <p>{c.createdAt.split("T")[0]}</p>
                      </td>

                      <td className={classes}>
                        <p>{c.Reason}</p>
                      </td>

                      <td className={classes}>
                        <p>{c.ApprovedDate ? c.ApprovedDate : "Хариу ирээгүй"}</p>
                      </td>

                      <td className={classes}>
                        <div className="flex gap-3">
                          {" "}
                          <Link
                            href={`/dashboard/delivery-request/${c.id}`}
                            className="size-7 flex items-center justify-center border border-gray-400 rounded hover:border-black hover:text-black duration-300 transition-colors"
                          >
                            <FiEye className="" />
                          </Link>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </CardBody>
          <CardFooter className="flex justify-end border-t border-gray-400 p-4" {...({} as any)}>
            <Pagination currentPage={currentPage} setCurrentPage={setCurrentPage} length={length} num={num} />
          </CardFooter>
        </Card>
      ) : (
        <NoData />
      )}
    </>
  );
};

export default AllRequests;
