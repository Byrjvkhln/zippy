"use client";

import React, { useEffect, useState, Suspense } from "react";
import axios from "axios";
import { FaLocationArrow, FaUser, FaPhoneAlt } from "react-icons/fa";
import Pagination from "../Pagination";
import { convertToMongolianTime } from "@/src/utils/timeConvertor";
import Loader from "../wait/Loader";

const DriverModal = ({ open, setOpen, data }: any) => {
  console.log({ data });

  const [drivers, setDrivers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const length = drivers?.length;
  const driverPerPage = 10;
  const [selected, setSelected] = useState<any>();
  const [name, setName] = useState<any>("");
  const [delid, setDelid] = useState<any>("");
  const [loading, setLoading] = useState(false);
  const [loading2, setLoading2] = useState(false);

  const indexOfLastItem = currentPage * driverPerPage;
  const indexOfFirstItem = indexOfLastItem - driverPerPage;
  const currentDrivers = drivers.slice(indexOfFirstItem, indexOfLastItem);

  useEffect(() => {
    const fetchdrivers = async () => {
      setLoading(true);
      const res = await axios.get("/api/driver", {
        headers: {
          "x-api-key": "Lol999za",
        },
        params: {
          page: currentPage,
          pageSize: driverPerPage,
        },
      });

      setDrivers(res?.data.reverse());
      setLoading(false);
    };
    fetchdrivers();
  }, []);

  const deliveryChanger = async (id: any, change: any) => {
    setLoading2(true);
    const res = await axios.put("/api/delivery", {
      id,
      change: [change, ["Status", "comfirmed"]],
    });

    if (res.status === 200) {
      setLoading2(false), setOpen(!open);
    }
  };

  const handler = () => {
    setOpen(!open);
  };

  useEffect(() => {
    if (open) {
      document.body.classList.add("overflow-hidden");
    } else {
      document.body.classList.remove("overflow-hidden");
    }
    return () => {
      document.body.classList.remove("overflow-hidden");
    };
  }, [open]);
  return (
    <div className="fixed inset-0 top-0 z-10 flex items-center justify-center w-full h-full   ">
      <div
        className="fixed inset-0 top-0 w-full h-full  bg-black/50 backdrop-blur-sm  py-10 2xl:py-0 overflow-y-auto xl:flex xl:items-center"
        onClick={handler}
      >
        <div
          onClick={(e) => e.stopPropagation()}
          className=" max-w-[1500px] mx-auto w-[95%] my-auto max-h-fit bg-white p-3 lg:p-8 rounded-xl text-gray-900  flex gap-5 flex-col xl:flex-row"
        >
          {/* { {many == false ? ( */}
          <div
            style={{
              boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
            }}
            className="flex-1 flex flex-col gap-5 border p-5 rounded-md "
          >
            <div className="text-2xl font-semibold border-b pb-5 border-gray-400">Хүргэлт</div>

            <div className="text-[#183770] text-lg font-medium">Хүргэлтийн дугаар - {data?.id}</div>
            <div className="text-lg font-semibold">Хүлээн авагчийн мэдээлэл</div>
            <div className="bg-[#f1f1f1] rounded-lg p-5 gap-7 flex flex-col lg:text-lg">
              <div className="flex gap-5 items-center">
                <FaLocationArrow className="text-blue-900" />
                <div className=" font-medium">
                  {data.Street}, {data.Location}
                </div>
              </div>
              <div className="flex gap-5 items-center">
                <FaUser className="text-blue-900" />
                <div className=" font-medium">{data?.Name}</div>
              </div>
              <div className="flex gap-5 items-center">
                <FaPhoneAlt className="text-blue-900" />
                <div className=" font-medium">{data?.PhoneNumber}</div>
              </div>
            </div>
            <div className="font-semibold text-lg">Жолооч: {selected?.Name}</div>
            <div className="text-lg">{selected?.PhoneNumber} / Жолоочийн дугаар /</div>
            <div className="flex justify-between">
              <div className="rounded-full bg-[orange] px-4 py-2 text-[white] font-semibold justify-center items-center">
                {data.Status === "waiting" ? "Баталгаажаагүй" : "Баталгаажсан"}
              </div>
              <div className="p-1 text-[#183770] font-medium text-xl">{data.Price}</div>
            </div>
            <div className="text-lg">{convertToMongolianTime(data?.createdAt)}</div>
          </div>
          {/* ) : ( } */}
          {/* <div
              style={{
                boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
              }}
              className="flex flex-col gap-3 overflow-auto h-66 scrollbar-hide xl:flex-1 p-5 "
            >
              <div className="text-2xl font-semibold">Хүргэлтийн жагсаалт</div>
              <div className=" flex justify-between  items-center overflow-auto scrollbar-hide">
                <input
                  type="text"
                  className="border border-gray-400 rounded-lg px-4 py-2 outline-none w-[70%]"
                  placeholder="Хүргэлтийн id-р хайх"
                  onChange={(e) => setDelid(e.target.value)}
                />
              </div>
              {delivery?.map((el: any, i: any) => {
                if (delid == "" || el?.includes(delid)) {
                  return (
                    <div key={i}>
                      <div className="flex justify-center border border-gray-400 items-center p-2 rounded-md cursor-pointer">
                        <div>{el}</div>
                      </div>
                    </div>
                  );
                }
              })}
            </div> */}
          {/* )} */}
          <div className="flex flex-[1.5] flex-col gap-4 overflow-y-auto">
            <div className="text-2xl font-semibold">Жолоочдын жагсаалт</div>
            <div className="flex justify-between gap-5 items-center overflow-auto scrollbar-hide">
              <input
                type="text"
                className="border border-gray-400 rounded-lg px-4 py-2 outline-none"
                placeholder="Жолоочийн нэрээр хайх"
                onChange={(e) => setName(e.target.value)}
              />
            </div>

            <div>
              <div className="flex justify-between p-2 font-bold border-b border-gray-400 whitespace-nowrap overflow-auto scrollbar-hide">
                <div className="w-[20%] text-center">Нэр</div>
                <div className="w-[20%] text-center">Төлөв</div>
                <div className="w-[30%] text-center shrink-0">Утасны дугаар</div>
              </div>
            </div>
            <div className="flex flex-col gap-3 overflow-auto h-56 scrollbar-hide xl:flex-1">
              {loading ? (
                <Loader color="#183770" />
              ) : (
                currentDrivers?.map((driver: any, i) => {
                  if (
                    driver?.Name?.toLowerCase().includes(name?.toLowerCase()) &&
                    driver.id != "678623f3057973ab31d6ff1c"
                  ) {
                    return (
                      <div key={i}>
                        <div
                          onClick={() => setSelected(driver)}
                          style={{
                            border: selected?.id == driver.id ? "2px solid #51ba5b" : "",
                          }}
                          className="flex justify-between border border-gray-400 items-center p-2 rounded-md cursor-pointer"
                        >
                          <div className="w-[20%] flex justify-center items-center">{driver.Name}</div>
                          {driver.Active ? (
                            <div className="flex w-[30%] text-[#f4a622] text-sm font-bold justify-center items-center">
                              Хүргэлтэнд гарсан
                            </div>
                          ) : (
                            <div className="flex w-[30%] text-[#51ba5b] text-sm font-bold justify-center items-center">
                              Чөлөөтэй
                            </div>
                          )}
                          <div className="w-[30%] flex justify-center items-center">{driver.PhoneNumber}</div>
                        </div>
                      </div>
                    );
                  }
                })
              )}
            </div>
            <Suspense fallback={<div>Loading...</div>}>
              <Pagination
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
                length={length}
                num={driverPerPage}
              />
            </Suspense>

            <div className="flex justify-end">
              <button
                onClick={() => setOpen(false)}
                className="border border-main text-main px-4 text-base py-2 rounded font-medium hover:bg-main hover:text-white transition-colors duration-300 mr-5"
              >
                Буцах
              </button>
              <button
                onClick={() => deliveryChanger(data.id, ["DriverId", selected?.id])}
                className="bg-main px-4 text-base py-2 rounded text-white font-medium hover:bg-main/80 transition-colors duration-300"
                style={{
                  display: selected ? "flex" : "none",
                }}
              >
                {loading2 ? <Loader color="white" /> : "Хадгалах"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DriverModal;
