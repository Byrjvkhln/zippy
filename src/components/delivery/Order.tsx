import React from "react";
import Image from "next/image";

const Order = ({ Name, Img, Price, too }: any) => {
  return (
    <div className="border border-gray-300 rounded-lg p-4">
      <div className="flex  gap-3 mb-3">
        <div className="size-16 rounded-full overflow-hidden">
          <Image src={Img} alt="" width={100} height={100} className="w-full h-full object-cover " />
        </div>
        <div>
          <p className="text-blue-900 font-semibold">{Name}</p>
          <p>{too}ш</p>
        </div>
      </div>

      <div>
        <p className="text-gray-800 font-medium mb-2">
          Үнэ:{" "}
          {Number(Price).toLocaleString("en-US", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
          ₮
        </p>
        <p className="mb-2"> x {too} = </p>
        <p className="font-medium text-base text-blue-900">
          {(Number(too) * Number(Price)).toLocaleString("en-US", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
          ₮
        </p>
      </div>
    </div>
  );
};

export default Order;
