"use client";

import { useState, useEffect } from "react";
import { Card, CardBody, CardFooter, Avatar } from "@material-tailwind/react";
import Pagination from "../Pagination";
import { FiEye } from "react-icons/fi";
import DriverModal from "./DriverModal";
import Link from "next/link";
import NoData from "../NoData";
import { useMemo } from "react";
import { type MRT_ColumnDef } from "material-react-table";
import Table from "@/src/components/Table";

// const TABLE_HEAD = ["№", "Захиалагч", "Хаяг", "Дүүрэг", "Утас", "Төлөв", "Хүргэлтийн ажилтан", ""];

export function Orders({}: // user,
// customerId,
// orders,
// refetch,
// fetchOrders,
// many,
// open,
// setOpen,
// selectedOrder,
// setSelectedOrder,
// setMany,
any) {
  const [open, setOpen] = useState(false);
  const [data, setData] = useState([]);
  const [selected, setSelected] = useState({});
  const moduleopener = async (delivery: any) => {
    console.log({ delivery });

    setSelected(delivery);
    setOpen(true);
  };
  console.log({ selected });

  const deliveryColumns = useMemo(
    () => [
      {
        accessorKey: "index", //access nested data with dot notation
        header: "№",
        size: 50,
        Cell: ({ row }: any) => row.index + 1,
        enableColumnFilter: false,
      },
      {
        accessorKey: "Name",
        header: "Захиалагч",
      },
      {
        accessorKey: "Location",
        header: "Хаяг",
      },
      {
        accessorKey: "Street",
        header: "Дүүрэг",
      },
      {
        accessorKey: "PhoneNumber",
        header: "Утас",
      },
      {
        accessorKey: "Status",
        header: "Төлөв",
        Cell: ({ row }: any) => {
          const status = row.original.Status;
          return (
            <p
              className={`py-2 text-center px-2 rounded-full border ${
                status === "waiting"
                  ? "border-red1"
                  : status === "done"
                  ? "border-green1"
                  : status === "comfirmed"
                  ? "border-main"
                  : status === "cancel"
                  ? "border-red1"
                  : status === "upcoming"
                  ? "border-yellow1"
                  : "border-gray-400"
              }`}
            >
              {status === "waiting"
                ? "Хүлээгдэж буй"
                : status === "comfirmed"
                ? "Баталгаажсан"
                : status === "upcoming"
                ? "Хүргэлтэнд гарсан"
                : status === "done"
                ? "Хүргэгдсэн"
                : status === "cancel"
                ? "Цуцлагдсан"
                : "Хойшилсон"}
            </p>
          );
        },
      },
      {
        accessorKey: "Driver.Name",
        header: "Хүргэлтийн ажилтан",
        Cell: ({ row }: any) => {
          const Driver = row?.original?.Driver;
          const Delivery = row?.original;
          return Driver?.id !== "678623f3057973ab31d6ff1c" ? (
            <div>{Driver?.Name}</div>
          ) : (
            <button
              onClick={() => moduleopener(Delivery)}
              className="bg-blue-900 px-4 py-2 font-semibold text-white rounded-lg hover:bg-main duration-300 transition-colors"
            >
              Жолооч хуваарилах
            </button>
          );
        },
      },
      {
        header: "Үзэх",
        Cell: ({ row }: any) => {
          const original = row.original;
          return (
            <div>
              <Link href={`/dashboard/orders/${original.id}`} className="underline">
                харах
              </Link>
            </div>
          );
        },
      },
    ],
    []
  );
  // const [currentPage, setCurrentPage] = useState(1);
  // const orderPerPage = 150;
  // const length = orders.length;
  // console.log(orders);x

  // useEffect(() => {
  //   setCurrentPage(1);
  // }, [open, customerId, refetch]);
  // useEffect(() => {
  //   fetchOrders({ customerId, currentPage, orderPerPage });
  // }, [currentPage, open, , refetch]);

  // const handleDriverAssign = (order: any) => {
  //   setSelectedOrder(order);
  //   setOpen(true);
  // };
  console.log({ lalrindata: data });
  console.log("data ", data);

  return (
    <>
      <Table
        tableurl={"/api/delivery/customerid"}
        columns={deliveryColumns}
        findid={null}
        setData={setData}
        data={data}
      ></Table>
      {open && <DriverModal data={selected} open={open} setOpen={setOpen} />}
    </>
    // <>
    //   {orders.length > 0 ? (
    //     <Card className="h-full w-full rounded-none shadow-none" {...({} as any)}>
    //       <CardBody className="overflow-auto !p-0 " {...({} as any)}>
    //         <table className="w-full min-w-max table-auto text-left">
    //           <thead>
    //             <tr>
    //               {TABLE_HEAD.map((head) => (
    //                 <th key={head} className="bg-blue-gray-200/50 p-4">
    //                   <p>{head}</p>
    //                 </th>
    //               ))}
    //             </tr>
    //           </thead>
    //           <tbody>
    //             {orders?.map((order: any, index: any) => {
    //               const isLast = index === orders.length - 1;
    //               const classes = isLast ? "p-4" : "p-4 border-b border-blue-gray-100";
    //               const itemIndex = (currentPage - 1) * 20 + index + 1;
    //               return (
    //                 <tr key={index}>
    //                   <td className={classes}>
    //                     <p>{itemIndex}</p>
    //                   </td>
    //                   <td className={classes}>
    //                     <div className="flex items-center gap-3">
    //                       <Avatar
    //                         src="https://wallpapers.com/images/hd/profile-picture-xj8jigxkai9jag4g.jpg"
    //                         alt=""
    //                         size="sm"
    //                         {...({} as any)}
    //                       />
    //                       <div className="flex flex-col">
    //                         <p>{order.Name}</p>
    //                       </div>
    //                     </div>
    //                   </td>

    //                   <td className={classes}>
    //                     <p className="max-w-[200px]">{order.Location}</p>
    //                   </td>
    //                   <td className={classes}>
    //                     <p>{order.Street}</p>
    //                   </td>
    //                   <td className={classes}>
    //                     <p>{order.PhoneNumber}</p>
    //                   </td>

    //                   <td className={classes}>
    //                     <p
    //                       className={`py-2 text-center px-2 rounded-full border ${
    //                         order.Status === "waiting"
    //                           ? "border-red1"
    //                           : order.Status === "done"
    //                           ? "border-green1"
    //                           : order.Status === "comfirmed"
    //                           ? "border-main"
    //                           : order.Status === "cancel"
    //                           ? "border-red1"
    //                           : order.Status === "upcoming"
    //                           ? "border-yellow1"
    //                           : "border-gray-400"
    //                       }`}
    //                     >
    //                       {order.Status === "waiting"
    //                         ? "Хүлээгдэж буй"
    //                         : order.Status === "comfirmed"
    //                         ? "Баталгаажсан"
    //                         : order.Status === "upcoming"
    //                         ? "Хүргэлтэнд гарсан"
    //                         : order.Status === "done"
    //                         ? "Хүргэгдсэн"
    //                         : order.Status === "cancel"
    //                         ? "Цуцлагдсан"
    //                         : "Хойшилсон"}
    //                     </p>
    //                   </td>
    //                   <td className={classes}>
    //                     {user.Status !== "information" && order.Status === "waiting" ? (
    //                       <p className="capitalize">Баталгаажаагүй</p>
    //                     ) : order.DriverId !== "678623f3057973ab31d6ff1c" ? (
    //                       <p className="capitalize">{order.Driver.Surname[0] + "." + order.Driver.Name}</p>
    //                     ) : many == true ? (
    //                       !selectedOrder?.includes(order?.id) ? (
    //                         <button
    //                           onClick={() => {
    //                             setSelectedOrder([...selectedOrder, order?.id]);
    //                           }}
    //                           className="bg-blue-900 px-4 py-2 font-semibold text-white rounded-lg hover:bg-main duration-300 transition-colors"
    //                         >
    //                           Жолооч хуваарилах
    //                         </button>
    //                       ) : (
    //                         <button
    //                           onClick={() => {
    //                             setSelectedOrder(() => selectedOrder.filter((el: any) => el != order?.id));
    //                           }}
    //                           className="border border-blue-900 px-4 py-2 font-semibold rounded-lg "
    //                         >
    //                           Хасах
    //                         </button>
    //                       )
    //                     ) : (
    //                       <button
    //                         onClick={() => {
    //                           handleDriverAssign(order);
    //                         }}
    //                         className="bg-blue-900 px-4 py-2 font-semibold text-white rounded-lg hover:bg-main duration-300 transition-colors"
    //                       >
    //                         Жолооч хуваарилах
    //                       </button>
    //                     )}
    //                   </td>

    //                   <td className={classes}>
    //                     <div className="flex text-lg items-center gap-3 ">
    //                       <Link
    //                         href={`/dashboard/orders/${order.id}`}
    //                         className="size-8 flex items-center justify-center border border-gray-400 rounded cursor-pointer hover:border-black hover:text-black transition-all duration-300"
    //                       >
    //                         <FiEye />
    //                       </Link>
    //                     </div>
    //                   </td>
    //                 </tr>
    //               );
    //             })}

    //
    //           </tbody>
    //         </table>
    //       </CardBody>
    //       <CardFooter className="flex justify-end border-t border-gray-400 p-4" {...({} as any)}>
    //         <Pagination currentPage={currentPage} setCurrentPage={setCurrentPage} length={length} num={orderPerPage} />
    //       </CardFooter>
    //     </Card>
    //   ) : (
    //     <NoData />
    //   )}
    // </>
  );
}
