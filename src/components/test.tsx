// src/app/test.tsx
"use client";
import { useEffect } from "react";
import { useRouter } from "next/navigation";
import { useTokenHandler } from "../utils/tokenhandler";
const TestComponent = () => {
  useTokenHandler();
  return <></>;
};

export default TestComponent;
