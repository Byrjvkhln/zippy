import React, { useEffect, useState } from "react";
import Image from "next/image";
import { count } from "console";
const Loading = () => {
  const [width, setWidth] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setWidth((prevWidth) => prevWidth + 1);
    }, 10); // Increase width gradually every 50 milliseconds

    // Clear interval after 5 seconds (5000 milliseconds)
    setTimeout(() => {
      clearInterval(interval);
    }, 1000);

    return () => {
      clearInterval(interval); // Clean up interval on component unmount
    };
  }, []);
  const fillLineStyle = {
    width: `${width}%`,
    height: "100%",
    background: "linear-gradient(90deg, rgba(34,193,195,1) 0%, rgba(45,253,88,1) 100%)",
    transition: "width 0.05s linear", // Adjust transition speed here
  };

  return (
    <div className="flex justify-center items-center flex-col gap-7 h-full ">
      <div className="size-16 relative ">
        <Image src="/logo.svg" alt="" fill />
      </div>
      <div className=" h-2 w-[200px] rounded-md">
        <div className="rounded-md fill-line " style={fillLineStyle}></div>
      </div>
    </div>
  );
};

export default Loading;
