import React, { useEffect, useState } from "react";
import Image from "next/image";
import { count } from "console";
const Adding = ({ width, data }: any) => {
  const fillLineStyle = {
    width: `${width}%`,
    height: "5px",
    background: data.color,
    transition: "width 0.05s linear", // Adjust transition speed here
  };

  return (
    <div
      style={{
        boxShadow: "flex rgba(149, 157, 165, 0.2) 0px 8px 24px",
        position: "absolute",
        top: "60px",
        backgroundColor: "white",
      }}
      className=" flex justify-center items-center flex-col rounded-md overflow-hidden"
    >
      <div className=" flex justify-center items-center gap-3  p-4">
        <Image src={data.image} alt="" width={22} height={22} />
        <div>{data.text}</div>
      </div>
      <div className="fill-line " style={fillLineStyle}></div>
    </div>
  );
};
export default Adding;
{
  /* <div className="size-10 relative flex justify-center items-center">
        <Image src="/logo.svg" alt="" fill />
        <div>Мэдээлэл амжилттай нэмэгдлээ</div>
      </div> */
}
{
  /* <div className=" h-2 w-[200px] rounded-md">
        <div className="rounded-md fill-line " style={fillLineStyle}></div>
      </div> */
}
