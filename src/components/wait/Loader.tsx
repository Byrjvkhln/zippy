import React from 'react'
import { Ring } from "@uiball/loaders";

const Loader = ({color}:any) => {
  return (
    <div className=" flex justify-center items-center ">
      <Ring size={25} color={color} />
    </div>
  );
}

export default Loader
