"use client";

import React, { useState } from "react";
import { Menu, MenuHandler, MenuList, MenuItem } from "@material-tailwind/react";
import { FaChevronDown, FaChevronUp } from "react-icons/fa6";

const Dropdown = ({ selectedCategory, setSelectedCategory, type }: any) => {
  const categories: any = {
    req: [
      { id: 1, label: "Бүгд" },
      { id: 2, label: "Хариу ирээгүй" },
      { id: 3, label: "Зөвшөөрсөн" },
      { id: 4, label: "Татгалзсан" },
    ],
    tuluv: [
      { id: 1, label: "Бүгд" },
      { id: 2, label: "Хүргэгдсэн" },
      { id: 3, label: "Баталгаажсан" },
      { id: 4, label: "Хүлээгдэж буй" },
      { id: 5, label: "Цуцлагдсан" },
      { id: 6, label: "Хойшилсон" },
    ],
    street: [
      { id: 1, label: "Сонгино хайрхан дүүрэг" },
      { id: 2, label: "Баянгол дүүрэг" },
      { id: 3, label: "Хан-Уул дүүрэг" },
      { id: 4, label: "Баянзүрх дүүрэг" },
      { id: 5, label: "Сүхбаатар дүүрэг" },
      { id: 6, label: "Чингэлтэй дүүрэг" },
      { id: 7, label: "Налайх дүүрэг" },
      { id: 8, label: "Багахангай дүүрэг" },
      { id: 9, label: "Багануур дүүрэг" },
    ],
  };
  const [open, setOpen] = useState(false);

  return (
    <Menu {...({} as any)} open={open} handler={setOpen}>
      <MenuHandler>
        <div className="w-52 border  border-gray-400 rounded-lg flex items-center px-4 justify-between">
          {selectedCategory ? selectedCategory : "Бүгд"}
          {open ? <FaChevronUp /> : <FaChevronDown />}
        </div>
      </MenuHandler>

      <MenuList {...({} as any)} className="text-gray-900 w-52">
        {categories[type].map((c: any) => (
          <MenuItem {...({} as any)} key={c.id} onClick={() => setSelectedCategory(c.label)}>
            {c.label}
          </MenuItem>
        ))}
      </MenuList>
    </Menu>
  );
};

export default Dropdown;
