"use client";

import React, { useState, Suspense } from "react";
import { Card, Button, CardBody, CardFooter } from "@material-tailwind/react";
import Pagination from "./Pagination";
import Link from "next/link";
import { FiEye } from "react-icons/fi";

const TABLE_HEAD = ["№", "Хаяг", "Утас", "Бараа", "Нийт тооцоо", "Х/Төлбөр", "Х/Ажилтан", ""];

const DeliveryReport = ({
  customers,
  deliveries,
  setCurrentPage,
  currentPage,
  deliveryPerPage,
  setChosen,
  chosen,
}: any) => {
  const customerIds = deliveries?.map((d: any) => d.CustomerId);
  const cost = Number(customers?.find((c: any) => customerIds?.includes(c.id))?.TransportCost);

  const length = deliveries?.length;

  const indexOfLastItem = currentPage * deliveryPerPage;
  const indexOfFirstItem = indexOfLastItem - deliveryPerPage;
  const currentDeliveries = deliveries?.slice(indexOfFirstItem, indexOfLastItem);

  return (
    <>
      <Card className="h-full w-full rounded-none shadow-none" {...({} as any)}>
        <CardBody className="overflow-auto  !p-0 " {...({} as any)}>
          <table className=" w-full min-w-max table-auto text-left">
            <thead>
              <tr>
                {TABLE_HEAD.map((head) => (
                  <th key={head} className=" bg-blue-gray-200/50 p-4">
                    <p>{head}</p>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {currentDeliveries?.map((d: any, index: any) => {
                const isLast = index === deliveries.length - 1;
                const classes = isLast ? "p-4 " : "p-4 border-b border-blue-gray-100 ";

                return (
                  <tr
                    onClick={() => {
                      if (!chosen.map((el: any) => el.id).includes(d?.id)) {
                        setChosen([
                          ...chosen,
                          {
                            id: d?.id,
                            Price: d?.Price,
                          },
                        ]);
                      } else {
                        setChosen(chosen.filter((el: any) => el?.id != d?.id));
                      }
                    }}
                    key={d?.id}
                    style={{
                      backgroundColor: chosen.map((el: any) => el.id).includes(d?.id) ? "#90EE90" : "",
                      cursor: "pointer",
                    }}
                  >
                    <td className={classes}>{d?.id}</td>
                    <td className={classes}>
                      <p className="max-w-[170px]">{d?.Location}</p>
                    </td>
                    <td className={classes}>
                      <p>{d?.PhoneNumber}</p>
                    </td>
                    <td className={classes}>
                      <p>{d?.Product?.length}</p>
                    </td>
                    <td className={classes}>
                      <p>
                        {Number(d?.Price).toLocaleString("en-US", {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })}
                        ₮
                      </p>
                    </td>

                    <td className={classes}>
                      <p>
                        {cost.toLocaleString("en-US", {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })}
                        ₮
                      </p>
                    </td>

                    <td className={classes}>
                      <p className="capitalize">{d?.Driver?.Surname[0] + "." + d?.Driver?.Name}</p>
                    </td>
                    <td className={classes}>
                      <Link
                        target="_blank"
                        href={`/dashboard/orders/${d.id}`}
                        className="size-6 flex items-center justify-center border border-gray-400 rounded cursor-pointer hover:border-black hover:text-black transition-all duration-300"
                      >
                        <FiEye />
                      </Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </CardBody>
        <Suspense fallback={<div>Loading...</div>}>
          <Pagination currentPage={currentPage} setCurrentPage={setCurrentPage} length={length} num={deliveryPerPage} />
        </Suspense>
      </Card>
    </>
  );
};

export default DeliveryReport;
