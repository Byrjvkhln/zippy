"use client";

import React, { useState, useEffect, Suspense } from "react";
import { useMemo } from "react";
import { type MRT_ColumnDef } from "material-react-table";
import { convertToMongolianTime } from "@/src/utils/timeConvertor";
import Table from "@/src/components/Table";
import Link from "next/link";
// import { Card, CardBody, CardFooter } from "@material-tailwind/react";
// import { FaCircleCheck } from "react-icons/fa6";
// import axios from "axios";
// import Pagination from "../Pagination";
// import Loader from "../wait/Loader";
// import NoData from "../NoData";

// const TABLE_HEAD = [
//   "",
//   "№",
//   "Захиалгын дугаар",
//   "Хүлээн авагч",
//   "Утас",
//   "Хаяг",
//   "Дүүрэг",
//   "Жолооч",
//   "Б/тоо",
//   "Авах дүн",
//   "Төлөв",
//   "З/Огноо",
//   "",
// ];

const Deliveries = ({ customerid }: any) => {
  const deliveryColumns = useMemo(
    () => [
      {
        accessorKey: "index", //access nested data with dot notation
        header: "№",
        size: 50,
        Cell: ({ row }: any) => row.index + 1,
        enableColumnFilter: false,
      },
      {
        accessorKey: "id",
        header: "Захиалгын дугаар",
      },
      {
        accessorKey: "Name",
        header: "Хүлээн авагч",
      },
      {
        accessorKey: "PhoneNumber",
        header: "Утас",
      },
      {
        accessorKey: "Location",
        header: "Хаяг",
      },

      {
        accessorKey: "Street",
        header: "Дүүрэг",
      },
      {
        accessorKey: "Driver.Name",
        header: "Хүргэлтийн ажилтан",
      },
      {
        accessorKey: "Price",
        header: "Авах дүн",
      },
      {
        accessorKey: "Status",
        header: "Төлөв",
        Cell: ({ row }: any) => {
          const status = row.original.Status;
          return (
            <p
              className={`py-2 px-3 font-medium ${
                status === "comfirmed"
                  ? "bg-main"
                  : status === "upcoming"
                  ? "bg-yellow1"
                  : status === "done"
                  ? "bg-green1"
                  : "bg-[#e3242b]"
              }  text-white rounded-3xl text-center`}
            >
              {status === "waiting"
                ? "Баталгаажаагүй"
                : status === "comfirmed"
                ? "Баталгаажсан"
                : status === "upcoming"
                ? "Хүргэлтэнд гарсан"
                : status === "cancel"
                ? "Цуцлагдсан"
                : status === "done"
                ? "Хүргэгдсэн"
                : "Хойшилсон"}
            </p>
          );
        },
      },

      {
        header: "З.Огноо",
        Cell: ({ row }: any) => {
          const createdAt = row.original.createdAt;
          return <div>{convertToMongolianTime(createdAt)}</div>;
        },
      },
      {
        header: "Үзэх",
        Cell: ({ row }: any) => {
          const original = row.original;
          return (
            <div>
              <Link href={`/dashboard/orders/${original.id}`} className="underline">
                харах
              </Link>
            </div>
          );
        },
      },
    ],
    []
  );
  const [data, setData] = useState([]);
  console.log({ data });

  // const [deliveries, setDeliveries] = useState([]);
  // const [currentPage, setCurrentPage] = useState(1);
  // const [loading, setLoading] = useState(true);
  // const length = deliveries?.length;
  // const driverPerPage = 100;

  // const indexOfLastItem = currentPage * driverPerPage;
  // const indexOfFirstItem = indexOfLastItem - driverPerPage;
  // const currentDeliveries = deliveries.slice(indexOfFirstItem, indexOfLastItem);

  // useEffect(() => {
  //   const fetchDeliveries = async () => {
  //     try {
  //       const res = await axios.post("  /api/delivery/customerid", {
  //         customerId: JSON.parse(localStorage.getItem("user") || "{}")?.id,
  //         page: currentPage,
  //         pageSize: driverPerPage,
  //       });
  //       setDeliveries(res.data);
  //       setLoading(false);

  //       if (res.status === 200) setReFetch(false);
  //     } catch (error) {
  //       console.error(error);
  //     }
  //   };

  //   fetchDeliveries();
  // }, [refetch]);

  return (
    <>
      {/* {loading ? (
        <Loader color="#183770" />
      ) : deliveries.length > 0 ? (
        <Card className="h-full w-full rounded-none shadow-none" {...({} as any)}>
          <CardBody className="overflow-auto  !p-0 " {...({} as any)}>
            <table className=" w-full min-w-max table-auto text-left">
              <thead>
                <tr>
                  {TABLE_HEAD.map((head) => (
                    <th key={head} className=" bg-blue-gray-200/50 py-4 px-2">
                      <p>{head}</p>
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {currentDeliveries?.map((d: any, index) => {
                  const isLast = index === deliveries.length - 1;
                  const classes = isLast ? "px-2 py-4" : "px-2 py-4 border-b border-blue-gray-100 ";
                  const itemIndex = indexOfFirstItem + index + 1;

                  return (
                    <tr key={d.id} className={`${d.Status === "done" && "bg-green-100"}`}>
                      <td className={classes}>
                        <FaCircleCheck className={`text-xl ${d.Status === "done" && "text-green1"}`} />
                      </td>
                      <td className={classes}>
                        <p>{itemIndex}</p>
                      </td>
                      <td className={classes}>
                        <p className="max-w-[150px] line-clamp-1">{d.id}</p>
                      </td>
                      <td className={classes}>
                        <p>{d.Name}</p>
                      </td>

                      <td className={classes}>
                        <p>{d.PhoneNumber}</p>
                      </td>
                      <td className={classes}>
                        <p className="max-w-[180px]">{d.Location}</p>
                      </td>
                      <td className={classes}>
                        <p>{d.Street}</p>
                      </td>
                      <td className={classes}>
                        <p>{d.Status === "waiting" ? "Баталгаажаагүй" : d.Driver.Name}</p>
                      </td>
                      <td className={classes}>
                        <p>{d.Product.map((el: any) => JSON.parse(el).too)} ш</p>
                      </td>
                      <td className={classes}>
                        <p>
                          {Number(d.Price)?.toLocaleString("en-US", {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          })}
                          ₮
                        </p>
                      </td>
                      <td className={classes}>
                        <p
                          className={`py-2 px-3 font-medium ${
                            d.Status === "comfirmed"
                              ? "bg-main"
                              : d.Status === "upcoming"
                              ? "bg-yellow1"
                              : d.Status === "done"
                              ? "bg-green1"
                              : "bg-[#e3242b]"
                          }  text-white rounded-3xl text-center`}
                        >
                          {d.Status === "waiting"
                            ? "Баталгаажаагүй"
                            : d.Status === "comfirmed"
                            ? "Баталгаажсан"
                            : d.Status === "upcoming"
                            ? "Хүргэлтэнд гарсан"
                            : d.Status === "cancel"
                            ? "Цуцлагдсан"
                            : d.Status === "done"
                            ? "Хүргэгдсэн"
                            : "Хойшилсон"}
                        </p>
                      </td>

                      <td className={classes}>
                        <p>{d.createdAt.split("T")[0]}</p>
                      </td>
                      <td className={classes}>
                        <Link href={`/dashboard/orders/${d.id}`} className="underline">
                          Үзэх
                        </Link>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </CardBody>
          <CardFooter className="flex items-center justify-end border-t border-blue-gray-50 p-4" {...({} as any)}>
            <Suspense fallback={<div>Loading...</div>}>
              <Pagination
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
                length={length}
                num={driverPerPage}
              />
            </Suspense>
          </CardFooter>
        </Card>
      ) : (
        <NoData />
      )} */}
      <>
        {customerid && (
          <Table
            tableurl={"/api/delivery/customerid"}
            columns={deliveryColumns}
            findid={customerid}
            data={data}
            setData={setData}
          ></Table>
        )}
      </>
    </>
  );
};

export default Deliveries;
