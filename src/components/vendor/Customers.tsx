import React, { useState, useEffect, Suspense } from "react";
import Pagination from "../Pagination";
import NoData from "../NoData";
import { Card, CardBody, CardFooter, Avatar, Switch } from "@material-tailwind/react";
import axios from "axios";
import { FiEye } from "react-icons/fi";
import Link from "next/link";
import Loader from "../wait/Loader";

const TABLE_HEAD = ["Харилцагч", "Утас", "И-мэйл", "Хүргэлтийн үнэ", "Бүртгэгдсэн", ""];

const Customers = ({ show, fetchCustomers, customers, loading, refetch }: any) => {
  const [currentPage, setCurrentPage] = useState(1);
  const length = customers?.length;

  const customerPerPage = 100;

  const indexOfLastItem = currentPage * customerPerPage;
  const indexOfFirstItem = indexOfLastItem - customerPerPage;
  const currentCustomers = customers.slice(indexOfFirstItem, indexOfLastItem);

  useEffect(() => {
    fetchCustomers({ currentPage, customerPerPage });
  }, [show, refetch]);

  return (
    <>
      {loading ? (
        <Loader color="#183770" />
      ) : customers.length > 0 ? (
        <Card className=" w-full rounded-none overflow-auto shadow-none " {...({} as any)}>
          <CardBody className=" !p-0 " {...({} as any)}>
            <table className=" w-full min-w-max table-auto text-left">
              <thead>
                <tr className="">
                  {TABLE_HEAD.map((head) => (
                    <th key={head} className=" bg-blue-gray-200/50  py-4">
                      <p className="max-w-[200px] text-center px-4">{head}</p>
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {currentCustomers?.map((order: any, index: any) => {
                  const isLast = index === customers.length - 1;
                  const classes = isLast ? " py-4 " : " py-4";

                  return (
                    <tr key={order?.id}>
                      <td className={classes}>
                        <div className="flex items-center gap-3 ml-2">
                          <Avatar src={order.Image} alt="" size="sm" {...({} as any)} />
                          <div className="flex flex-col">
                            <p className="max-w-[200px] text-center line-clamp-1">{order.Name}</p>
                          </div>
                        </div>
                      </td>
                      <td className={classes}>
                        <p className="max-w-[200px] text-center">{order.PhoneNumber}</p>
                      </td>
                      <td className={classes}>
                        <p className="max-w-[200px] text-center">{order.Email}</p>
                      </td>
                      <td className={classes}>
                        <p className="max-w-[200px] text-center">
                          {Number(order.TransportCost).toLocaleString("en-US", {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          })}
                          ₮
                        </p>
                      </td>

                      <td className={classes}>
                        <p className=" max-w-[200px] text-center">{order.createdAt.split("T")[0]}</p>
                      </td>
                      <td className={classes}>
                        <Link
                          href={`/dashboard/vendor/${order.id}`}
                          className="size-8 flex items-center justify-center border border-gray-400 rounded hover:border-black hover:text-black transition-colors duration-300"
                        >
                          <FiEye />
                        </Link>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </CardBody>
          <CardFooter className="flex items-center justify-end border-t border-gray-400 p-4" {...({} as any)}>
            <Suspense fallback={<div>Loading...</div>}>
              <Pagination
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
                length={length}
                num={customerPerPage}
              />
            </Suspense>
          </CardFooter>
        </Card>
      ) : (
        <NoData />
      )}
    </>
  );
};

export default Customers;
