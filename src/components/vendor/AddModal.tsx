"use client";

import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import { AiOutlineClose } from "react-icons/ai";
import FormInput from "../../components/delivery-worker/FormInput";
import { ImageUploader } from "../../utils/imageuploader";
import "react-color-palette/css";
import Loader from "../wait/Loader";
import { FiEye, FiEyeOff } from "react-icons/fi";
import Image from "next/image";

const AddModal = ({ setShow, show }: any) => {
  const [loading, setLoading] = useState(false);
  const file: any = useRef(null);
  const [img, setImg] = useState<any>(null);
  const [image, setImage] = useState<any>("");
  const [latitude, setLatitude] = useState<any>("");
  const [longtitude, setLongtitude] = useState<any>("");
  const [hide, setHide] = useState(false);
  const [formValues, setFormValues] = useState({
    name: "",
    phone: "",
    email: "",
    payment: "",
    password: "",
    driverpayment: "",
  });

  const handler = () => {
    setShow(false);
  };

  useEffect(() => {
    if (img) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setImage(reader.result);
      };
      reader.readAsDataURL(img);
    }
  }, [img]);

  useEffect(() => {
    if (show) {
      document.body.classList.add("overflow-hidden");
    } else {
      document.body.classList.remove("overflow-hidden");
    }
    return () => {
      document.body.classList.remove("overflow-hidden");
    };
  }, [show]);

  const handleChange = (e: any, field: string) => {
    setFormValues({
      ...formValues,
      [field]: e.target.value,
    });
  };

  const send = async () => {
    setLoading(true);
    const res = await axios.post("/api/customer", {
      Email: formValues.email,
      Name: formValues.name,
      PhoneNumber: formValues.phone,
      Password: formValues.password,
      TransportCost: formValues.payment,
      Location: [latitude, longtitude],
      Image: ImageUploader(img),
      DriverSalary: formValues.driverpayment,
    });

    if (res.status === 200) {
      setLoading(false);
      setShow(false);
    }
  };

  // const handleImageChange = (e: any) => {
  //   if (e.target.files && e.target.files[0]) {
  //     setImg(e.target.files[0]);
  //   }
  // };

  return (
    <div className="fixed inset-0 top-0 z-10 flex items-center justify-center w-full h-full   ">
      <div
        className="fixed inset-0 top-0 w-full h-full  bg-black/50 backdrop-blur-sm overflow-y-auto py-10 flex items-center justify-center"
        onClick={handler}
      >
        <div
          onClick={(e) => e.stopPropagation()}
          className=" max-w-[600px] mx-auto w-[95%] max-h-fit bg-white px-6 py-8  rounded-xl text-gray-900 mt-[20vh]  "
        >
          <div className="flex items-center justify-between w-full border-b pb-3 border-gray-400 mb-5">
            <h2 className="text-xl font-bold">Шинэ харилцагч нэмэх</h2>
            <AiOutlineClose className="text-xl cursor-pointer" onClick={() => setShow(!show)} />
          </div>

          <FormInput text="Нэр" onChange={(e: any) => handleChange(e, "name")} value={formValues.name} />
          <FormInput
            text="И-мэйл"
            type="email"
            onChange={(e: any) => handleChange(e, "email")}
            value={formValues.email}
          />
          <FormInput text="Утас" onChange={(e: any) => handleChange(e, "phone")} value={formValues.phone} />

          <p>Байршил</p>
          <div className="w-[100%]  flex justify-between">
            <div className="w-[48%]">
              <FormInput placeholder="Уртраг" onChange={(e: any) => setLongtitude(e.target.value)} value={longtitude} />
            </div>
            <div className="w-[48%]">
              <FormInput placeholder="Өргөрөг" onChange={(e: any) => setLatitude(e.target.value)} value={latitude} />
            </div>
          </div>

          <FormInput
            text="Хүргэлтийн төлбөр"
            onChange={(e: any) => handleChange(e, "payment")}
            value={formValues.payment}
          />

          <FormInput
            text="Жолоочид очих цалин"
            onChange={(e: any) => handleChange(e, "driverpayment")}
            value={formValues.driverpayment}
          />

          <div className="mb-5">
            <label htmlFor="uploadImg">
              <div className="size-40 border rounded-full border-gray-400 flex items-center justify-center cursor-pointer overflow-hidden mx-auto">
                {image ? (
                  <Image src={image} alt="" width={100} height={120} className="w-full h-full object-cover" />
                ) : (
                  "Зураг хуулах"
                )}
              </div>
            </label>
            <input
              ref={file}
              type="file"
              id="uploadImg"
              className="hidden"
              onChange={(e: any) => setImg(e.target.files[0])}
            />
          </div>

          <p className="mb-3">Нууц үг</p>
          <div className="flex items-center border px-4 py-2 rounded-lg border-gray-400 gap-5 mb-5">
            <input
              type={hide ? "text" : "password"}
              className="outline-none w-full"
              onChange={(e: any) => handleChange(e, "password")}
              value={formValues.password}
            />

            {hide ? (
              <FiEye className="text-lg cursor-pointer" onClick={() => setHide(!hide)} />
            ) : (
              <FiEyeOff className="text-lg cursor-pointer" onClick={() => setHide(!hide)} />
            )}
          </div>

          <button
            onClick={send}
            className="bg-main text-white rounded-lg py-2 w-full px-4 font-medium text-base hover:bg-blue-900 transition-colors duration-300"
          >
            {loading ? <Loader color="white" /> : "Хадгалах"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default AddModal;
