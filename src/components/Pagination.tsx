"use client";

import React, { useEffect } from "react";
import { useRouter, usePathname, useSearchParams } from "next/navigation";

const Pagination = ({ length, currentPage, setCurrentPage, num }: any) => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const newsPerPage = num;
  const totalPages = Math.ceil(length / newsPerPage);

  useEffect(() => {
    const page = parseInt(searchParams.get("page") || "1", 10);
    setCurrentPage(page);
  }, [searchParams, setCurrentPage]);

  const next = () => {
    const nextPage = currentPage + 1;
    router.push(`${pathname}?page=${nextPage}`);
  };

  const prev = () => {
    if (currentPage > 1) {
      const prevPage = currentPage - 1;
      router.push(`${pathname}?page=${prevPage}`);
    }
  };

  return (
    <>
      <div className="flex items-center justify-end gap-5 px-5 h-14">
        <p>Хуудас: {currentPage}</p>
        {length > 0 && (
          <div>
            <p>
              {currentPage} - {totalPages}
            </p>
          </div>
        )}
        <div className="flex items-center gap-3">
          <svg
            onClick={prev}
            aria-label="Previous Page"
            width="12"
            height="14"
            viewBox="0 0 12 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className={`cursor-pointer ${currentPage === 1 ? "opacity-50" : ""}`}
          >
            <path
              d="M10.1911 19.8151L0 9.90755L10.1911 0L12 1.75859L3.61783 9.90755L12 18.0565L10.1911 19.8151Z"
              fill="currentColor"
            />
          </svg>
          <svg
            onClick={next}
            aria-label="Next Page"
            width="11"
            height="14"
            viewBox="0 0 11 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className={`cursor-pointer ${currentPage === totalPages ? "opacity-50" : ""}`}
          >
            <path d="M1.65817 20L11 10L1.65817 0L0 1.775L7.68365 10L0 18.225L1.65817 20Z" fill="currentColor" />
          </svg>
        </div>
      </div>
    </>
  );
};

export default Pagination;
