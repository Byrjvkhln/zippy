import React from 'react'
import {
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
} from "@material-tailwind/react";
import { AiOutlineClose } from "react-icons/ai";
import Image from 'next/image';
import { TbSquaresSelected } from 'react-icons/tb';

const NotifModal = ({selected, show, setShow}:any) => {

  return (
    <Dialog
      {...({} as any)}
    open={show}
    handler={() =>setShow(!show)}
      size="xs"
    >
      <DialogHeader {...({} as any)}>
        <div className="flex items-center justify-between w-full">
        Мэдэгдэл
          <AiOutlineClose
            className="text-xl cursor-pointer"
           onClick={() => setShow(false)}
          />
        </div>
      </DialogHeader>
      <DialogBody {...({} as any)} className='text-gray-900'>
        <h2 className='font-semibold mb-5'>{selected.Title}</h2>
        {selected.Image && <Image src={selected.Image} alt='' width={500} height={500} className='w-full h-full mb-5'/>}
       {selected.Discription}
      </DialogBody>
      <DialogFooter {...({} as any)}>
      </DialogFooter>
    </Dialog>
  );
}

export default NotifModal
