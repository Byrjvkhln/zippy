"use client";

import React, { useState, useEffect } from "react";
import { Button, Menu, MenuHandler, MenuList, MenuItem } from "@material-tailwind/react";
import axios from "axios";
import { FaMessage } from "react-icons/fa6";
import { convertToMongolianTime } from "../utils/timeConvertor";
import NotifModal from "./NotifModal";

const Notif = () => {
  const [news, setNews] = useState([]);
  const [too, setToo] = useState<any>(0);
  const [show, setShow] = useState(false);
  const [selected, setSelected] = useState<any>(null);

  const handleModal = (notif: any) => {
    setSelected(notif);
    setShow(true);
  };

  const getNews = async () => {
    let a: any = await axios.get("/api/news", {
      headers: {
        "x-api-key": "Lol999za",
      },
    });
    a = await a?.data
      .filter((d: any) => d.For.includes("Харилцагчдад"))
      .reverse()
      .slice(0, 6);
    setNews(a);

    const b = a?.data?.filter((el: any) => !el?.read?.includes(JSON.parse(localStorage.getItem("user") || "{}").id));
    setToo(b?.length);
  };

  useEffect(() => {
    getNews();

    const interval = setInterval(() => {
      getNews();
    }, 10000);

    return () => clearInterval(interval);
  }, [show]);

  const reader = async (newsid: any, oldread: any) => {
    await axios.put("/api/news", {
      id: newsid,
      change: ["read", [...oldread, JSON.parse(localStorage.getItem("user") || "{}").id]],
    });
    getNews();
  };

  return (
    <div>
      <Menu placement="bottom-end">
        <MenuHandler>
          <div className="relative">
            {too > 0 && (
              <div className="size-6 bg-red1 -top-2 -right-3 absolute rounded-full flex items-center justify-center text-white font-semibold text-xs z-10">
                {too}
              </div>
            )}
            <Button {...({} as any)} className="bg-white px-4">
              <FaMessage className="text-lg text-main" />
            </Button>
          </div>
        </MenuHandler>
        <MenuList {...({} as any)} className="w-[95%] sm:w-[400px] mx-auto left-0 right-0 mt-2 sm:mx-0 text-gray-900">
          {news.map((n: any, i: any) =>
            n?.read.includes(JSON.parse(localStorage.getItem("user") || "{}").id) ? (
              <MenuItem
                {...({} as any)}
                key={n.id}
                onClick={() => handleModal(n)}
                className="mb-2 last:mb-0 border border-gray-300 flex gap-2"
              >
                <p className="flex-1 text-main">{n.Title}</p>
                <p>{convertToMongolianTime(n.createdAt)}</p>
              </MenuItem>
            ) : (
              <MenuItem
                {...({} as any)}
                className="mb-2 last:mb-0 border border-main flex items-center gap-3"
                onClick={() => {
                  reader(n?.id, n?.read);
                  handleModal(n);
                }}
                key={n.id}
              >
                <p className="flex-1 font-semibold text-main">{n.Title}</p>
                <div className="flex items-center gap-3">
                  <p>{convertToMongolianTime(n.createdAt)}</p>
                  <div className="size-[6px] rounded-full bg-main"></div>
                </div>
              </MenuItem>
            )
          )}

          {news.length > 0 ? (
            <div className="flex justify-end">
              {" "}
              <p className=" text-gray-700 cursor-pointer hover:outline-none hover:underline">Бүгдийг харах</p>
            </div>
          ) : (
            <div className="flex justify-center">
              {" "}
              <p className=" text-gray-700 cursor-pointer hover:outline-none hover:underline">Хоосон байна</p>
            </div>
          )}
        </MenuList>
      </Menu>
      {show && <NotifModal selected={selected} show={show} setShow={setShow} />}
    </div>
  );
};

export default Notif;
