"use client";
import Image from "next/image";

import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import { motion } from "framer-motion";
import { IconWorldWww } from "@tabler/icons-react";
import { IconBrandFacebook } from "@tabler/icons-react";
import { IconMail } from "@tabler/icons-react";
import { Flip, toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { cn } from "@/src/lib/utils";
import { HeroParallax } from "@/src/components/ui/hero-parallax";
import { HeroHighlight, Highlight } from "@/src/components/ui/hero-highlight";
import { CardBody, CardContainer, CardItem } from "@/src/components/ui/3d-card";
import { WobbleCard } from "@/src/components/ui/wobble-card";
import { Input } from "@/src/components/ui/input";
import { Label } from "@/src/components/ui/label";

import { data } from "@/data";

export default function Home() {
  const [customers, setCustomers] = useState([]);

  const lastNameRef = useRef<HTMLInputElement>(null);
  const firstNameRef = useRef<HTMLInputElement>(null);
  const phoneNumberRef = useRef<HTMLInputElement>(null);
  const emailRef = useRef<HTMLInputElement>(null);
  const detailRef = useRef<HTMLInputElement>(null);

  const customerhandler = async () => {
    const result: any = await axios.get("/api/customer", {
      headers: {
        "x-api-key": "Lol999za",
      },
      params: {
        app: true,
      },
    });
    setCustomers(result?.data);
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    const res = await axios.post("/api/admin/contact", {
      lastName: lastNameRef?.current?.value,
      firstName: firstNameRef?.current?.value,
      phoneNumber: phoneNumberRef?.current?.value,
      email: emailRef?.current?.value,
      detail: detailRef?.current?.value,
    });

    if (res.status === 200) {
      toast.info("Амжилттай бүртгэгдлээ!", {
        transition: Flip,
      });
    }

    lastNameRef && lastNameRef.current && (lastNameRef.current.value = "");
    firstNameRef && firstNameRef.current && (firstNameRef.current.value = "");
    phoneNumberRef && phoneNumberRef.current && (phoneNumberRef.current.value = "");
    emailRef && emailRef.current && (emailRef.current.value = "");
    detailRef && detailRef.current && (detailRef.current.value = "");
  };

  useEffect(() => {
    customerhandler();
  }, []);

  return (
    <div
      style={{
        backgroundImage: `url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width='8' height='8' fill='none' stroke='rgb(0 0 0 / 0.1)'%3e%3cpath d='M0 .5H31.5V32'/%3e%3c/svg%3e")`,
      }}
    >
      <ToastContainer position="bottom-center" autoClose={1500} />

      <HeroParallax />

      <div className="container mx-auto px-4">
        <div className="flex flex-col lg:flex-row">
          <div className="lg:w-1/2">
            <Image
              src="/aboutus.jpg"
              height="600"
              width="600"
              className="w-full object-cover rounded-xl group-hover/card:shadow-xl p-20"
              alt="thumbnail"
            />
          </div>
          <div className="lg:w-1/2">
            <HeroHighlight className="" containerClassName="">
              <motion.h1
                initial={{
                  opacity: 0,
                  y: 20,
                }}
                animate={{
                  opacity: 1,
                  y: [20, -5, 0],
                }}
                transition={{
                  duration: 0.5,
                  ease: [0.4, 0.0, 0.2, 1],
                }}
                className="text-4xl px-4 pb-6 font-semibold text-neutral-700 max-w-4xl leading-loose mx-auto "
              >
                Бидний тухай
              </motion.h1>
              <motion.h1
                initial={{
                  opacity: 0,
                  y: 20,
                }}
                animate={{
                  opacity: 1,
                  y: [20, -5, 0],
                }}
                transition={{
                  duration: 0.5,
                  ease: [0.4, 0.0, 0.2, 1],
                }}
                className="text-lg px-4 font-bold text-neutral-700 max-w-4xl leading-loose mx-auto "
              >
                Манай баг мэдээлэл технологийн салбарт сүүлийн 5 жил ажилласан тушлагатай бөгөөд өнгөрсөн хугацаанд олон
                төрлийн томоохон системүүдийг зах зээлд амжилттай нэвтрүүлээд байгаа билээ. Бид өөрсдийн туршлага, орчин
                цагийн хэрэгцээнд нийцүүлэн технологид суурилсан{" "}
                <Highlight className="text-black dark:text-white">шуурхай хүргэлтийн цогц шийдэл</Highlight>, үйлчилгээг
                үе шаттайгаар танд хүргэж байгаадаа таатай байна.
              </motion.h1>

              <div className="flex gap-5 mt-3 ms-2">
                <a target="_blank" href="https://www.facebook.com/delivery.zippy">
                  <IconBrandFacebook className="h-[40px] w-[40px]" />
                </a>
                <a target="_blank" href="https://virasoft.mn/">
                  <IconWorldWww className="h-[40px] w-[40px]" />
                </a>
                <IconMail className="h-[40px] w-[40px]" title="yukionna697@gmail.com" />
              </div>
            </HeroHighlight>
          </div>
        </div>

        <p className="mt-10 text-xl md:text-2xl font-bold">Хамтрагч байгууллагууд</p>

        <div className="grid grid-cols-1 md:grid-cols-3 gap-5">
          {customers?.map((val: any, idx) => {
            return (
              <CardContainer containerClassName="" className="inter-var" key={idx}>
                <CardBody className="bg-gray-50 relative group/card  dark:hover:shadow-2xl dark:hover:shadow-emerald-500/[0.1] dark:bg-black dark:border-white/[0.2] border-black/[0.1] w-auto sm:w-[30rem] h-auto rounded-xl p-6 border  ">
                  <CardItem translateZ={50} className="text-xl font-bold text-neutral-600 dark:text-white">
                    {val?.Name}
                  </CardItem>
                  <CardItem translateZ={100} className="w-full mt-4">
                    <Image
                      src={val.Image}
                      height="1000"
                      width="1000"
                      className="h-60 lg:h-80 w-full object-cover rounded-xl group-hover/card:shadow-xl"
                      alt="thumbnail"
                    />
                  </CardItem>
                </CardBody>
              </CardContainer>
            );
          })}
        </div>

        <p className="my-10 text-xl md:text-2xl font-bold">Бид танд дараах зүйлсийг санал болгож байна</p>

        <div className="grid grid-cols-1 lg:grid-cols-3 gap-8 mx-auto w-full">
          {data.map((val, idx) => {
            return (
              <WobbleCard className="" containerClassName={`col-span-1 h-full ${val?.bg} min-h-[300px]`} key={idx}>
                <div className="max-w-xs">
                  <h2 className="text-left text-balance text-base md:text-xl lg:text-3xl font-semibold tracking-[-0.015em] text-white">
                    {val?.title}
                  </h2>
                  <p className="mt-4 text-left  text-base/6 text-neutral-200 text-white">{val?.desc}</p>
                </div>
                <Image
                  src={val?.icon}
                  width={70}
                  height={70}
                  alt="icon"
                  className="absolute right-3 grayscale filter top-3 object-contain w-auto h-auto"
                />
              </WobbleCard>
            );
          })}
        </div>

        <div className="mt-10">
          <div className="w-full mx-auto rounded-none md:rounded-2xl p-4 md:p-8 shadow-input bg-white">
            <h2 className="font-bold text-xl text-neutral-800 dark:text-neutral-200">Zippy-д тавтай морилно уу</h2>
            <p className="text-neutral-600 text-sm mt-2 dark:text-neutral-300">
              Танд асуух зүйл байна уу эсвэл манай баг болон надтай хамтран ажиллах сонирхолтой байна уу?
            </p>

            <form className="my-8" onSubmit={handleSubmit}>
              <div className="flex flex-col md:flex-row space-y-2 md:space-y-0 md:space-x-2 mb-4">
                <LabelInputContainer>
                  <Label htmlFor="lastName">Овог</Label>
                  <Input id="lastName" placeholder="Бат" type="text" ref={lastNameRef} />
                </LabelInputContainer>
                <LabelInputContainer>
                  <Label htmlFor="firstName">Нэр</Label>
                  <Input id="firstName" placeholder="Дорж" type="text" ref={firstNameRef} />
                </LabelInputContainer>
              </div>
              <LabelInputContainer className="mb-4">
                <Label htmlFor="phoneNumber">Утасны дугаар</Label>
                <Input id="phoneNumber" placeholder="99999999" type="text" ref={phoneNumberRef} />
              </LabelInputContainer>
              <LabelInputContainer className="mb-4">
                <Label htmlFor="email">Мэйл хаяг</Label>
                <Input id="email" placeholder="example@zippy.com" type="email" ref={emailRef} />
              </LabelInputContainer>
              <LabelInputContainer className="mb-8">
                <Label htmlFor="detail">Дэлгэрэнгүй мэдээлэл</Label>
                <Input id="detail" placeholder="Дэлгэрэнгүй мэдээлэл ..." type="text" ref={detailRef} />
              </LabelInputContainer>

              <button className="p-[3px] relative z-50 mt-3">
                <div className="absolute inset-0 bg-gradient-to-r from-indigo-500 to-purple-500 rounded-lg" />
                <div className="px-8 py-2  bg-black rounded-[6px]  relative group transition duration-200 text-white hover:bg-transparent">
                  Илгээх &rarr;
                </div>
              </button>
            </form>
          </div>
        </div>
      </div>

      <footer className="mt-10">
        <div className="border-b text-sm">
          <div className="container mx-auto px-4 grid grid-cols-1 lg:grid-cols-4 gap-4">
            <div className="py-10">
              <div className="flex items-center">
                <Image src={`logo.svg`} alt="logo" width={60} height={60} className="w-auto h-auto" />

                <div className=" font-bold text-gray flex">
                  ZIPPY<p className="text-[#51ba5b]">&nbsp;DELIVERY</p>
                </div>
              </div>
              <div className="uppercase">
                хүргэлтийн <span className="font-bold">тань</span> асуудлыг <span className="font-bold">бид</span>{" "}
                шийдье.
              </div>
            </div>
            <div className="py-10">
              <div className="text-lg font-bold mb-5 ">Цагийн хуваарь</div>
              <div className="flex gap-3">
                <p className="font-bold opacity-80 text-gray-800"> Ажлын өдөр:</p>
                <p>09:00-22:00</p>
              </div>
              <div className="flex gap-3">
                <p className="font-bold opacity-80 text-gray-800">Амралтын өдөр:</p>
                <p>10:30-23:30</p>
              </div>
            </div>
            <div className="py-10">
              <div className="text-lg font-bold mb-5 ">Байршил</div>
              <div className="font-semibold opacity-80 text-gray-800">
                Чд, 8-р хороо, Компьютер молл цогцолбор, 13д-1316
              </div>
            </div>
            <div className="py-10">
              <div className="text-lg font-bold mb-5 ">Холбоо барих</div>
              <div className="flex gap-3">
                <p className="font-bold opacity-80 text-gray-800"> Утас:</p>
                <p>+976 7600-2001, +976 9915-0719</p>
              </div>
              <div className="flex gap-3">
                <p className="font-bold opacity-80 text-gray-800">Майл хаяг:</p>
                <p>info@zippy.mn</p>
              </div>
            </div>
          </div>
        </div>
        <div className="container mx-auto px-4 py-5 text-xs text-gray-500">
          <span id="nowYear">2024</span> VIRASOFT LLC
          <br />
          <span className="uppercase">хүргэлтийн тань асуудлыг бид шийдье.</span>
        </div>
      </footer>
    </div>
  );
}

const LabelInputContainer = ({ children, className }: any) => {
  return <div className={cn("flex flex-col space-y-2 w-full", className)}>{children}</div>;
};
