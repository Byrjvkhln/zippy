"use client";

import React, { useState, useEffect } from "react";
import axios from "axios";
import Link from "next/link";
import Image from "next/image";
const SingleNews = ({ params }: any) => {
  const { id } = params;
  const [news, setNews] = useState<any>([]);
  const getNews = async () => {
    const news: any = await axios.get("/api/news", {
      headers: {
        "x-api-key": "Lol999za",
      },
    });

    setNews(news?.data.find((d: any) => d.id === id));
  };
  useEffect(() => {
    getNews();
  }, []);

  return (
    <div className="px-5">
      <div className="font-medium mb-10">
        <h2 className="font-bold text-2xl mb-2">Мэдээний дэлгэрэнгүй</h2>
        <div className="flex gap-1 overflow-auto scrollbar-hide">
          {" "}
          <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
            Эхлэл
          </Link>{" "}
          <ul className="inline-flex gap-6 text-gray-500 list-disc ">
            <li className="shrink-0">
              <Link href="/dashboard/news">Мэдээний жагсаалт</Link>
            </li>
            <li className="shrink-0">{news.Title}</li>
          </ul>
        </div>
      </div>

      <div>
        <Image src={news.Image} alt="" width={500} height={500} className="w-96 h-full" />
      </div>
    </div>
  );
};

export default SingleNews;
