"use client";

import React, { useEffect, useState, Suspense } from "react";
import { RiLoader3Line } from "react-icons/ri";
import axios from "axios";
import Link from "next/link";
import Pagination from "@/src/components/Pagination";
import { FiEye, FiEdit2 } from "react-icons/fi";
import { RiDeleteBin6Line } from "react-icons/ri";
import { Card, CardBody, CardFooter, Avatar, Button } from "@material-tailwind/react";
import NoData from "@/src/components/NoData";

const TABLE_HEAD = ["№", "Зураг", "Мэдээ", "Нийтлэгдсэн огноо", "Зориулалт", ""];

const NewsPage = () => {
  const [news, setNews] = useState([]);
  const getNews = async () => {
    const news: any = await axios.get("  /api/news", {
      headers: {
        "x-api-key": "Lol999za",
      },
    });

    setNews(news?.data.reverse());
  };
  useEffect(() => {
    getNews();
  }, []);
  return (
    <div className="text-[14px] text-gray-900 px-5 h-full">
      <div className="p-5 shadow-md rounded-xl h-full bg-white">
        <div className="m-5">
          <h1 className="font-semibold text-2xl">Мэдээний жагсаалт</h1>
          <div className="flex items-center  gap-2 mb-3 mt-1">
            <p className="text-black">Эхлэл</p>
            <span className="h-1 w-1 rounded-full bg-gray-300"></span>
            <p className="text-grey-300">Мэдээ</p>
          </div>
        </div>

        <div className="flex items-center justify-between mb-5">
          <div>
            <Button className="bg-[#183770] text-white font-normal text-base normal-case" size="sm" {...({} as any)}>
              Мэдээ нэмэх
            </Button>
          </div>
          <div>
            <RiLoader3Line className="text-xl text-green-800 cursor-pointer" />
          </div>
        </div>

        {news.length > 0 ? (
          <Card className="h-fit w-full rounded-none shadow-none overflow-auto" {...({} as any)}>
            <CardBody className="!p-0 " {...({} as any)}>
              <table className=" w-full min-w-max table-auto text-left">
                <thead>
                  <tr className="">
                    {TABLE_HEAD.map((head) => (
                      <th key={head} className=" bg-blue-gray-200/50 px-4 py-2">
                        <p className="text-center">{head}</p>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {news?.map((el: any, index) => {
                    const isLast = index === el.length - 1;
                    const classes = isLast
                      ? "px-4 py-3 text-center"
                      : "px-4 py-3 border-b border-blue-gray-100 text-center";

                    return (
                      <tr key={el?.id}>
                        <td className={classes}> {index + 1}</td>
                        <td className={classes}>
                          {" "}
                          <div>
                            <Avatar src={el.Image} size="sm" {...({} as any)} />
                          </div>
                        </td>
                        <td className={classes}>
                          <p className=" text-left line-clamp-1 max-w-[400px]">{el.Title}</p>
                        </td>
                        <td className={classes}>{el.createdAt.split("T")[0]}</td>
                        <td className={classes}>
                          <div>
                            {el.For.map((a: any, i: any) => (
                              <p key={i}>{a}</p>
                            ))}
                          </div>
                        </td>

                        <td className={classes}>
                          <div className="flex text-lg items-center gap-3 ">
                            <Link
                              href={`/dashboard/news/${el.id}`}
                              className="size-6 flex items-center justify-center border border-gray-400 rounded hover:border-black hover:text-black duration-300 transition-colors"
                            >
                              <FiEye className="text-sm" />
                            </Link>
                            <div className="size-6 flex items-center justify-center border border-gray-400 rounded">
                              <FiEdit2 className="text-sm" />
                            </div>
                            <div className="size-6 flex items-center justify-center border border-gray-400 rounded">
                              <RiDeleteBin6Line className="text-sm" />
                            </div>
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </CardBody>
            <CardFooter className="flex items-center  border-t border-gray-400  p-4 justify-end" {...({} as any)}>
              <Suspense fallback={<div>Loading...</div>}></Suspense>
            </CardFooter>
          </Card>
        ) : (
          <NoData />
        )}
      </div>
    </div>
  );
};

export default NewsPage;
