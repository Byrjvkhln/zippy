"use client";

import React, { useState, useEffect } from "react";
import { Orders } from "../../../components/delivery/Orders";
// import CustomerDelivery from "../../../components/customerDelivery/CustomerDelivery";
import Deliveries from "@/src/components/customerDelivery/Deliveries";
// import axios from "axios";
import Link from "next/link";
// import Dropdown from "@/src/components/Dropdown";
// const Statuses: any = {
//   Хүргэгдсэн: "done",
//   Баталгаажсан: "comfirmed",
//   "Хүлээгдэж буй": "waiting",
//   Цуцлагдсан: "cancel",
//   Хойшилсон: "dela
// };
// function getKeyByValue(value: any) {
//   return Object.keys(Statuses).find((key) => Statuses[key] === value);
// }
const DeliveryPage = () => {
  // const [selectedCategory, setSelectedCategory] = useState("");
  const [user, setUser] = useState<any>("");
  // const [loading, setLoading] = useState(true);
  // const [customers, setCustomers] = useState([]);
  // const [customerId, setCustomerId] = useState<any>("Not selected");
  // const [orders, setOrders] = useState([]);
  // const [showorders, setShoworders] = useState<any>([]);
  // const [selected, setSelected] = useState<any>([]);
  // const [many, setMany] = useState(false);
  // const [open, setOpen] = useState(false);
  // const [selectedOrder, setSelectedOrder] = useState([]);
  useEffect(() => {
    if (typeof window !== "undefined") {
      const storedUser = window.localStorage.getItem("user");
      if (storedUser) {
        setUser(JSON.parse(storedUser));
      }
    }
  }, []);
  // useEffect(() => {
  //   const fetchCustomer = async () => {
  //     const res = await axios.get("/api/customer/delivery", {
  //       headers: {
  //         "x-api-key": "Lol999za",
  //       },
  //     });
  //     // setCustomers(res.data);
  //     // setLoading(false);
  //     setReFetch(false);
  //   };
  //   fetchCustomer();
  // }, [refetch]);
  // const fetchOrders = async ({ customerId, currentPage, orderPerPage }: any) => {
  //   const res = await axios.post("/api/delivery/customerid", {
  //     customerId: customerId,
  //     page: currentPage,
  //     pageSize: orderPerPage,
  //   });
  //   setOrders(res?.data.reverse());
  //   setShoworders(res?.data.reverse());
  //   setReFetch(false);
  // };

  // useEffect(() => {
  //   if (selectedCategory != "") {
  //     if (selectedCategory == "Бүгд") {
  //       // setSelected([]);
  //       // setShoworders(orders);
  //     } else {
  //       //@ts-ignore
  //       let tur = [...new Set([...selected, Statuses[selectedCategory]])];
  //       setSelected(tur);
  //     }
  //   }
  // }, [selectedCategory]);
  // useEffect(() => {
  //   if (selected == "") {
  //     setSelectedCategory("Бүгд");
  //   } else {
  //     setShoworders(orders.filter((el: any) => selected?.includes(el?.Status)));
  //   }
  // }, [selected]);

  return (
    <>
      <div className="text-[14px] text-gray-900 px-5 h-full flex flex-col">
        <div className="font-medium mb-10">
          <h2 className="font-bold text-2xl mb-2">Хүргэлт</h2>
          <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
            Эхлэл
          </Link>{" "}
          <ul className="inline-block text-gray-500 list-disc">
            <li>Хүргэлт</li>
          </ul>
        </div>
        {user.Status === "admin" ? (
          <>
            {/* {customers.length > 0 ? (
              <div>
                <div
                  className={`grid sm:grid-cols-2  bg-white ${
                    customers.length > 12 && "h-56 overflow-auto"
                  } overflow-auto p-5 gap-5 rounded-lg shadow-md lg:grid-cols-3 2xl:grid-cols-4 mb-5 `}
                >
                  {customers?.map((c: any) => (
                    <div
                      onClick={() => {
                        setCustomerId(c?.id);
                      }}
                      className="cursor-pointer"
                      key={c.id}
                    >
                      <div className="flex items-center  gap-3 ">
                        <div className="relative">
                          <Avatar src={c.Image} alt="" size="md" {...({} as any)} />
                          <div
                            className={`absolute size-7 -top-2 -right-2 flex items-center justify-center bg-red1 text-white rounded-full text-xs ${
                              c?.too === 0 ? "hidden" : "block"
                            }`}
                          >
                            {c?.too}
                          </div>
                        </div>

                        <div>
                          <p className=" text-center line-clamp-1">{c.Name}</p>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            ) : (
              ""
            )} */}
            <div>
              <Orders
              // user={user}
              // customerId={customerId}
              // orders={showorders}
              // setReFetch={setReFetch}
              // fetchOrders={fetchOrders}
              // refetch={refetch}
              // many={many}
              // open={open}
              // setOpen={setOpen}
              // selectedOrder={selectedOrder}
              // setSelectedOrder={setSelectedOrder}
              // setMany={setMany}
              />
            </div>
          </>
        ) : (
          <div>
            {/* <CustomerDelivery customerid={user?.id} /> */}
            <Deliveries customerid={user?.id && user?.id} />
          </div>
        )}
      </div>
    </>
  );
};

export default DeliveryPage;
