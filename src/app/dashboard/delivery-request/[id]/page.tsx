"use client";

import React, { useState, useEffect } from "react";
import axios from "axios";
import { FaLocationArrow, FaUser, FaPhoneAlt } from "react-icons/fa";
import Order from "../../../../components/delivery/Order";
import Loading from "../../../../components/wait/loading";
import { convertToMongolianTime } from "@/src/utils/timeConvertor";
import Link from "next/link";
import { MdCancel, MdOutlineCheck } from "react-icons/md";
import { useRouter } from "next/navigation";
import Loader from "@/src/components/wait/Loader";
const RejectedDelivery = ({ params }: any) => {
  const [user, setUser] = useState<any>(null);

  useEffect(() => {
    const storedUser = localStorage.getItem("user");

    if (storedUser) {
      try {
        setUser(JSON.parse(storedUser));
      } catch (e) {
        console.error("Failed to parse user from localStorage:", e);
      }
    }
  }, []);
  const { id } = params;
  const [delivery, setDelivery] = useState<any>();
  const [loading, setLoading] = useState<any>(false);
  const router = useRouter();
  useEffect(() => {
    const fetch = async () => {
      const res = await axios.get(`/api/cancelRequest/findone/${id}`, {
        headers: {
          "x-api-key": "Lol999za",
        },
      });

      setDelivery(res.data);
    };

    fetch();
  }, [id]);

  const changeReq = async (id: any, change: any) => {
    setLoading(true);

    await axios.put(" /api/cancelRequest", {
      id,
      change: [["ApprovedDate", `${convertToMongolianTime(new Date())}`], change],
    });

    if (change[1] == true) {
      await axios.put("/api/delivery", {
        id: delivery?.Delivery?.id,
        change: [["Status", delivery?.TypeOfReq]],
      });
    } else {
      await axios.put("/api/delivery", {
        id: delivery?.Delivery?.id,
        change: [["Status", "comfirmed"]],
      });
    }
    router.push("/dashboard/delivery-request");
  };

  const products = delivery?.Delivery.Product?.map((p: any) => JSON.parse(p));
  let addition = 0;

  const time = convertToMongolianTime(delivery?.createdAt);
  const deliveredTime = convertToMongolianTime(delivery?.Delivery.updatedAt);
  products?.map((p: any) => (addition += Number(p.Price) * p.too));

  return (
    <div className="px-5 h-full flex flex-col ">
      {delivery ? (
        <div className="">
          {" "}
          <div className="font-medium mb-10">
            <h2 className="font-bold text-2xl mb-2">{id}</h2>
            <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
              Эхлэл
            </Link>{" "}
            <ul className="inline-block text-gray-500 list-disc">
              <li>Хүргэлтийн дэлгэрэнгүй</li>
            </ul>
          </div>
          {loading == true ? (
            <Loader color="#183770" />
          ) : (
            <div className="flex flex-col lg:flex-row gap-5 bg-white 3xl:h-full p-5 rounded-lg shadow-md">
              <div className="flex-1 lg:flex-[2] xl:flex-[1.5]">
                <p className="text-xl font-semibold text-gray-700 mb-5">Хүргэлт</p>

                <div>
                  <p className="mb-3 font-semibold text-gray-700">Тайлбар</p>
                  <p>{delivery.Reason}</p>
                </div>
                <div className=" py-5">
                  <p className="mb-3 font-semibold text-gray-700">Хүлээн авагчийн мэдээлэл</p>
                  <div className="bg-gray-200 p-5 rounded-lg text-gray-900 mb-5">
                    <div className="flex items-center gap-3 mb-3">
                      <FaLocationArrow className="text-blue-900" />
                      <p>
                        {delivery.Delivery.Street}, {delivery.Delivery.Location}
                      </p>
                    </div>
                    <div className="flex items-center gap-3 mb-3">
                      <FaUser className="text-blue-900" />
                      <p>{delivery.Delivery.Name}</p>
                    </div>
                    <div className="flex items-center gap-3 mb-3">
                      <FaPhoneAlt className="text-blue-900" />
                      <p>{delivery.Delivery.PhoneNumber}</p>
                    </div>

                    <p> </p>
                  </div>

                  <p className="font-semibold text-gray-700 mb-3">
                    Жолооч:{" "}
                    {delivery.Delivery.Status != "waiting" &&
                      delivery.Driver?.Name &&
                      delivery.Driver?.Surname[0] + "." + delivery.Driver?.Name}
                  </p>

                  <i className="mb-3 block">
                    {delivery.Delivery.Status != "waiting" && delivery.Delivery.Driver?.PhoneNumber}
                  </i>
                  <div className="flex items-center justify-between mb-8">
                    {delivery.Delivery.Status && (
                      <div
                        className={` text-white font-medium w-fit px-4 py-2 rounded-3xl ${
                          delivery.Delivery.Status === "waiting"
                            ? "bg-red1"
                            : delivery.Delivery.Status === "comfirmed"
                            ? "bg-main"
                            : delivery.Delivery.Status === "done"
                            ? "bg-green1"
                            : "bg-yellow-800"
                        }`}
                      >
                        {delivery.Delivery.Status === "waiting"
                          ? "Баталгаажаагүй"
                          : delivery.Delivery.Status === "comfirmed"
                          ? "Баталгаажсан"
                          : delivery.Delivery.Status === "upcoming"
                          ? "Хүргэлтэнд гарсан"
                          : "Хүргэгдсэн"}
                      </div>
                    )}

                    <p className="text-base text-blue-900 font-medium flex items-center">
                      {Number(delivery?.Delivery?.Price)?.toLocaleString("en-US", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                      {"("}
                      <p className="text-xs  text-[#32CD32]">
                        {Number(delivery?.Delivery?.PriceDetail?.tulsun)?.toLocaleString("en-US", {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })}
                      </p>
                      +
                      <p className="text-xs  text-[#FFD700]">
                        {Number(delivery?.Delivery?.PriceDetail?.belen)?.toLocaleString("en-US", {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })}
                      </p>
                      {")"}₮
                    </p>
                  </div>

                  <p>Хүргэлт үүссэн ОГНОО: {delivery.Delivery.Status === "waiting" ? "" : time}</p>
                  <p className="mb-5">Хүргэгдсэн: {delivery.Delivery.Status === "done" ? deliveredTime : "~"}</p>

                  <p className="mb-10">{time}</p>
                  <div className="flex gap-5 items-center">
                    <p className="mb-10">Хойшилсон ОГНОО:</p>
                    <p className="mb-10">{delivery?.NextDate != null ? delivery?.NextDate : "~"}</p>
                  </div>
                  <div className="flex items-center gap-3">
                    <Link
                      href="/dashboard/delivery-request"
                      className="bg-main text-white text-base font-medium px-4 py-2 rounded-lg hover:bg-blue-900 transition-colors duration-300"
                    >
                      Жагсаалт руу буцах
                    </Link>
                    <div
                      style={{
                        display: user?.Status == "admin" ? "flex" : "none",
                      }}
                      className={`flex gap-3 `}
                    >
                      <div
                        className="size-10 flex items-center justify-center border border-green1 rounded text-green1 cursor-pointer"
                        onClick={() => changeReq(delivery.id, ["Status", true])}
                      >
                        <MdOutlineCheck className="text-2xl" />
                      </div>
                      <div
                        className="size-10 flex items-center justify-center border border-red1 text-red1 rounded cursor-pointer"
                        onClick={() => changeReq(delivery.id, ["Status", false])}
                      >
                        <MdCancel className="text-2xl" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="flex-[2.5]">
                <p className="text-xl font-semibold text-gray-700 mb-5">
                  {" "}
                  Нийт бараа: {delivery.Delivery.Product?.length}
                </p>

                <div className="border-t @container py-5">
                  <div className="grid @2xl:grid-cols-3 @sm:grid-cols-2 gap-5">
                    {products?.map((p: any) => (
                      <Order {...p} key={p.id} />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      ) : (
        <Loading />
      )}
    </div>
  );
};

export default RejectedDelivery;
