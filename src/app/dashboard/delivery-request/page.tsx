"use client";

import React, { useState, useEffect } from "react";
import { LuFilterX } from "react-icons/lu";
import { RiLoader3Line } from "react-icons/ri";

import axios from "axios";
import AllRequests from "@/src/components/delivery/CancelledDeliveries";
import Dropdown from "@/src/components/Dropdown";
const Statuses: any = {
  "Хариу ирээгүй": "null",
  Зөвшөөрсөн: "true",
  Татгалзсан: "false",
};
function getKeyByValue(value: any) {
  return Object.keys(Statuses).find((key) => Statuses[key] === value);
}
const DeliveryRejected = () => {
  const [selectedCategory, setSelectedCategory] = useState("");
  const [refetch, setReFetch] = useState(false);
  const [allReqData, setallReqData] = useState<any>();
  const [showReqData, setshowReqData] = useState<any>();
  const [currentCustomers, setCurrentCustomers] = useState<any>();
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState();
  const [currentPage, setCurrentPage] = useState(1);
  const [selected, setSelected] = useState<any>([]);
  const customerPerPage = 100;

  const indexOfLastItem = currentPage * customerPerPage;
  const indexOfFirstItem = indexOfLastItem - customerPerPage;

  useEffect(() => {
    setCurrentCustomers(showReqData?.slice(indexOfFirstItem, indexOfLastItem));
  }, [showReqData]);

  const fetch = async () => {
    const res = await axios.get("/api/cancelRequest", {
      headers: {
        "x-api-key": "Lol999za",
      },
      params: {
        page: currentPage,
        pageSize: customerPerPage,
      },
    });
    setallReqData(res.data.reverse());
    setshowReqData(res.data.reverse());
    setLoading(false);
    if (res.status === 200) setReFetch(false);
  };

  useEffect(() => {
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      setUser(JSON.parse(storedUser));
    }
  }, []);

  useEffect(() => {
    fetch();
  }, [refetch]);
  useEffect(() => {
    if (selectedCategory != "") {
      if (selectedCategory == "Бүгд") {
        setSelected([]);
        setshowReqData(allReqData);
      } else {
        //@ts-ignore
        let tur = [...new Set([...selected, `${Statuses[selectedCategory]}`])];
        setSelected(tur);
      }
    }
  }, [selectedCategory]);
  useEffect(() => {
    if (selected == "") {
      setSelectedCategory("Бүгд");
    } else {
      setshowReqData(allReqData.filter((el: any) => selected?.includes(`${el?.Status}`)));
    }
  }, [selected]);
  return (
    <div className="mx-5 bg-white shadow-md rounded-lg h-full">
      <div className="p-5 text-[14px] text-gray-900 h-full">
        <div className="font-medium mb-10">
          <h2 className="font-bold text-2xl mb-2">Цуцлах болон хойшлох хүсэлтүүд</h2>

          <p>Цуцлах болон хойшлох хүсэлтүүд & зарим бараа буцаагдсан болон тоо ширхэг өөрчлөгдсөн хүсэлтийн жагсаалт</p>
        </div>

        <div className="flex items-center justify-between mb-5 overflow-scroll xl:overflow-visible  scrollbar-hide gap-20 ">
          <div className="flex gap-5 ">
            <input
              onChange={(e) =>
                e.target.value == ""
                  ? setCurrentCustomers(allReqData)
                  : setCurrentCustomers(
                      showReqData.filter((el: any) => el?.Delivery?.PhoneNumber?.includes(e.target.value))
                    )
              }
              type="text"
              className="border border-gray-400 rounded-lg px-4 py-2"
              placeholder="Утасны дугаараар хайх"
            />

            <Dropdown selectedCategory={selectedCategory} setSelectedCategory={setSelectedCategory} type={"req"} />
            <div className="flex  items-center gap-1  w-[25vw] overflow-scroll scrollbar-hide">
              {selected.map((el: any, i: any) => {
                return (
                  <div key={i} className="bg-[#90EE90] rounded-lg px-2 py-1 text-[white] text-[12px] flex gap-2">
                    {getKeyByValue(`${el}`)}
                    <div
                      onClick={() => setSelected(selected.filter((el: any, index: any) => index != i))}
                      className="cursor-pointer"
                    >
                      X
                    </div>
                  </div>
                );
              })}
            </div>
            <div onClick={() => setSelectedCategory("Бүгд")} className="flex  items-center gap-2  h-9 cursor-pointer">
              <LuFilterX className="shrink-0" />
              <p className="shrink-0"> Шүүлтүүр цэвэрлэх</p>
            </div>
          </div>
          <div>
            <RiLoader3Line
              className={`text-xl text-green-800 cursor-pointer ${refetch && "animate-spin"}`}
              onClick={() => setReFetch(!refetch)}
            />
          </div>
        </div>

        <AllRequests
          allReqData={currentCustomers}
          loading={loading}
          refetch={refetch}
          setReFetch={setReFetch}
          user={user}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          num={customerPerPage}
        />
      </div>
    </div>
  );
};

export default DeliveryRejected;
