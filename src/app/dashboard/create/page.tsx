"use client";

import React, { useState, useRef, useEffect } from "react";
import Link from "next/link";
import { ImageUploader } from "../../../utils/imageuploader";
import axios from "axios";
import Image from "next/image";
import Adding from "../../../components/wait/adding";
import { convertToMongolianTime } from "@/src/utils/timeConvertor";
import Loader from "@/src/components/wait/Loader";

const AddProduct = () => {
  const file: any = useRef(null);
  const [product, setProduct] = useState<any>("");
  const [user, setUser] = useState<any>("");
  const [img, setImg] = useState<any>();
  const [image, setImage] = useState<any>();
  const [code, setCode] = useState<any>("");
  const [price, setPrice] = useState<any>("");
  const [quantity, setQuantity] = useState<any>("");
  const [hide, setHide] = useState<any>(false);
  const [width, setWidth] = useState(0);
  const [loading, setLoading] = useState(false);
  const [d, setD] = useState<any>();
  const [emptyProduct, setEmptyProduct] = useState(false);
  const [emptyPrice, setEmptyPrice] = useState(false);

  const loader = () => {
    setHide(true);
    const interval = setInterval(() => {
      setWidth((prevWidth) => prevWidth + 1);
    }, 15);

    setTimeout(() => {
      clearInterval(interval);
      setWidth(0);
      setHide(false);
    }, 1500);
    return () => {
      clearInterval(interval);
    };
  };

  useEffect(() => {
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      setUser(JSON.parse(storedUser));
    }
  }, []);

  useEffect(() => {
    if (img) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setImage(reader.result);
      };
      reader.readAsDataURL(img);
    }
  }, [img]);

  const handleClick = async () => {
    if (!product) {
      setEmptyProduct(true);
    }

    if (!price) {
      setEmptyPrice(true);
    }

    if (!product || !price) {
      return;
    }

    setLoading(true);
    try {
      const res = await axios.post("  /api/product", {
        Name: product,
        BarCode: code,
        Price: price,
        Amount: quantity,
        Img: ImageUploader(img),
        customerId: user.id,
        Change: {
          amount: [quantity],
          time: [convertToMongolianTime(new Date())],
        },
      });

      loader();
      setD("a");
      if (res.status === 200) {
        setImage("");
        setCode("");
        setPrice("");
        setProduct("");
        setQuantity("");
      }
    } catch (error) {
      loader();
    }
    setLoading(false);
  };

  return (
    <div className="p-5  h-full flex flex-col">
      <div className="font-medium mb-16">
        <h2 className="font-bold text-2xl mb-2">Бараа нэмэх</h2>
        <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
          Эхлэл
        </Link>{" "}
        <ul className="inline-block text-gray-500 list-disc">
          <li>Бараа нэмэх</li>
        </ul>
      </div>

      <div className="bg-white flex-1 p-5 rounded-lg shadow-md">
        <div className="flex flex-col gap-5 lg:flex-row lg:gap-20 border-b pb-10 border-dashed border-gray-400">
          <div>
            <p className="text-lg font-medium">Бараа</p>
            <p>Үүсгэх барааны мэдээллийг үнэн зөв оруулна уу!</p>
          </div>
          <div className="grid sm:grid-cols-2 flex-1 gap-5">
            <div>
              <label> Нэр</label>
              <input
                value={product}
                onChange={(e: any) => {
                  setProduct(e.target.value);
                  setEmptyProduct(e.target.value === "");
                }}
                type="text"
                placeholder="Барааны нэр"
                className={`block border px-4 py-2  rounded-lg w-full mt-3 outline-none ${
                  emptyProduct ? "border-red-500" : "border-gray-400"
                }`}
              />
              {emptyProduct && <p className="mt-2 text-red-500">Барааны нэрийг оруулна уу!</p>}
            </div>
            <div>
              <label> Баркод /Optional/</label>
              <input
                value={code}
                onChange={(e: any) => setCode(e.target.value)}
                type="text"
                placeholder="12345 00010"
                className="block border px-4 py-2 border-gray-400 rounded-lg w-full mt-3 outline-none"
              />
            </div>
            <div>
              <label> Үнийн дүн</label>
              <input
                value={price}
                onChange={(e: any) => {
                  setPrice(e.target.value);
                  setEmptyPrice(e.target.value === "");
                }}
                type="text"
                className={`block border px-4 py-2 rounded-lg w-full mt-3 outline-none ${
                  emptyPrice ? "border-red-500" : "border-gray-400"
                }`}
              />
              {emptyPrice && <p className="mt-2 text-red-500">Үнийн дүн оруулна уу!</p>}
            </div>
            <div>
              <label>Тоо ширхэг</label>
              <input
                value={quantity}
                onChange={(e: any) => setQuantity(e.target.value)}
                type="text"
                className="block border px-4 py-2 border-gray-400 rounded-lg w-full mt-3 outline-none"
              />
            </div>
          </div>
        </div>

        <div className="mb-5 py-10 flex flex-col sm:flex-row sm:gap-12 gap-5 md:gap-40 border-b  border-dashed border-gray-400">
          <div>
            <p className="text-lg font-medium">Зураг</p>
            <p>Бараанд ашиглагдах зураг хуулна уу!</p>
          </div>

          <div>
            <label htmlFor="uploadImg">
              <div className="size-40 border rounded-full border-gray-400 flex items-center justify-center cursor-pointer overflow-hidden">
                {image ? (
                  <Image src={image} alt="" width={100} height={120} className="w-full h-full object-cover" />
                ) : (
                  "Зураг хуулах"
                )}
              </div>
            </label>
            <input
              ref={file}
              type="file"
              id="uploadImg"
              className="hidden"
              onChange={(e: any) => setImg(e.target.files[0])}
            />
          </div>
        </div>

        <div className="flex justify-center" style={{ display: hide ? "flex" : "none" }}>
          <Adding
            width={width}
            data={
              d
                ? {
                    color: "rgba(0,186,0,255)",
                    text: "Бараа амжилттай нэмэгдлээ",
                    image: "/check.png",
                  }
                : {
                    color: "red",
                    text: "Бараа нэмэгдсэнгүй",
                    image: "/multiply.png",
                  }
            }
          />
        </div>

        <button
          onClick={handleClick}
          className="block ml-auto bg-main text-white font-medium px-4 py-2 text-base rounded hover:bg-blue-900 transition-colors duration-300"
        >
          {loading ? <Loader color="white" /> : " Бараа нэмэх"}
        </button>
      </div>
    </div>
  );
};

export default AddProduct;
