"use client";

import React, { useState, useEffect } from "react";
import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import { FaPlus } from "react-icons/fa6";
import Accordion, { AccordionItem } from "@/src/components/Accordion";
import Loading from "./../../../components/wait/loading";
import EditProduct from "../../../components/customerDash/EditProduct";
import { convertToMongolianTime } from "@/src/utils/timeConvertor";
import Loader from "@/src/components/wait/Loader";

const Product = ({ params }: any) => {
  const [product, setproduct] = useState<any>("");
  const [balance, setBalance] = useState<number>(0);
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const { id } = params;
  const fetchProduct = async () => {
    const res = await axios.get(`  /api/product/${id}`, {
      headers: {
        "x-api-key": "Lol999za",
      },
    });
    setproduct(res.data);
    return res.data;
  };
  useEffect(() => {
    fetchProduct();
  }, [open, balance]);

  const amountChanger = async (change: any) => {
    setLoading(true);
    const pro = await fetchProduct();
    await axios.put("  /api/product", {
      id: pro.id,
      change,
    });
    const res = await axios.put("  /api/product", {
      id: pro.id,
      change: [
        "Change",
        {
          amount: [...pro?.Change.amount, ...[balance.toString()]],
          time: [...pro?.Change.time, convertToMongolianTime(new Date())],
        },
      ],
    });

    if (res.status === 200) {
      setBalance(0);
      setLoading(false);
    }
  };
  return (
    <>
      {product ? (
        <div className="px-5">
          <div className="font-medium mb-10">
            <h2 className="font-bold text-2xl mb-2">Барааны дэлгэрэнгүй</h2>
            <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
              Эхлэл
            </Link>
            <ul className="inline-block text-gray-500 list-disc">
              <li>Барааны дэлгэрэнгүй</li>
            </ul>
          </div>

          <div className="flex flex-col md:flex-row gap-5">
            <div className="flex-1 rounded overflow-hidden">
              <Image
                src={product?.Img}
                alt=""
                width={500}
                height={500}
                className="w-full h-[450px] object-cover"
                priority
              />
            </div>
            <div className="flex-[2] md:flex-1 xl:flex-[2]">
              <h4 className="text-3xl font-semibold mb-3">{product?.Name}</h4>
              <p className="text-lg mb-5">Үлдэгдэл: {product?.Amount} ш</p>

              <div className="border-y border-gray-400 py-6">
                <p className="text-2xl font-semibold">
                  {Number(product.Price).toLocaleString("en-US", {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })}
                  ₮
                </p>
                <p className="text-green-600 font-medium mb-5">Борлуулалт: 0.00₮</p>

                <div className="underline cursor-pointer" onClick={() => setOpen(true)}>
                  Бараа засах
                </div>
                {open && <EditProduct open={open} setOpen={setOpen} product={product} />}
              </div>

              <div className="flex flex-col sm:flex-row md:flex-col lg:flex-row gap-4 py-8">
                <input
                  type="text"
                  className="border  border-gray-400 py-2 px-3 rounded-lg outline-none"
                  value={balance}
                  onChange={(e: any) => setBalance(Number(e.target.value))}
                />
                <button
                  className=" rounded-lg w-[220px]  hover:bg-blue-900 transition-colors duration-300 py-2 bg-main text-white font-medium text-base"
                  onClick={() => amountChanger(["Amount", (Number(product.Amount) + balance).toString()])}
                >
                  {loading ? (
                    <Loader color="white" />
                  ) : (
                    <div className="flex justify-center items-center gap-3">
                      <FaPlus /> Үлдэгдэл нэмэх{" "}
                    </div>
                  )}
                </button>
              </div>

              <Accordion value="">
                <AccordionItem value="1" trigger="Тоо ширхэг нэмсэн түүх">
                  <div className=" flex gap-32 h-56 overflow-auto rounded">
                    <div>
                      {product?.Change?.time?.reverse().map((t: any, i: any) => (
                        <p className="mb-1" key={i}>
                          {t}
                        </p>
                      ))}
                    </div>
                    <div>
                      {product?.Change?.amount?.reverse().map((a: any, i: any) => (
                        <p className="mb-1" key={i}>
                          {a}
                        </p>
                      ))}
                    </div>
                  </div>
                </AccordionItem>
              </Accordion>
            </div>
          </div>
        </div>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default Product;
