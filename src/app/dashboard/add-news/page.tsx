"use client";

import axios from "axios";
import React, { useState, useEffect, useRef } from "react";
import Image from "next/image";
import { Checkbox, Button } from "@material-tailwind/react";
import { ImageUploader } from "@/src/utils/imageuploader";
import Loader from "@/src/components/wait/Loader";

const AddNewsPage = () => {
  const file: any = useRef(null);
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [img, setImg] = useState<any>("");
  const [image, setImage] = useState<any>("");
  const [value, setValue] = useState<string[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (img) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setImage(reader.result);
      };
      reader.readAsDataURL(img);
    }
  }, [img]);

  const handleCheckboxChange = (e: any) => {
    const { name, checked } = e.target;
    if (checked) {
      setValue((prev) => [...prev, name]);
    } else {
      setValue((prev) => prev.filter((item) => item !== name));
    }
  };

  const send = async () => {
    setLoading(true);
    const res = await axios.post("/api/news", {
      Body: "",
      Title: title,
      Discription: desc,
      Image: ImageUploader(img),
      For: value,
    });

    if (res.status === 200) {
      setLoading(false);
      setImage("");
      setTitle("");
      setDesc("");
      setValue([]);
    }
  };

  return (
    <div className="text-gray-900 text-[14px] px-5 h-full flex flex-col">
      <h2 className="font-bold text-2xl mb-5">Мэдээ нэмэх</h2>

      <div className="bg-white flex-1 rounded-lg p-5 shadow-md">
        <div className="flex flex-col max-w-[500px]">
          <div>
            <p className="mb-3">Мэдээний гарчиг:</p>
            <input
              type="text"
              value={title}
              className="outline-none w-full border border-gray-400 px-2 py-1 rounded mb-5"
              placeholder="Гарчиг..."
              onChange={(e: any) => setTitle(e.target.value)}
            />

            <p className="mb-3">Богино танилцуулга:</p>
            <textarea
              className="border w-full resize-none h-28 border-gray-400 rounded px-2 py-1 mb-5 outline-none"
              placeholder="Богино танилцуулга "
              value={desc}
              onChange={(e: any) => setDesc(e.target.value)}
            ></textarea>
            <div className="mb-5">
              <label htmlFor="uploadImg">
                <div className="border rounded border-gray-400 flex items-center justify-center cursor-pointer overflow-hidden mx-auto h-20">
                  Зураг хуулах
                </div>
              </label>
              <input
                ref={file}
                type="file"
                id="uploadImg"
                className="hidden"
                onChange={(e: any) => setImg(e.target.files[0])}
              />
            </div>

            <div>
              {image && (
                <Image
                  src={image}
                  alt=""
                  width={100}
                  height={120}
                  className="w-full h-[300px] object-cover object-center"
                />
              )}
            </div>

            <div className="mb-5">
              <div className="flex gap-10">
                <Checkbox
                  {...({} as any)}
                  name="Жолоочдод"
                  ripple={false}
                  className="border-main p-0 transition-all hover:before:opacity-0 checked:bg-main checked:border-main"
                  label={<p className="font-normal ">Жолоочдод</p>}
                  onChange={handleCheckboxChange}
                  checked={value.includes("Жолоочдод")}
                />
                <Checkbox
                  {...({} as any)}
                  name="Харилцагчдад"
                  ripple={false}
                  className="border-main p-0 transition-all hover:before:opacity-0 checked:bg-main checked:border-main"
                  label={<p className="font-normal ">Харилцагчдад</p>}
                  onChange={handleCheckboxChange}
                  checked={value.includes("Харилцагчдад")}
                />
              </div>
            </div>
          </div>
        </div>
        <Button
          {...({} as any)}
          onClick={send}
          className="mr-3 normal-case text-sm bg-main hover:bg-blue-900 transition-colors duration-300"
        >
          {loading ? <Loader color="white" /> : "Хадгалах"}
        </Button>
        <Button
          {...({} as any)}
          className="mr-3 normal-case text-sm bg-yellow1 hover:bg-yellow-700 transition-colors duration-300"
        >
          Хаах
        </Button>
      </div>
    </div>
  );
};

export default AddNewsPage;
