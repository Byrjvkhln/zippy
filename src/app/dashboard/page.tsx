"use client";

import CustomerDash from "../../components/customerDash/CustomerDash";
// import Card from "../../components/dashboard/Card";
import React, { useState, useEffect } from "react";
// import { FiBox } from "react-icons/fi";
// import { FaRegCheckCircle, FaRegTimesCircle } from "react-icons/fa";
// import { MdAccessTime } from "react-icons/md";
// import TopCustomers from "@/src/components/dashboard/TopCustomers";
import { useRouter } from "next/navigation";
// import axios from "axios";

const Dashboard = () => {
  const router = useRouter();
  const [user, setUser] = useState<any>("");
  // const [deliveries, setDeliveries] = useState<any>();
  // const [week, setWeek] = useState<any>();
  // const today = new Date();
  // const startOfLastMonth = new Date(today.getFullYear(), today.getMonth() - 1, 1);
  // const endOfLastMonth = new Date(today.getFullYear(), today.getMonth(), 0);
  // endOfLastMonth.setHours(23, 59, 59, 999);

  // const startOfLastWeek = new Date(today);
  // startOfLastWeek.setDate(today.getDate() - today.getDay() - 7);
  // startOfLastWeek.setHours(0, 0, 0, 0);

  // const endOfLastWeek = new Date(today);
  // endOfLastWeek.setDate(today.getDate() - today.getDay() - 1);
  // endOfLastWeek.setHours(23, 59, 59, 999);

  useEffect(() => {
    if (typeof window !== "undefined") {
      const storedUser = window.localStorage.getItem("user");
      if (storedUser) {
        if (JSON.parse(storedUser).Status == "admin") {
          router.push("/dashboard/delivery");
        }
        setUser(JSON.parse(storedUser));
      } else {
        router.push("/login");
      }
    }
  }, [router]);

  // useEffect(() => {
  //   const fetch = async () => {
  //     const res = await axios.get("/api/delivery", {
  //       headers: {
  //         "x-api-key": "Lol999za",
  //       },
  //     });
  //     const filteredDeliveries = res.data
  //       .filter((c: any) => {
  //         const deliveryDate = new Date(c.createdAt);
  //         return deliveryDate >= startOfLastMonth && deliveryDate <= endOfLastMonth;
  //       })
  //       .map((c: any) => ({
  //         ...c,
  //         Delivery: c,
  //       }))
  //       .sort((a: any, b: any) => b.Delivery.length - a.Delivery.length);

  //     setDeliveries(filteredDeliveries);

  //     const filteredWeekDeliveries = res.data.filter((c: any) => {
  //       const deliveryDate = new Date(c.createdAt);
  //       return deliveryDate >= startOfLastWeek && deliveryDate <= endOfLastWeek;
  //     });
  //     setWeek(filteredWeekDeliveries);
  //   };

  //   fetch();
  // }, []);

  return (
    <div className="text-[14px] text-gray-900 px-5">
      {user?.Status === "admin" ? (
        <div>Zippy Delivery</div>
      ) : (
        // <>
        //   <div className="grid sm:grid-cols-2 2xl:grid-cols-4 gap-5 mb-5">
        //     <Card
        //       text="Нийт хүргэлт"
        //       bg="bg-cyan-300"
        //       icon={<FiBox className="text-2xl text-white" />}
        //       month={deliveries?.length}
        //       week={week?.length}
        //     />
        //     <Card
        //       icon={<FaRegCheckCircle className="text-white text-2xl" />}
        //       bg="bg-pink-300"
        //       text="Амжилттай"
        //       month={deliveries?.filter((d: any) => d.Status === "done").length}
        //       week={week?.filter((d: any) => d.Status === "done").length}
        //     />
        //     <Card
        //       icon={<MdAccessTime className="text-white text-3xl" />}
        //       bg="bg-blue-700"
        //       text="Хүлээгдэж буй"
        //       month={deliveries?.filter((d: any) => d.Status === "upcoming").length}
        //       week={week?.filter((d: any) => d.Status === "upcoming").length}
        //     />
        //     <Card
        //       icon={<FaRegTimesCircle className="text-white text-2xl" />}
        //       bg="bg-red-500"
        //       text="Цуцлагдсан"
        //       month={deliveries?.filter((d: any) => d.Status === "cancel").length}
        //       week={week?.filter((d: any) => d.Status === "cancel").length}
        //     />
        //   </div>
        //   <TopCustomers />
        // </>
        <CustomerDash user={user} />
      )}
    </div>
  );
};

export default Dashboard;
