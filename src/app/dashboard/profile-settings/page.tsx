"use client";

import React, { useState, useEffect } from "react";
import Link from "next/link";
import CustomerPro from "../../../components/profile/customer/CustomerPro";

const Profile = () => {
  const [user, setuser] = useState<any>({});

  useEffect(() => {
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      setuser(JSON.parse(storedUser));
    }
  }, []);
  return (
    <div>
      <div className="font-medium mb-10 px-5">
        <h2 className="font-bold text-2xl mb-2">Хэрэглэгчийн бүртгэл</h2>
        <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
          Эхлэл
        </Link>{" "}
        <ul className="inline-block text-gray-500 list-disc">
          <li>Бүртгэл</li>
        </ul>
      </div>

      <div>
        <CustomerPro user={user} />
      </div>
    </div>
  );
};

export default Profile;
