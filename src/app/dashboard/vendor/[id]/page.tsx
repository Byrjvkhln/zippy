"use client";

import React, { useState, useEffect } from "react";
import axios from "axios";
import Stat from "../../../../components/delivery-worker/Stat";
import Link from "next/link";
import { Avatar } from "@material-tailwind/react";
import Loading from "../../../../components/wait/loading";
import { CiEdit } from "react-icons/ci";
import EditModal from "../../../../components/EditModal";

const SingleVendor = ({ params }: any) => {
  const { id } = params;
  const [customer, setCustomer] = useState<any>();
  const [openCustomerPass, setOpenCustomerPass] = useState(false);
  const [openCustomerPhone, setOpenCustomerPhone] = useState(false);
  const [openCustomerEmail, setOpenCustomerEmail] = useState(false);
  const [openCustomerCost, setOpenCustomerCost] = useState(false);

  useEffect(() => {
    const fetchCustomer = async () => {
      const res = await axios.get(`  /api/customer/${id}`, {
        headers: {
          "x-api-key": "Lol999za",
        },
      });
      setCustomer(res.data);
    };

    fetchCustomer();
  }, [id, openCustomerEmail, openCustomerPass, openCustomerPhone, openCustomerCost]);

  return (
    <>
      {customer ? (
        <div className="px-5  h-full">
          <div>
            <div className="font-medium mb-10">
              <div>
                {" "}
                <h2 className="font-bold text-2xl mb-2">{customer?.Name}</h2>
                <div className="flex  gap-2 overflow-auto scrollbar-hide">
                  {" "}
                  <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
                    Эхлэл
                  </Link>{" "}
                  <ul className=" text-gray-500 list-disc flex gap-7">
                    <li>
                      <Link href="/dashboard/delivery-worker">Харилцагч</Link>
                    </li>
                    <li className="shrink-0">Харилцагчийн дэлгэрэнгүй</li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="flex flex-col lg:flex-row gap-10 bg-white p-5 rounded-lg shadow-md">
              <div className="flex-[2] ">
                <div className="flex items-center gap-3 mb-5 ">
                  <Avatar src={customer?.Image} alt="" size="sm" {...({} as any)} />
                  <div className="flex flex-col">
                    <p className="font-semibold mb-1">{customer?.Name}</p>
                    <p className="text-gray-700">{customer?.Email}</p>
                  </div>
                </div>

                <div className="border-b pb-5 border-gray-400 mb-5">
                  <p className="text-lg font-semibold text-main mb-5">Дансны мэдээлэл</p>
                  <div className="">
                    <div className="flex  mb-2">
                      <p className="flex-1">Дансны дугаар</p>
                      <p className="flex-1 xl:flex-[2]">Холбоогүй</p>
                    </div>
                    <div className="flex">
                      <p className="flex-1">Данс эзэмшигчийн нэр</p>
                      <p className="flex-1 xl:flex-[2]">Холбоогүй</p>
                    </div>
                  </div>
                </div>

                <div className="text-gray-800">
                  <p className="text-lg font-semibold text-main mb-5">Бүртгэлийн мэдээлэл</p>

                  <div
                    className="flex items-center gap-2 mb-5 cursor-pointer"
                    onClick={() => setOpenCustomerPass(!openCustomerPass)}
                  >
                    <CiEdit className="text-2xl" />
                    <p>Нууц үг солих</p>
                  </div>
                  <div
                    className="flex items-center gap-2 mb-5 cursor-pointer"
                    onClick={() => setOpenCustomerPhone(!openCustomerPhone)}
                  >
                    <CiEdit className="text-2xl" />
                    <p>{customer?.PhoneNumber}</p>
                  </div>
                  <div
                    className="flex items-center gap-2 mb-5 cursor-pointer"
                    onClick={() => setOpenCustomerCost(!openCustomerCost)}
                  >
                    <CiEdit className="text-2xl" />
                    <p>{customer?.TransportCost}</p>
                  </div>
                  <div
                    className="flex items-center gap-2 mb-10 cursor-pointer"
                    onClick={() => setOpenCustomerEmail(!openCustomerEmail)}
                  >
                    <CiEdit className="text-2xl" />
                    <p>{customer?.Email}</p>
                  </div>
                  <div className="flex items-center  mb-10 justify-between">
                    <p>Системд бүртгүүлсэн</p>
                    <p>{customer?.createdAt.split("T")[0]}</p>
                  </div>
                </div>

                {(openCustomerCost || openCustomerPass || openCustomerPhone || openCustomerEmail) && (
                  <EditModal
                    openCustomerPass={openCustomerPass}
                    setOpenCustomerPass={setOpenCustomerPass}
                    openCustomerPhone={openCustomerPhone}
                    setOpenCustomerPhone={setOpenCustomerPhone}
                    customerPhone={customer?.PhoneNumber}
                    openCustomerEmail={openCustomerEmail}
                    setOpenCustomerEmail={setOpenCustomerEmail}
                    customerEmail={customer?.Email}
                    setOpenCustomerCost={setOpenCustomerCost}
                    openCustomerCost={openCustomerCost}
                    customerCost={customer?.TransportCost}
                    customerId={customer?.id}
                  />
                )}
              </div>
              <div className="flex-1">
                <div className="border-b border-gray-400 pb-3 mb-5">
                  <p className="text-lg font-semibold text-main mb-5">Нэхэмжлэх</p>
                  <Stat text="Хаагдсан" num={0} />
                  <Stat text="Идэвхтэй" num={0} />
                </div>
                <div className="border-b border-gray-400 pb-3">
                  <p className="text-lg font-semibold text-main mb-5">Татан авалт</p>
                  <Stat text="Гүйцэтгэсэн" num={0} />
                  <Stat text="Хүлээгдэж буй" num={0} />
                  <Stat text="Авахаар очиж байгаа" num={0} />
                  <Stat text="Аваад ирж байгаа" num={0} />
                </div>
                <div className="border-b border-gray-400 pb-3 mt-5">
                  <p className="text-lg font-semibold text-main mb-5">Хүргэлт</p>
                  <Stat text="Хүргэгдсэн" num={0} />
                  <Stat text="Хүлээгдэж буй" num={0} />
                  <Stat text="Хүргэлтэнд гарсан" num={0} />
                  <Stat text="Буцаагдсан" num={0} />
                  <Stat text="Цуцлагдсан" num={0} />
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <Loading />
      )}{" "}
    </>
  );
};

export default SingleVendor;
