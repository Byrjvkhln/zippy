"use client";

import React, { useState } from "react";
import { FaPlus } from "react-icons/fa6";
import { LuFilterX } from "react-icons/lu";
import { RiLoader3Line } from "react-icons/ri";
import AddModal from "../../../components/vendor/AddModal";
import Customers from "../../../components/vendor/Customers";
import axios from "axios";
const Vendor = () => {
  const [show, setShow] = useState(false);
  const [customers, setCustomers] = useState([]);
  const [showcustomers, setShowcustomers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refetch, setReFetch] = useState(false);

  const fetchCustomers = async ({ currentPage, customerPerPage }: any) => {
    const res = await axios.get("/api/customer", {
      headers: {
        "x-api-key": "Lol999za",
      },
      params: {
        page: currentPage,
        pageSize: customerPerPage,
      },
    });
    setCustomers(res.data.reverse());
    setShowcustomers(res.data.reverse());
    setLoading(false);
    setReFetch(false);
  };
  const handleClick = () => {
    setShow(!show);
  };
  return (
    <div className="px-5  h-full">
      <div className="p-5 bg-white  text-gray-900 text-[14px] rounded-lg shadow-md h-full">
        <h2 className="text-lg font-bold">Харилцагч</h2>
        <p className="mb-5">Zippy хүргэлтийн системд бүртгэлтэй харилцагчдын жагсаалт</p>

        <div className="flex justify-end mb-10">
          <button
            onClick={handleClick}
            className="flex items-center gap-2 bg-main px-4 py-2 text-white font-semibold rounded-lg hover:bg-blue-900 transition-colors duration-300"
          >
            <FaPlus className="text-lg" /> Шинэ харилцагч
          </button>
          {show && <AddModal show={show} setShow={setShow} />}
        </div>

        <div className="flex items-center gap-5 mb-10 overflow-auto scrollbar-hide">
          <div className="flex items-center gap-5 ">
            <input
              type="text"
              className="border border-gray-400 rounded-lg px-4 py-2 outline-none"
              placeholder="Нэрээр хайх"
              onChange={(e) =>
                setShowcustomers(
                  customers.filter((el: any) => el?.Name?.toLowerCase().includes(e.target.value.toLowerCase()))
                )
              }
            />
          </div>

          <div>
            <RiLoader3Line
              className={`text-xl text-green-800 cursor-pointer ${refetch && "animate-spin"}`}
              onClick={() => setReFetch(!refetch)}
            />
          </div>
        </div>

        <Customers
          fetchCustomers={fetchCustomers}
          customers={showcustomers}
          loading={loading}
          show={show}
          refetch={refetch}
        />
      </div>
    </div>
  );
};

export default Vendor;
