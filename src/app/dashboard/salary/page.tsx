"use client";

import { useState, useEffect } from "react";
import Link from "next/link";
import { useMemo } from "react";
import Table from "@/src/components/Table";
import { convertToMongolianTime } from "@/src/utils/timeConvertor";
import axios from "axios";
const SalaryPage = () => {
  const [data, setData] = useState([]);
  const salaryhandler = async ({ status, id }: any) => {
    await axios.put("/api/salaryRequest", {
      id,
      change: ["Status", status],
    });
  };
  const salarycolumn = useMemo(
    () => [
      {
        accessorKey: "index", //access nested data with dot notation
        header: "№",
        size: 50,
        Cell: ({ row }: any) => row.index + 1,
        enableColumnFilter: false,
      },
      {
        accessorKey: "Driver.Name",
        header: "Жолооч",
      },
      {
        accessorKey: "Value",
        header: "Үнийн дүн",
      },

      {
        accessorKey: "Driver.PhoneNumber",
        header: "Утас",
      },
      {
        header: "ОГНОО",
        Cell: ({ row }: any) => {
          const createdAt = row.original?.createdAt;
          return <p>{convertToMongolianTime(createdAt)}</p>;
        },
      },

      {
        header: "Төлөв",
        Cell: ({ row }: any) => {
          const salary = row.original;
          return salary?.Status == null ? (
            <div className="flex gap-5">
              <button
                onClick={() => salaryhandler({ id: salary?.id, status: true })}
                // onClick={() => statusHandler(["Status", "Баталсан"])}
                className="flex  rounded-md items-center px-5 h-[30px]   border-[1px] gap-2 bg-[#51ba5b] hover:opacity-50"
              >
                <div className="font-semibold text-white">Батлах </div>
              </button>
              <button
                onClick={() => salaryhandler({ id: salary?.id, status: false })}
                // onClick={() => statusHandler(["Status", "Цуцлагдсан"])}
                className="flex  rounded-md items-center px-5 h-[30px]   border-[1px] gap-2 bg-[#E3242B] hover:opacity-50"
              >
                <div className="font-semibold text-white">Цуцлах </div>
              </button>
            </div>
          ) : salary?.Status === true ? (
            <div className="text-[#51ba5b]">Зөвшөөрөгдсөн</div>
          ) : (
            <div className="text-[#E3242B]">Цуцлагдсан</div>
          );
        },
      },
    ],
    []
  );
  return (
    <>
      <div className="text-[14px] text-gray-900 px-5 h-full flex flex-col">
        <div className="font-medium mb-10">
          <h2 className="font-bold text-2xl mb-2">Жолоочын цалин</h2>
          <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
            Эхлэл
          </Link>{" "}
          <ul className="inline-block text-gray-500 list-disc">
            <li>Жолоочын цалин</li>
          </ul>
        </div>

        <div>
          <>
            <Table
              tableurl={"/api/salaryRequest"}
              columns={salarycolumn}
              findid={null}
              setData={setData}
              data={data}
            ></Table>
          </>
        </div>
      </div>
    </>
  );
};

export default SalaryPage;
