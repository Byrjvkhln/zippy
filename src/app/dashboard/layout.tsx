"use client";

import Navbar from "../../components/dashboard/Navbar";
import { Drawer } from "@material-tailwind/react";
import Sidebar from "../../components/dashboard/Sidebar";
import React, { useState, useEffect, ReactNode } from "react";
interface DashboardLayoutProps {
  children: ReactNode;
}

const DashboardLayout: React.FC<DashboardLayoutProps> = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [user, setUser] = useState<any>(null);

  useEffect(() => {
    const storedUser = localStorage.getItem("user");

    if (storedUser) {
      try {
        setUser(JSON.parse(storedUser));
      } catch (e) {
        console.error("Failed to parse user from localStorage:", e);
      }
    }
  }, []);

  return (
    <div className="flex h-screen overflow-hidden">
      <div className={`w-[280px] shrink-0 px-5 hidden 2xl:block py-5 self-start overflow-hidden bg-[white] h-[100%]`}>
        <Sidebar user={user} />
      </div>
      <Drawer
        {...({} as any)}
        open={open}
        onClose={() => setOpen(false)}
        className="p-5 overflow-y-scroll 2xl:hidden scrollbar-hide"
      >
        <Sidebar user={user} setOpen={setOpen} open={open} />
      </Drawer>
      <div className="flex-[7] text-gray-900 text-[14px] flex flex-col pb-10 overflow-y-auto overflow-x-hidden">
        <Navbar open={open} setOpen={setOpen} user={user} />
        <div className="flex-1">{children}</div>
      </div>
    </div>
  );
};

export default DashboardLayout;
