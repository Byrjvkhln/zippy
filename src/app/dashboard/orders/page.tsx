"use client";

import React, { useState, useEffect } from "react";
import Link from "next/link";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Image from "next/image";
import axios from "axios";
import { useRouter } from "next/navigation";
import { convertToMongolianTime } from "@/src/utils/timeConvertor";
import Loader from "@/src/components/wait/Loader";
import Dropdown from "@/src/components/Dropdown";
const Orders = () => {
  const router = useRouter();
  const [products, setProducts] = useState([]);
  const [user, setUser] = useState<any>({});
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");
  // const [info, setInfo] = useState("");
  const [sum, setSum] = useState<any>("");
  const [emptyName, setEmptyName] = useState(false);
  const [emptyPhone, setEmptyPhone] = useState(false);
  const [emptyAddress, setEmptyAddress] = useState(false);
  const [loading, setLoading] = useState(false);
  const [price1, setPrice1] = useState("");
  const [price2, setPrice2] = useState("0");
  const [selectedCategory, setSelectedCategory] = useState("Бүгд");
  useEffect(() => {
    const storedProducts = localStorage.getItem("products");
    const storedUser = localStorage.getItem("user");
    if (storedProducts && storedUser) {
      setProducts(JSON.parse(storedProducts));
      setUser(JSON.parse(storedUser));
      sumCalculator(JSON.parse(storedProducts));
    }
  }, []);

  const handleChange = (value: any) => {
    setPhone(value);
    setEmptyPhone(value === "");
  };

  const send = async () => {
    if (!name) {
      setEmptyName(true);
    }
    if (!phone) {
      setEmptyPhone(true);
    }
    if (!address) {
      setEmptyAddress(true);
    }

    if (!name || !address || !phone) return;

    setLoading(true);

    try {
      await axios.post("/api/delivery", {
        CustomerId: user.id,
        Name: name,
        Location: address,
        PhoneNumber: phone.slice(3),
        Status: "waiting",
        DeliveryDate: "2024-07-10",
        Product: products.filter((el) => el !== null).map((el) => JSON.stringify(el)),
        DriverId: "678623f3057973ab31d6ff1c",
        Price: sum,
        PriceDetail: {
          tulsun: price1,
          belen: price2,
        },
        Street: selectedCategory,
      });
      await changer();

      router.push("/dashboard/delivery");
      setLoading(false);
    } catch (error) {
      console.error("Error during send operation:", error);
    }
  };

  const amountChanger = async (pro: any) => {
    await axios.put("  /api/product", {
      id: pro?.id,
      change: ["Amount", Number(pro?.Amount).toString()],
    });
    await axios.put("  /api/product", {
      id: pro?.id,
      change: [
        "Change",
        {
          amount: [...pro?.Change.amount, Number(pro?.Amount).toString()],
          time: [...pro?.Change.time, convertToMongolianTime(new Date())],
        },
      ],
    });
  };

  const sumCalculator: any = (a: any) => {
    let sum = 0;
    a?.map((el: any) => {
      if (el != null) {
        sum = sum + Number(el.too) * Number(el.Price);
      }
    });
    setSum(`${sum}`);
    setPrice1(`${sum}`);
  };
  const changer = async () => {
    await products.map((el, i) => {
      if (el != null) {
        amountChanger(el);
      }
    });
  };

  return (
    <div className="px-5">
      <div className="font-medium mb-5">
        <h2 className="font-bold text-2xl mb-2">Захиалга үүсгэх</h2>
        <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
          Эхлэл
        </Link>{" "}
        <ul className="inline-block text-gray-500 list-disc">
          <li>Захиалга үүсгэх</li>
        </ul>
      </div>

      <div className="flex flex-col lg:flex-row gap-5 ">
        <div className="lg:flex-[2] xl:flex-[2.5] 2xl:flex-[3] sm:flex-1 @container ">
          <h2 className="font-bold text-2xl mb-4">Хүргэлтийн дэлгэрэнгүй</h2>
          <div className="grid  @sm:grid-cols-2 @2xl:grid-cols-3 gap-5 mb-10">
            <div>
              <label> Нэр</label>
              <input
                type="text"
                value={name}
                onChange={(e: any) => {
                  setName(e.target.value);
                  setEmptyName(e.target.value === "");
                }}
                placeholder="Хүлээн авагчийн нэр"
                className={`block border px-4 py-2  rounded-lg w-full mt-3 outline-none ${
                  emptyName ? "border-red-500" : "border-gray-400"
                }`}
              />
              {emptyName && <p className="mt-1 text-red-500">Нэр оруулна уу!</p>}
            </div>
            <div>
              <label className="mb-3 block">Хүлээн авагчийн утасны дугаар </label>
              <div className={`border rounded-lg  pl-1 ${emptyPhone ? "border-red-500" : "border-gray-400"}`}>
                <PhoneInput
                  country={"mn"}
                  inputStyle={{
                    width: "100%",
                    height: "39px",
                    border: "none",
                    borderRadius: "8px",
                  }}
                  value={phone}
                  onChange={handleChange}
                />
              </div>
              {emptyPhone && <p className="mt-1 text-red-500">Утасны дугаар оруулна уу!</p>}
            </div>

            <div className="flex flex-col gap-2">
              <div>
                <label>Төлөгдсөн</label>
                <input
                  type="text"
                  defaultValue={sum}
                  onChange={(e) => setPrice1(e.target.value)}
                  className="block border px-4 py-2 border-gray-400 rounded-lg w-full mt-3 outline-none"
                />
              </div>
              <div>
                <label>Бэлэн авах төлбөр</label>
                <input
                  onChange={(e) => setPrice2(e.target.value)}
                  type="text"
                  defaultValue={"0"}
                  className="block border px-4 py-2 border-gray-400 rounded-lg w-full mt-2 outline-none"
                />
              </div>
              <div
                style={{ display: Number(price1) + Number(price2) == Number(sum) ? "none" : "flex" }}
                className="text-red-500 "
              >
                Төлбөр таарахгүй байна{"!"}
              </div>
            </div>
          </div>

          <div className="mb-10 flex justify-between">
            <div className="w-[66%]">
              <p className="mb-3">Хаягийн дэлгэрэнгүй мэдээлэл</p>
              <textarea
                className={`border w-full h-28 p-4  rounded-lg resize-none outline-none ${
                  emptyAddress ? "border-red-500" : "border-gray-400"
                }`}
                value={address}
                onChange={(e: any) => {
                  setAddress(e.target.value);
                  setEmptyAddress(e.target.value === "");
                }}
                placeholder="Хүргэлт хүргэх дэлгэрэнгүй мэдээллийг оруулна уу? Байрны орцны дугаар, давхар, тоот болон хаалганы код гэх мэт."
              ></textarea>
            </div>
            <div className="w-[31%]">
              <p className="mb-3">Дүүрэг</p>
              <div className="h-[40px] border flex w-full">
                <Dropdown
                  selectedCategory={selectedCategory}
                  setSelectedCategory={setSelectedCategory}
                  type={"street"}
                />
              </div>
            </div>
            {emptyAddress && <p className="mt-1 text-red-500">Хаягийн дэлгэрэнгүй оруулна уу!</p>}
          </div>
          {/* <div>
            <p className="mb-3">Нэмэлт мэдээлэл (Заавал бөглөх албагүй)</p>
            <textarea
              className="border w-full h-28 p-4 border-gray-400 rounded-lg resize-none outline-none"
              onChange={(e: any) => setInfo(e.target.value)}
              value={info}
              placeholder="Жолоочид дамжуулах эсвэл хүргэлтэнд шаардлагатай нэмэлт мэдээлэл байгаа гэж үзвэл бөглөөрэй."
            ></textarea>
          </div> */}
        </div>

        <div
          className={` 
             sm:flex-[1] xl:flex-[1.3] w-full self-start h-fit lg:mt-9 
          `}
        >
          <h2 className="font-bold text-xl mb-4">Барааны жагсаалт</h2>
          <div className="bg-white rounded-lg">
            <div className="border border-gray-400 rounded-lg  pb-6 p-4 lg:p-5">
              <div className=" flex justify-between border-b pb-5 ">
                <p>Нийт:</p>
                <Link href="/dashboard/delivery" className="text-blue-900 underline hover:text-gray-900">
                  Бараа нэмэх
                </Link>
              </div>
              <div className="border-b h-[200px] overflow-auto">
                {products.map((p: any) => {
                  return (
                    p !== null && (
                      <div className="flex justify-between items-center my-3">
                        <div className="flex items-center gap-2">
                          {" "}
                          <div className="size-16 rounded-lg overflow-hidden">
                            <Image src={p.Img} alt="" width={100} height={100} className="w-full h-full object-cover" />
                          </div>
                          <div>
                            <p className="font-semibold">{p.Name}</p>
                            <p>
                              {Number(p.Price).toLocaleString()}₮ x {p.too}
                            </p>
                          </div>
                        </div>
                        <p className="font-semibold text-gray-800">{(p.Price * p.too).toLocaleString()}₮</p>
                      </div>
                    )
                  );
                })}
              </div>
              <div className="py-4 lg:pb-2 text-base *:mb-2 ">
                <div className="flex justify-between">
                  <p className="text-gray-600">Нийт </p>
                  <p className="font-semibold text-gray-800">
                    {Number(sum).toLocaleString("en-US", {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                    ₮
                  </p>
                </div>
                <div className="flex justify-between">
                  <p className="text-gray-600">Хүргэлтийн төлбөр </p>
                  <p className="font-semibold text-gray-800"> {user.TransportCost}₮</p>
                </div>
              </div>
              <div className="">
                <div className="flex items-center justify-between mb-4 font-semibold text-base">
                  <p>Нийт</p>
                  <p></p>
                </div>
                <button
                  onClick={send}
                  className=" bg-main w-full px-4 py-2 text-white rounded-lg text-base font-medium hover:bg-blue-900 transition-colors duration-300"
                >
                  {loading ? <Loader color="white" /> : "Хадгалах"}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Orders;
