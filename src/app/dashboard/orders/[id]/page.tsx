"use client";

import React, { useState, useEffect } from "react";
import axios from "axios";
import Link from "next/link";
import { FaLocationArrow, FaUser, FaPhoneAlt } from "react-icons/fa";
import Order from "../../../../components/delivery/Order";
import Loading from "../../../../components/wait/loading";
import { convertToMongolianTime } from "@/src/utils/timeConvertor";

const SingleOrder = ({ params }: any) => {
  const [order, setOrder] = useState<any>("");
  const [user, setUser] = useState<any>({});
  const { id } = params;
  const products = order?.Product?.map((p: any) => JSON.parse(p));
  let addition = 0;

  useEffect(() => {
    const fetchDelivery = async () => {
      try {
        const res = await axios.get(`/api/delivery/${id}`, {
          headers: {
            "x-api-key": "Lol999za",
          },
        });
        setOrder(res.data);
        const storedUser = localStorage.getItem("user");
        if (storedUser) {
          setUser(JSON.parse(storedUser));
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchDelivery();
  }, [id]);

  const time = convertToMongolianTime(order?.createdAt);
  const deliveredTime = convertToMongolianTime(order?.updatedAt);
  products?.map((p: any) => (addition += Number(p.Price) * p.too));

  return (
    <div className="px-5 h-full flex flex-col ">
      {order ? (
        <div>
          {" "}
          <div className="font-medium mb-10">
            <h2 className="font-bold text-2xl mb-2">{order?.id}</h2>
            <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
              Эхлэл
            </Link>{" "}
            <ul className="inline-block text-gray-500 list-disc">
              <li>Хүргэлтийн дэлгэрэнгүй</li>
            </ul>
          </div>
          <div className="flex flex-col lg:flex-row gap-5 bg-white p-5 rounded-lg shadow-md 3xl:h-full">
            <div className="flex-1 lg:flex-[2] xl:flex-[1.5]">
              <p className="text-xl font-semibold text-gray-700 mb-5">Хүргэлт</p>
              <div className="border-t py-5">
                <p className="mb-3 font-semibold text-gray-700">Хүлээн авагчийн мэдээлэл</p>
                <div className="bg-gray-200 p-5 rounded-lg text-gray-900 mb-5">
                  <div className="flex items-center gap-3 mb-3">
                    <FaLocationArrow className="text-blue-900" />
                    <p>
                      {order.Street}, {order.Location}
                    </p>
                  </div>
                  <div className="flex items-center gap-3 mb-3">
                    <FaUser className="text-blue-900" />
                    <p>{order.Name}</p>
                  </div>
                  <div className="flex items-center gap-3 mb-3">
                    <FaPhoneAlt className="text-blue-900" />
                    <p>{order.PhoneNumber}</p>
                  </div>

                  <p> </p>
                </div>

                <p className="font-semibold text-gray-700 mb-3">
                  Жолооч:{" "}
                  {order.Status != "waiting" &&
                    order.Driver?.Name &&
                    order.Driver?.Surname[0] + "." + order.Driver?.Name}
                </p>

                <i className="mb-3 block">{order.Status != "waiting" && order.Driver?.PhoneNumber}</i>
                <div className="flex items-center justify-between mb-8">
                  {order.Status && (
                    <div
                      className={` text-white font-medium w-fit px-4 py-2 rounded-3xl ${
                        order.Status === "waiting"
                          ? "bg-red1"
                          : order.Status === "comfirmed"
                          ? "bg-main"
                          : order.Status === "done"
                          ? "bg-green1"
                          : order.Status === "cancel"
                          ? "bg-red1"
                          : order.Status === "upcoming"
                          ? "bg-yellow1"
                          : "bg-gray-400"
                      }`}
                    >
                      {order.Status === "waiting"
                        ? "Баталгаажаагүй"
                        : order.Status === "comfirmed"
                        ? "Баталгаажсан"
                        : order.Status === "upcoming"
                        ? "Хүргэлтэнд гарсан"
                        : order.Status === "done"
                        ? " Хүргэгдсэн"
                        : order.Status === "cancel"
                        ? "Цуцлагдсан"
                        : "Хойшилсон"}
                    </div>
                  )}

                  <p className="text-base text-blue-900 font-medium flex items-center">
                    {Number(order?.Price)?.toLocaleString("en-US", {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                    {"("}
                    <p className="text-xs  text-[#32CD32]">
                      {Number(order.PriceDetail.tulsun)?.toLocaleString("en-US", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </p>
                    +
                    <p className="text-xs  text-[#FFD700]">
                      {Number(order.PriceDetail.belen)?.toLocaleString("en-US", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </p>
                    {")"}₮
                  </p>
                </div>

                <p>Хүргэлт үүссэн ОГНОО: {order.Status === "waiting" ? "" : time}</p>
                <p className="mb-5">Хүргэгдсэн: {order.Status === "done" ? deliveredTime : "~"}</p>

                <p className="mb-10">{time}</p>

                <Link
                  href="/dashboard/delivery"
                  className="bg-main text-white text-base font-medium px-4 py-2 rounded-lg hover:bg-blue-900 transition-colors duration-300"
                >
                  Жагсаалт руу буцах
                </Link>
              </div>
            </div>

            <div className="flex-[2.5]">
              <p className="text-xl font-semibold text-gray-700 mb-5"> Нийт бараа: {order.Product?.length}</p>

              <div className="border-t @container py-5">
                <div className="grid @2xl:grid-cols-3 @sm:grid-cols-2 gap-5">
                  {products?.map((p: any) => (
                    <Order {...p} key={p.id} />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <Loading />
      )}
    </div>
  );
};

export default SingleOrder;
