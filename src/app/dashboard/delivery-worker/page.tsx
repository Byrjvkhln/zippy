"use client";

import { Workers } from "../../../components/delivery-worker/Workers";
import React, { useState } from "react";
import { FaPlus } from "react-icons/fa6";
import { RiLoader3Line } from "react-icons/ri";
import Modal from "../../../components/delivery-worker/Modal";
import axios from "axios";
const DeliveryWorker = () => {
  const [showworkers, setShowworkers] = useState([]);

  const [drivers, setDrivers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [refetch, setReFetch] = useState(false);
  const fetchDrivers = async ({ currentPage, driverPerPage }: any) => {
    setLoading(true);
    const res = await axios.get("/api/driver", {
      headers: {
        "x-api-key": "Lol999za",
      },
      params: {
        page: currentPage,
        pageSize: driverPerPage,
      },
    });
    setDrivers(res.data.reverse());
    setShowworkers(res.data.reverse());
    setLoading(false);
    setReFetch(false);
  };
  const [show, setShow] = useState(false);
  return (
    <div className="px-5 h-full">
      <div className="p-5 bg-white shadow-md text-gray-900 text-[14px] rounded-lg h-full">
        <h2 className="text-lg font-bold">Хүргэлтийн ажилтан</h2>
        <p className="mb-5">Zippy хүргэлтийн системд бүртгэлтэй жолоочдын жагсаалт</p>

        <div className="flex sm:justify-end  mb-10">
          <button
            onClick={() => setShow(!show)}
            className="flex items-center gap-2 bg-main px-4 py-2 text-white font-semibold rounded-lg hover:bg-blue-900 transition-colors duration-300"
          >
            <FaPlus className="text-lg" /> Шинэ жолооч
          </button>
          {show && <Modal show={show} setShow={setShow} />}
        </div>

        <div className="flex items-center gap-5 mb-10 overflow-auto scrollbar-hide">
          <div className="flex items-center gap-5">
            <input
              onChange={(e) =>
                setShowworkers(
                  drivers?.filter((el: any) => el?.Name?.toLowerCase().includes(e.target.value.toLowerCase()))
                )
              }
              type="text"
              className="border border-gray-400 rounded-lg px-4 py-2 outline-none"
              placeholder="Жолоочийн нэрээр хайх"
            />
          </div>

          <div>
            <RiLoader3Line
              className={`text-xl text-green-800 cursor-pointer ${refetch && "animate-spin"} `}
              onClick={() => setReFetch(!refetch)}
            />
          </div>
        </div>

        <Workers refetch={refetch} fetchDrivers={fetchDrivers} drivers={showworkers} show={show} loading={loading} />
      </div>
    </div>
  );
};

export default DeliveryWorker;
