"use client";

import React, { useState, useEffect } from "react";
import { Avatar } from "@material-tailwind/react";
import axios from "axios";
import Link from "next/link";
import Loading from "../../../../components/wait/loading";
import Stat from "../../../../components/delivery-worker/Stat";
import SalaryInfo from "../../../../components/delivery-worker/SalaryInfo";
import Documents from "@/src/components/delivery-worker/Documents";
import { CiEdit } from "react-icons/ci";
import EditModal from "../../../../components/EditModal";

const Worker = ({ params }: any) => {
  const { id } = params;
  const [driver, setDriver] = useState<any>();
  const [openPass, setOpenPass] = useState(false);
  const [openPhone, setOpenPhone] = useState(false);
  const [openEmail, setOpenEmail] = useState(false);
  const [openSalary, setOpenSalary] = useState(false);

  useEffect(() => {
    const fetchDriver = async () => {
      const res = await axios.get(`  /api/driver/${id}`, {
        headers: {
          "x-api-key": "Lol999za",
        },
      });
      setDriver(res.data);
    };
    fetchDriver();
  }, [id, openPass, openEmail, openPhone, openSalary]);

  return (
    <>
      {driver ? (
        <div className="px-5 ">
          <div>
            <div className="font-medium mb-10">
              <div>
                {" "}
                <h2 className="font-bold text-2xl mb-2">{`${driver?.Surname} ${driver?.Name}`}</h2>
                <div className="flex flex-wrap gap-2">
                  {" "}
                  <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
                    Эхлэл
                  </Link>{" "}
                  <ul className=" text-gray-500 list-disc flex gap-7">
                    <li>
                      <Link href="/dashboard/delivery-worker">Хүргэлтийн ажилтан</Link>
                    </li>
                    <li>Жолоочийн дэлгэрэнгүй</li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="flex flex-col lg:flex-row gap-10 bg-white p-5 rounded-lg shadow-md">
              <div className="flex-[2]">
                <div className="flex items-center gap-3 mb-5">
                  <Avatar
                    src="https://wallpapers.com/images/hd/profile-picture-xj8jigxkai9jag4g.jpg"
                    alt=""
                    size="sm"
                    {...({} as any)}
                  />
                  <div className="flex flex-col">
                    <p className="font-semibold mb-1">{driver?.Surname}</p>
                    <p className="text-gray-700">{driver?.Email}</p>
                  </div>
                </div>

                <div className="border-b pb-5 border-gray-400 mb-5">
                  <p className="text-lg font-semibold text-main mb-5">Дансны мэдээлэл</p>
                  <div className="">
                    <div className="flex  mb-2">
                      <p className="flex-1">Дансны дугаар</p>
                      <p className="flex-1 xl:flex-[2]">{driver?.Account}</p>
                    </div>
                    <div className="flex">
                      <p className="flex-1">Данс эзэмшигчийн нэр</p>
                      <p className="flex-1 xl:flex-[2]">{driver?.AccountName}</p>
                    </div>
                  </div>
                </div>

                <Documents />

                <div className="text-gray-800">
                  <p className="text-lg font-semibold text-main mb-5">Бүртгэлийн мэдээлэл</p>

                  <div className="flex items-center gap-2 mb-5 cursor-pointer" onClick={() => setOpenPass(!openPass)}>
                    <CiEdit className="text-2xl" />
                    <p>Нууц үг солих</p>
                  </div>
                  {(openPass || openPhone || openEmail || openSalary) && (
                    <EditModal
                      openPass={openPass}
                      setOpenPass={setOpenPass}
                      openPhone={openPhone}
                      setOpenPhone={setOpenPhone}
                      phone={driver?.PhoneNumber}
                      openEmail={openEmail}
                      setOpenEmail={setOpenEmail}
                      email={driver?.Email}
                      id={driver?.id}
                      openSalary={openSalary}
                      setOpenSalary={setOpenSalary}
                    />
                  )}
                  <div className="flex items-center gap-2 mb-5 cursor-pointer" onClick={() => setOpenPhone(!openPhone)}>
                    <CiEdit className="text-2xl" />
                    <p>{driver?.PhoneNumber}</p>
                  </div>
                  {/* {openPass && <EditModal openPass={openPass} setOpenPass={setOpenPass}/>} */}
                  <div
                    className="flex items-center gap-2 mb-10 cursor-pointer"
                    onClick={() => setOpenEmail(!openEmail)}
                  >
                    <CiEdit className="text-2xl" />
                    <p>{driver?.Email}</p>
                  </div>
                  <div className="flex items-center  mb-10 justify-between">
                    <p>Системд бүртгүүлсэн</p>
                    <p>{driver?.createdAt.split("T")[0]}</p>
                  </div>
                </div>
              </div>
              <div className="flex-1">
                <div className="border-b border-gray-400 pb-3">
                  <p className="text-lg font-semibold text-main mb-5">Татан авалт</p>
                  <Stat text="Гүйцэтгэсэн" num={0} />
                  <Stat text="Хүлээгдэж буй" num={0} />
                  <Stat text="Авахаар очиж байгаа" num={0} />
                  <Stat text="Аваад ирж байгаа" num={0} />
                </div>
                <div className="border-b border-gray-400 pb-3 mt-5">
                  <p className="text-lg font-semibold text-main mb-5">Хүргэлт</p>
                  <Stat text="Хүргэгдсэн" num={0} />
                  <Stat text="Хүлээгдэж буй" num={0} />
                  <Stat text="Хүргэлтэнд гарсан" num={0} />
                  <Stat text="Буцаагдсан" num={0} />
                  <Stat text="Цуцлагдсан" num={0} />
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <Loading />
      )}
    </>
  );
};

export default Worker;
