"use client";

import React, { useState, useRef, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { format } from "date-fns";
import { GoCalendar } from "react-icons/go";
import axios from "axios";
import Loader from "../../../components/wait/Loader";
import { filtererTime } from "@/src/utils/timeinterval";
import { Avatar } from "@material-tailwind/react";
import DeliveryReport from "@/src/components/DeliveryReport";
import Link from "next/link";
const Accounting = () => {
  const today = new Date();
  const [dateRange, setDateRange] = useState<[Date | null, Date | null]>([today, today]);
  const [startDate, endDate]: any = dateRange;
  const [showDatePicker, setShowDatePicker] = useState(false);
  const datePickerRef = useRef<HTMLDivElement>(null);
  const [customers, setCustomers] = useState([]);
  const [deliveries, setDeliveries] = useState<any>([]);
  const [chosen, setChosen] = useState<any>([]);
  const [loading, setLoading] = useState(true);
  const [id, setId] = useState();
  const [name, setName] = useState();
  const [tracost, setTracost] = useState();
  const [currentPage, setCurrentPage] = useState(1);
  const deliveryPerPage = 100;
  const toggleDatePicker = () => {
    setShowDatePicker(!showDatePicker);
  };

  const handleDateChange = (update: [Date | null, Date | null]) => {
    setDateRange(update);
    if (update[0] && update[1]) {
      setShowDatePicker(false);
    }
  };

  const handleClickOutside = (event: MouseEvent) => {
    if (datePickerRef.current && !datePickerRef.current.contains(event.target as Node)) {
      setShowDatePicker(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  useEffect(() => {
    const fetchCustomers = async () => {
      const res = await axios.get("/api/customer", {
        headers: {
          "x-api-key": "Lol999za",
        },
        params: {
          app: true,
        },
      });
      setCustomers(res.data);
      setLoading(false);
    };

    fetchCustomers();
  }, []);

  const fetchDeliveries = async () => {
    const res = await axios.post("/api/delivery/customerid", {
      customerId: id,
      page: currentPage,
      pageSize: deliveryPerPage,
      filter: "done",
    });
    const result = await filtererTime(res.data, startDate, endDate);
    setDeliveries(result.reverse());
  };
  useEffect(() => {
    fetchDeliveries();
    setChosen([]);
  }, [id]);
  const invoicehandler = async () => {
    setLoading(true);
    let total = 0;
    let Value = 0;
    const Items = await chosen?.map((el: any) => {
      total = total + Number(el?.Price);
      Value = Value + 1;
      return el?.id;
    });

    await axios.post("/api/invoice", {
      customerId: id,
      Items,
      total: total.toString(),
      Value: Value.toString(),
      CustomerName: name,
      Interval: {
        Start: startDate,
        End: endDate,
      },
      totaltransportcost: (Number(tracost) * Value).toString(),
    });
    window.location.reload();
  };

  return (
    <>
      {loading ? (
        <Loader color="#51ba5b" />
      ) : (
        <div className="px-5 h-full  flex flex-col">
          <div className="font-medium mb-10">
            <h2 className="font-bold text-2xl mb-2">Нэхэмжлэл үүсгэх</h2>
            <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
              Эхлэл
            </Link>
            <ul className="inline-block text-gray-500 list-disc">
              <li>Хүргэлт</li>
            </ul>
          </div>
          {customers.length > 0 && (
            <div
              className={`grid sm:grid-cols-2  ${
                customers.length > 12 && "h-60 overflow-auto"
              } overflow-auto p-5 gap-5 rounded-lg bg-white shadow-md lg:grid-cols-3 2xl:grid-cols-4 mb-5`}
            >
              {customers?.map((c: any) => (
                <div
                  className={`cursor-pointer p-2 ${c.id === id ? "border rounded border-main" : ""}`}
                  key={c.id}
                  onClick={() => (setId(c?.id), setName(c?.Name), setTracost(c?.TransportCost))}
                >
                  <div className="flex items-center  gap-3 ">
                    <Avatar src={c.Image} alt="" size="sm" {...({} as any)} />
                    <div>
                      <p className=" text-center line-clamp-1">{c.Name}</p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}

          <div className="flex items-center gap-4 mb-10">
            <div
              className="relative bg-white shadow-md w-[233px] px-4 py-2 rounded-lg cursor-pointer flex items-center"
              ref={datePickerRef}
              onClick={toggleDatePicker}
              style={{
                display: id ? "flex" : "none",
              }}
            >
              <GoCalendar className="text-xl" />
              <span className={`${startDate ? "ml-2 shrink-0" : "ml-2"}`}>
                {startDate ? format(startDate, "yyyy/MM/dd") : format(today, "yyyy/MM/dd")} -{" "}
                {endDate ? format(endDate, "yyyy/MM/dd") : format(today, "yyyy/MM/dd")}
              </span>

              {showDatePicker && (
                <div className="absolute z-10 mt-4 top-full left-0" onClick={(e: any) => e.stopPropagation()}>
                  <DatePicker
                    selected={startDate}
                    onChange={handleDateChange}
                    startDate={startDate}
                    endDate={endDate}
                    selectsRange
                    inline
                  />
                </div>
              )}
            </div>

            <button
              onClick={() => fetchDeliveries()}
              className="bg-main px-4 py-2 text-white font-semibold rounded-lg hover:bg-blue-900 transition-colors duration-300"
              style={{
                display: id ? "flex" : "none",
              }}
            >
              Шүүх
            </button>
            <button
              onClick={() =>
                setChosen(
                  deliveries?.filter((el: any) => {
                    return {
                      id: el?.id,
                      Price: el?.Price,
                    };
                  })
                )
              }
              className="bg-main px-4 py-2 text-white font-semibold rounded-lg hover:bg-blue-900 transition-colors duration-300"
              style={{
                display: deliveries.length > 0 ? "flex" : "none",
              }}
            >
              Бүгдийг сонгох
            </button>
          </div>

          {deliveries?.length > 0 && (
            <DeliveryReport
              setCurrentPage={setCurrentPage}
              currentPage={currentPage}
              deliveryPerPage={deliveryPerPage}
              customers={customers}
              deliveries={deliveries}
              setChosen={setChosen}
              chosen={chosen}
            />
          )}
          <div className="w-[100%] flex justify-end mt-2 ">
            <button
              onClick={() => invoicehandler()}
              className="bg-main px-4 py-2 text-white font-semibold rounded-lg hover:bg-blue-900 transition-colors duration-300"
              style={{
                display: chosen.length > 0 && deliveries.length > 0 ? "flex" : "none",
              }}
            >
              Нэхэмжлэл үүсгэх
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default Accounting;
