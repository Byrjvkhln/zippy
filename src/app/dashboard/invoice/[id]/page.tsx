"use client";
import React, { useRef, useEffect, useState } from "react";
import ReactToPrint from "react-to-print";
import PrintComponent from "@/src/components/invoicepdf";
import axios from "axios";
import Link from "next/link";
import Loader from "@/src/components/wait/Loader";
import Image from "next/image";
import { color } from "framer-motion";
interface PageProps {
  params: { id: string };
}

const InvoiceDetail: React.FC<PageProps> = ({ params }) => {
  const componentRef = useRef<HTMLDivElement>(null); // Ref for the component to print
  const [invoice, setInvoice] = useState<any>();
  const [loading, setloading] = useState(false);
  const [user, setUser] = useState<any>();

  const invoicehandler = async () => {
    setloading(true);
    const response = await axios.get(`/api/invoice/${params?.id}`, {
      headers: {
        "x-api-key": "Lol999za",
      },
    });
    setInvoice(response?.data);
    setloading(false);
  };
  useEffect(() => {
    invoicehandler();
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      try {
        setUser(JSON.parse(storedUser));
      } catch (e) {
        console.error("Failed to parse user from localStorage:", e);
      }
    }
  }, [params.id]);
  const statusHandler = async (change: any) => {
    await axios.put("/api/invoice", {
      id: invoice?.id,
      change,
    });
    invoicehandler();
  };
  return loading ? (
    <Loader color="#51ba5b" />
  ) : (
    <div className="px-10">
      <div className="font-medium mb-5">
        <h2 className="font-bold text-2xl mb-2">Нэхэмжлэл харах</h2>
        <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
          Эхлэл
        </Link>
        <ul className="inline-block text-gray-500 list-disc">
          <li>Нэхэмжлэл харах</li>
        </ul>
      </div>
      <div className="w-[100%] flex justify-between mb-2 ">
        <ReactToPrint
          trigger={() => (
            <button className="flex  rounded-lg items-center  gap-2  hover:opacity-50">
              <div className="font-semibold text-[#183770]">Хэвлэх </div>
              <Image src={`/print.svg`} alt="" width={25} height={20} />
            </button>
          )}
          content={() => componentRef.current}
        />

        <div
          style={{
            display: invoice?.Status == "Хүлээгдэж байна" && user?.Status == "admin" ? "flex" : "none",
          }}
          className="flex gap-5"
        >
          <button
            onClick={() => statusHandler(["Status", "Баталсан"])}
            className="flex  rounded-md items-center px-5 h-[30px]   border-[1px] gap-2 bg-[#51ba5b] hover:opacity-50"
          >
            <div className="font-semibold text-white">Батлах </div>
          </button>
          <button
            onClick={() => statusHandler(["Status", "Цуцлагдсан"])}
            className="flex  rounded-md items-center px-5 h-[30px]   border-[1px] gap-2 bg-[#E3242B] hover:opacity-50"
          >
            <div className="font-semibold text-white">Цуцлах </div>
          </button>
        </div>
      </div>
      <PrintComponent invoice={invoice} ref={componentRef} />
    </div>
  );
};

export default InvoiceDetail;
