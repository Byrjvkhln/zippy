"use client";
import { useEffect, useState } from "react";
import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/navigation";
const Invoice = () => {
  const router = useRouter();
  const [invoices, setInvoices] = useState<any>([]);
  const StatusColor: any = {
    Баталсан: "text-[#008767] border-[#008767] bg-[#A1E5E2]",
    "Хүлээгдэж байна": "text-[#FFC107] border-[#FFC107] bg-[#FFF9C4]",
    Цуцлагдсан: "text-[#DF0404] border-[#DF0404] bg-[#FFC5C5]",
  };
  const [user, setUser] = useState<any>(null);

  const invoicehandler = async (customer: any) => {
    const result: any = await axios.get(
      customer.Status == "admin" ? "/api/invoice" : `/api/invoice/customer/${customer?.id}`,
      {
        headers: {
          "x-api-key": "Lol999za",
        },
        params: {
          app: true,
        },
      }
    );
    setInvoices(result?.data);
  };
  useEffect(() => {
    const storedUser: any = localStorage.getItem("user");

    if (storedUser) {
      try {
        setUser(JSON.parse(storedUser));
      } catch (e) {
        console.error("Failed to parse user from localStorage:", e);
      }
    }
    invoicehandler(JSON.parse(storedUser));
  }, []);

  return (
    <div className="text-[14px] text-gray-900 px-5 h-full flex flex-col">
      <div className="font-medium mb-10">
        <h2 className="font-bold text-2xl mb-2">Нэхэмжлэл</h2>
        <Link href="/dashboard/delivery" className="text-gray-800 mr-5">
          Эхлэл
        </Link>
        <ul className="inline-block text-gray-500 list-disc">
          <li>Нэхэмжлэл</li>
        </ul>
      </div>
      <div className="bg-[white] rounded-xl p-7 flex flex-col gap-5">
        <div className="flex gap-2 flex-col ">
          <div className="text-[22px] font-semibold">Нэхэмжлэлийн түүх</div>
          <div className="text-[#16C098]">Бүртгэгдсэн нэхэмжлэлүүд</div>
        </div>
        <table className="w-[100%] overflow-scroll ">
          <thead className="border-b-[1px] overflow-scroll">
            <tr>
              <th className="font-medium text-[#808080] text-left pb-4   w-[220px] ">id</th>
              <th className="font-medium text-[#808080] text-left pb-4    w-[120px] ">Барааны үнэ</th>
              <th className="font-medium text-[#808080] text-left pb-4  w-[120px] ">Хүргэлтийн үнэ</th>
              <th className="font-medium text-[#808080] text-left pb-4    ">Ширхэг</th>
              <th className="font-medium text-[#808080] text-left pb-4   w-[150px] ">Байгууллага</th>
              <th className="font-medium text-[#808080] text-left pb-4   w-[230px] ">Төлөв</th>
              <th className="font-medium text-[#808080] text-left pb-4   w-[150px] ">ОГНОО</th>
            </tr>
          </thead>
          <tbody>
            {invoices?.map((el: any) => {
              return (
                <tr
                  onClick={() => router.replace(`/dashboard/invoice/${el?.id}`)}
                  key={el?.id}
                  className="cursor-pointer hover:bg-[#F0F0F0] "
                >
                  <td className="text-left pt-5  ">{el?.id}</td>
                  <td className="text-left pt-5    ">{el?.total}₮</td>
                  <td className="text-left pt-5     ">{el?.totaltransportcost}₮</td>
                  <td className="text-left pt-5 ">
                    <div className="flex ps-5">{el?.Value}</div>
                  </td>
                  <td className="text-left pt-5   ">{el?.CustomerName}</td>
                  <td className="text-left pt-5   ">
                    <div className="w-[100%] h-[50px] ">
                      <div className={`rounded-md p-2 w-[65%]  ${StatusColor[el?.Status]}  border-[2px] font-semibold`}>
                        {el?.Status}
                      </div>
                    </div>
                  </td>
                  <td className="text-left pt-5   ">{el?.createdAt.split("T")[0]}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};
export default Invoice;
