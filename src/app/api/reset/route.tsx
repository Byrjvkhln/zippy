import { prisma } from "@/src/utils/prisma";
import { NextResponse, NextRequest } from "next/server";
import { addAdmin } from "@/src/services/adminservice";
export const DELETE = async (req: NextRequest) => {
  try {
    const data = await req.json();
    // await prisma.admin.deleteMany({});
    // await prisma.cancelReq.deleteMany({});
    // await prisma.driver.deleteMany({});
    // await prisma.news.deleteMany({});
    // await prisma.product.deleteMany({});
    await prisma.delivery.deleteMany({
      where: {
        CustomerId: data?.id,
      },
    });
    // await prisma.customer.deleteMany({});
    // const { response, error } = await addAdmin(data);
    // if (error) return NextResponse.json(error, { status: 500 });
    //Jolooch nemeed IDgn default choloochor soli
    return NextResponse.json("Амжилттай");
  } catch (error) {
    return NextResponse.json(error);
  }
};
export const PUT = async (req: NextRequest) => {
  try {
    const result = await updater();
    return NextResponse.json(result);
  } catch (error) {
    console.error("Error updating deliveries:", error); // Log the error for debugging
    return NextResponse.json({ error: "Failed to update deliveries." }, { status: 500 });
  }
};

const updater = async () => {
  const updatedDeliveries = await prisma.delivery.updateMany({
    // Change 'test' to 'delivery'
    where: {}, // This is empty to target all deliveries
    data: {
      Street: "Бүгд", // Use the new field you've added
    },
  });

  return { updatedCount: updatedDeliveries.count }; // Return the updated count
};
