import { NextResponse, NextRequest } from "next/server";
import { getDeliverByCustomerId } from "@/src/services/deliveryservice";
import { prisma } from "@/src/utils/prisma";

export const GET = async (req: NextRequest, res: NextResponse) => {
  const id: any = req.nextUrl.searchParams.get("id");
  console.log({ id });

  let dbData = await prisma.delivery.findMany({
    where: {
      ...(id != "null" && { CustomerId: id }),
    },
    include: {
      Driver: true,
    },
    orderBy: {
      createdAt: "desc",
    },
  });
  console.log(dbData);

  const start = req.nextUrl.searchParams.get("start") || "0"; // Default to 0
  const size = req.nextUrl.searchParams.get("size") || "10"; // Default to 10
  const filters = req.nextUrl.searchParams.get("filters"); // Default to 10
  const sorting = req.nextUrl.searchParams.get("sorting"); // Default to 10
  const globalFilter = req.nextUrl.searchParams.get("globalFilter"); // Default to 10

  const parsedColumnFilters = JSON.parse(filters || "");

  if (parsedColumnFilters?.length) {
    parsedColumnFilters.map((filter: any) => {
      const { id: columnId, value: filterValue } = filter;
      dbData = dbData.filter((row: any) => {
        if (columnId === "lastLogin") {
          const rowDateValue = new Date(row[columnId]);
          const filterDateValue = new Date(filterValue as string);
          return rowDateValue > filterDateValue;
        }
        return row[columnId]
          ?.toString()
          ?.toLowerCase()
          ?.includes?.((filterValue as string).toLowerCase());
      });
    });
  }

  if (globalFilter) {
    dbData = dbData.filter((row: any) =>
      Object.keys(row).some((columnId) =>
        row[columnId]
          ?.toString()
          ?.toLowerCase()
          ?.includes?.((globalFilter as string).toLowerCase())
      )
    );
  }

  const parsedSorting = JSON.parse(sorting || "");
  if (parsedSorting?.length) {
    const sort = parsedSorting[0];
    const { id, desc } = sort;
    dbData.sort((a: any, b: any) => {
      if (desc) {
        return a[id] < b[id] ? 1 : -1;
      }
      return a[id] > b[id] ? 1 : -1;
    });
  }
  const a: any = {
    data: dbData?.slice(parseInt(start), parseInt(start) + parseInt(size)) ?? [],
    meta: { totalRowCount: dbData.length },
  };
  return NextResponse.json(a);
};
export const POST = async (req: NextRequest) => {
  const data = await req.json();

  const { response, error } = await getDeliverByCustomerId(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
