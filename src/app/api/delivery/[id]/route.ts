import { NextResponse, NextRequest } from "next/server";
import { getPathVariable } from "@/src/utils/url";
import { getDeliverById } from "@/src/services/deliveryservice";
export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");

  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  const id = await getPathVariable(req, "/api/delivery/");

  const { response, error } = await getDeliverById(id);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
