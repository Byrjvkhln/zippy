import { NextResponse, NextRequest } from "next/server";
import { addDelivery, getAllDelivery, changeDelivery, deletealldelivery } from "@/src/services/deliveryservice";
export const POST = async (req: NextRequest) => {
  const data = await req.json();
  const { response, error } = await addDelivery(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");
  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  const { response, error } = await getAllDelivery();
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const PUT = async (req: NextRequest) => {
  const { id, change } = await req.json();
  const { response, error } = await changeDelivery({ id, change });
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const DELETE = async () => {
  await deletealldelivery();
};
