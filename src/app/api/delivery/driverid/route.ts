import { NextResponse, NextRequest } from "next/server";
import { getDeliverByDriverId } from "@/src/services/deliveryservice";
export const POST = async (req: NextRequest) => {
  const driverId = await req.json();
  const { response, error } = await getDeliverByDriverId(driverId);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
