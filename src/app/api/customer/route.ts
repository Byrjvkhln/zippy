import { NextResponse, NextRequest } from "next/server";
import { addCustomer, getALLCustomer, changeCustomer } from "@/src/services/customerservice";
export const POST = async (req: NextRequest) => {
  const data = await req.json();
  const { response, error } = await addCustomer(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");

  // Compare with the expected API key
  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  const { searchParams } = new URL(req.url);
  const data = {
    page: searchParams.get("page"),
    pageSize: searchParams.get("pageSize"),
    app: searchParams.get("app"),
  };
  const { response, error } = await getALLCustomer(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const PUT = async (req: NextRequest) => {
  const { id, change } = await req.json();

  const { response, error } = await changeCustomer({ id, change });
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
