import { NextResponse, NextRequest } from "next/server";
import { createContact } from "@/src/services/contactservice";

export const POST = async (req: NextRequest) => {
  const data = await req.json();

  const { response, error }: any = await createContact(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response, { status: 200 });
};
