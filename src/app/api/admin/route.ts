import { NextResponse, NextRequest } from "next/server";
import { addAdmin, getAllAdmin } from "@/src/services/adminservice";
export const POST = async (req: NextRequest) => {
  const data = await req.json();
  const { response, error } = await addAdmin(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");

  // Compare with the expected API key
  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  const { response, error } = await getAllAdmin();
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
