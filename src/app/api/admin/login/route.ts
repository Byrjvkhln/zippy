import { NextResponse, NextRequest } from "next/server";
import { LoginAdmin } from "@/src/services/adminservice";
export const POST = async (req: NextRequest) => {
  const data = await req.json();

  const { response, error }: any = await LoginAdmin(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response, { status: 200 });
};
