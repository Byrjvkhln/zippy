import { NextResponse, NextRequest } from "next/server";
import { getPathVariable } from "@/src/utils/url";
import { getRequestById } from "@/src/services/cancelrequest";
export const GET = async (req: NextRequest) => {
  const id = await getPathVariable(req, "/api/cancelRequest/findone/");

  const { response, error } = await getRequestById(id);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
