import { NextResponse, NextRequest } from "next/server";
import { getProductByCustomerId } from "@/src/services/productservice";
export const POST = async (req: NextRequest) => {
  const customerId = await req.json();
  const { response, error } = await getProductByCustomerId(customerId);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
