import { NextResponse, NextRequest } from "next/server";
import { addProducts, getAllProducts, changeProduct } from "@/src/services/productservice";
export const POST = async (req: NextRequest) => {
  const data = await req.json();
  const { response, error } = await addProducts(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const PUT = async (req: NextRequest) => {
  const { id, change } = await req.json();
  const { response, error } = await changeProduct({ id, change });
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
