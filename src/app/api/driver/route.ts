import { NextResponse, NextRequest } from "next/server";
import { addDriver, changeDriver, getAllDrivers } from "@/src/services/driverservice";
export const POST = async (req: NextRequest) => {
  const data = await req.json();
  const { response, error } = await addDriver(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");

  // Compare with the expected API key
  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  const { searchParams } = new URL(req.url);
  const data = {
    page: searchParams.get("page"),
    pageSize: searchParams.get("pageSize"),
    // ...other params
  };
  const { response, error } = await getAllDrivers(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const PUT = async (req: NextRequest) => {
  const { id, change } = await req.json();

  const { response, error } = await changeDriver({ id, change });
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
