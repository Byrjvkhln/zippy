import { NextResponse, NextRequest } from "next/server";
import { getALLDriverWithDelivery } from "@/src/services/driverservice";

export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");

  // Compare with the expected API key
  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  const { response, error } = await getALLDriverWithDelivery();
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
