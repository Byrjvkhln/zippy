import { NextResponse, NextRequest } from "next/server";
import { deliveriesByArray } from "../../../../services/invoiceservice";
export const POST = async (req: NextRequest) => {
  const { data } = await req.json();
  const { response, error } = await deliveriesByArray(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
