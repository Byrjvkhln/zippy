import { NextResponse, NextRequest } from "next/server";
import { changeInvoice, createInvoice, getAllInvoice } from "@/src/services/invoiceservice";
export const POST = async (req: NextRequest) => {
  const data = await req.json();
  const { response, error } = await createInvoice(data);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");
  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  const { response, error } = await getAllInvoice();
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
export const PUT = async (req: NextRequest) => {
  const { id, change } = await req.json();
  const { response, error } = await changeInvoice({ id, change });
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
