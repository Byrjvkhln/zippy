import { NextResponse, NextRequest } from "next/server";
import { getPathVariable } from "@/src/utils/url";
import { getInvoiceById } from "@/src/services/invoiceservice";
export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");
  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  const id = await getPathVariable(req, "/api/invoice/");
  const { response, error } = await getInvoiceById(id);
  if (error) return NextResponse.json(error, { status: 500 });
  return NextResponse.json(response);
};
