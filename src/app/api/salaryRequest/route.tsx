import { prisma } from "@/src/utils/prisma";
import { NextResponse, NextRequest } from "next/server";
export const POST = async (req: NextRequest) => {
  try {
    const { DriverId, Value } = await req.json();

    await prisma.salaryReq.create({ data: { DriverId, Status: null, Value } });
    return NextResponse.json("success");
  } catch (error) {
    return NextResponse.json(error);
  }
};
export const PUT = async (req: any) => {
  const { change, id } = await req.json();

  try {
    await prisma.salaryReq.update({
      where: { id },
      data: { [change[0]]: change[1] },
    });
    console.log(change[1]);
    console.log("psda ");
    const salary: any = await prisma.salaryReq.findUnique({
      where: {
        id: id,
      },
    });
    const driver: any = await prisma.driver.findUnique({
      where: {
        id: salary?.DriverId,
      },
    });

    const newtotal = (await driver?.totalSalary) + driver?.Salary?.Total;
    await prisma.driver.update({
      where: { id: driver?.id },
      data: {
        Salary: { History: [], Total: 0 }, // Updating nested fields in Salary
        totalSalary: newtotal, // Updating totalSalary
      },
    });
    return NextResponse.json("success");
  } catch (error) {
    return NextResponse.json(error);
  }
};
export const GET = async (req: NextRequest) => {
  let dbData = await prisma.salaryReq.findMany({
    include: {
      Driver: true,
    },
    orderBy: {
      createdAt: "desc",
    },
  });

  const start = req.nextUrl.searchParams.get("start") || "0"; // Default to 0
  const size = req.nextUrl.searchParams.get("size") || "10"; // Default to 10
  const filters = req.nextUrl.searchParams.get("filters"); // Default to 10
  const sorting = req.nextUrl.searchParams.get("sorting"); // Default to 10
  const globalFilter = req.nextUrl.searchParams.get("globalFilter"); // Default to 10

  const parsedColumnFilters = JSON.parse(filters || "");

  if (parsedColumnFilters?.length) {
    parsedColumnFilters.map((filter: any) => {
      const { id: columnId, value: filterValue } = filter;
      dbData = dbData.filter((row: any) => {
        if (columnId === "lastLogin") {
          const rowDateValue = new Date(row[columnId]);
          const filterDateValue = new Date(filterValue as string);
          return rowDateValue > filterDateValue;
        }
        return row[columnId]
          ?.toString()
          ?.toLowerCase()
          ?.includes?.((filterValue as string).toLowerCase());
      });
    });
  }

  if (globalFilter) {
    dbData = dbData.filter((row: any) =>
      Object.keys(row).some((columnId) =>
        row[columnId]
          ?.toString()
          ?.toLowerCase()
          ?.includes?.((globalFilter as string).toLowerCase())
      )
    );
  }

  const parsedSorting = JSON.parse(sorting || "");
  if (parsedSorting?.length) {
    const sort = parsedSorting[0];
    const { id, desc } = sort;
    dbData.sort((a: any, b: any) => {
      if (desc) {
        return a[id] < b[id] ? 1 : -1;
      }
      return a[id] > b[id] ? 1 : -1;
    });
  }
  const a: any = {
    data: dbData?.slice(parseInt(start), parseInt(start) + parseInt(size)) ?? [],
    meta: { totalRowCount: dbData.length },
  };
  return NextResponse.json(a);
};
