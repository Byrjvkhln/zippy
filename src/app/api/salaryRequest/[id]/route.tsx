import { NextResponse, NextRequest } from "next/server";
import { getPathVariable } from "@/src/utils/url";
import { prisma } from "@/src/utils/prisma";
export const GET = async (req: NextRequest) => {
  const apiKey = req.headers.get("x-api-key");

  if (apiKey !== process.env.API_KEY) {
    return NextResponse.json({ message: "Zaa bti davrad bgarai" }, { status: 403 });
  }
  try {
    const id = getPathVariable(req, "/api/salaryRequest/");
    const result = await prisma.salaryReq.findMany({
      where: {
        DriverId: id,
      },
    });
    return NextResponse.json(result);
  } catch (error) {
    return NextResponse.json(error);
  }
};
