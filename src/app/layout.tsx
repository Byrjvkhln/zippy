import type { Metadata } from "next";
import { Montserrat } from 'next/font/google'
import "./globals.css";
import TestComponent from "../components/test";

const montserrat = Montserrat({
  subsets: ['cyrillic'],
  weight: ['300', '400', '500'],
})

export const metadata: Metadata = {
  title: "Zippy.mn",
  description: "ZIPPY шуурхай хүргэлтийн саадгүй үйлчилгээ",
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <head>
        <link rel="icon" href="/logo.svg" />
      </head>
      <body className={montserrat.className}>
        {/* <TestComponent /> */}
        {children}
      </body>
    </html>
  );
}
