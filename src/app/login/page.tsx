"use client";

import React, { useState, useEffect } from "react";
import { Button, Input, Checkbox } from "@material-tailwind/react";
import { FiEye, FiEyeOff } from "react-icons/fi";
import axios from "axios";
import { useRouter } from "next/navigation";
import { Flip, toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Image from "next/image";

const LoginPage = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [remember, setRemember] = useState(false);
  const [hide, setHide] = useState(true);

  useEffect(() => {
    if (typeof window !== "undefined") {
      const storedRemember = window.localStorage.getItem("remember");
      if (storedRemember) {
        const rememberData = JSON.parse(storedRemember);
        setEmail(rememberData.email || "");
        setPassword(rememberData.password || "");
        setRemember(rememberData.remember || false);
      }

      const storedUser = window.localStorage.getItem("user");
      if (storedUser) {
        router.push("/dashboard");
      }
    }
  }, [router]);

  const adminLogin = async () => {
    if (email === "" || password === "") {
      toast.info("Бүх талбарыг бөглөнө үү!", {
        transition: Flip,
      });

      return;
    }

    try {
      const result = await axios.post("/api/admin/login", {
        Email: email,
        Password: password,
      });

      if (result.data === "User not found") {
        toast.error("Хэрэглэгч олдсонгүй!", {
          transition: Flip,
        });
      } else if (result.data === "Incorrect password") {
        toast.error("Нууц үг буруу байна!", {
          transition: Flip,
        });
      } else {
        if (typeof window !== "undefined") {
          window.localStorage.setItem("user", JSON.stringify(result.data.data));
          window.localStorage.setItem("token", result.data.token);
          router.push("/dashboard");
        }
      }
    } catch (error) {
      toast.info("Алдаа гарлаа. Дахин оролдоно уу!", {
        transition: Flip,
      });
    }
  };

  useEffect(() => {
    if (remember) {
      if (typeof window !== "undefined") {
        window.localStorage.setItem("remember", JSON.stringify({ email, password, remember }));
      }
    } else {
      if (typeof window !== "undefined") {
        window.localStorage.removeItem("remember");
      }
    }
  }, [email, password, remember]);

  const handleKeyPress = (e: any) => {
    if (e.key === "Enter") {
      adminLogin();
    }
  };

  return (
    <div className="bg-white h-screen  ">
      <ToastContainer position="bottom-center" autoClose={1500} />

      <div className="flex  items-center justify-center h-full xl:px-56 gap-5 lg:px-20 lg:gap-14 px-5 2xl:gap-40">
        <div className="hidden md:block flex-1 shrink-0">
          <Image
            src="/delivery.jpg"
            alt=""
            priority
            width={500}
            height={500}
            className="object-cover shrink-0 w-full"
          />
        </div>
        <div className="flex-1">
          <form
            className="2xl:w-[400px] border border-main rounded-2xl px-10 py-12 bg-white/20 2xl:ml-10"
            onSubmit={(e) => e.preventDefault()}
            onKeyDown={handleKeyPress}
          >
            <div className="full mb-10">
              <Input
                {...({} as any)}
                type="email"
                color=""
                label="И-мэйл"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="w-full mb-10">
              <Input
                {...({} as any)}
                type={hide ? "password" : "text"}
                label="Нууц үг"
                color=""
                className=""
                icon={
                  hide ? (
                    <FiEyeOff className="text-lg cursor-pointer" onClick={() => setHide(false)} />
                  ) : (
                    <FiEye onClick={() => setHide(true)} className="text-lg cursor-pointer" />
                  )
                }
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <div className="mt-3">
                <Checkbox
                  {...({} as any)}
                  label="Намайг сана"
                  color="blue"
                  onChange={() => setRemember(!remember)}
                  checked={remember}
                  labelProps={{ className: "" }}
                />
              </div>
            </div>
            <Button {...({} as any)} className="bg-main text-white normal-case text-lg w-full" onClick={adminLogin}>
              Нэвтрэх
            </Button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
